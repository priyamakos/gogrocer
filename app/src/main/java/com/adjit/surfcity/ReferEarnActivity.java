package com.adjit.surfcity;

import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Config.BaseURL;
import Fragment.About_us_fragment;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.Session_management;

public class ReferEarnActivity extends Fragment {

    RelativeLayout btn_refer_share;
    String referral_code="",userId, your_earn_amt = "0", your_frd_earn_amt = "0";
    TextView refercodeTextview, contentTextview;
    private static String TAG = About_us_fragment.class.getSimpleName();
    private Session_management sessionManagement;

    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    public ReferEarnActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_refer_earn, container, false);

        setHasOptionsMenu(true);

        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        sessionManagement = new Session_management(getActivity());
        //String geturl = getArguments().getString("url");

        ((MainActivity) getActivity()).setTitle("Refer and Earn");
        btn_refer_share = view.findViewById(R.id.btn_refer_share);
        refercodeTextview = view.findViewById(R.id.refercodeTextview);
        contentTextview = view.findViewById(R.id.contentTextview);

        btn_refer_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(sessionManagement.isLoggedIn() && !referral_code.equals(""))
                {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            "Hey check out app at: https://play.google.com/store/apps/details?id=com.adjit.surfcitycom.adjit.surfcity  ReferCode: " + referral_code);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
                else
                {
                    startActivity(new Intent(getActivity(),LoginOrReg.class));
                    getActivity().finish();
                }

            }
        });

        if (sessionManagement.isLoggedIn()) {
            userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        }
        if (NetworkConnection.connectionChecking(getActivity())){
            getRefercode();
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }

        return view;
    }

        private void getRefercode() {
            alertDialog.show();
            String wallet_amt_url = BaseURL.REFERAL_CODE_URL+"?user_id="+userId;

            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.POST, wallet_amt_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    alertDialog.dismiss();
                    Log.e("wallet_amt_res",response);
                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);

                        Boolean status = jsonObject.getBoolean("responce");
                        Log.e("wallet_amt_resstatus",""+status);
                        if (status)
                        {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            JSONObject obj = jsonArray.getJSONObject(0);
                            String user_id = obj.getString("user_id");
                            referral_code = obj.getString("referral_code");
                            refercodeTextview.setText(referral_code);

                            JSONObject global_setting = jsonObject.getJSONObject("global_setting");
                            your_earn_amt = global_setting.getString("refer_and_earn");
                            your_frd_earn_amt = global_setting.getString("new_user_reg_amt");
                            contentTextview.setText("Ask your friends to signup with your referral" +
                                    " code and make an initial payment." +
                                    " Once done. \nYou will get Rs." + your_earn_amt +
                                    " and your friend will get Rs." + your_frd_earn_amt);
                        } else {
                            String error = jsonObject.getString("error");
                            Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertDialog.dismiss();
                }
            });
            rq.add(stringRequest);
        }
    @Override public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setVisible(false);
    }
}
