package com.adjit.surfcity;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.internal.service.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Config.BaseURL;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.Utils;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = ForgotActivity.class.getSimpleName();

    private RelativeLayout btn_continue;
    private EditText et_email;
    private TextView tv_email;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title

        setContentView(R.layout.activity_forgot);
        // Call the function callInstamojo to start payment here
        mDialogBuilder = new AlertDialog.Builder(ForgotActivity.this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        et_email = (EditText) findViewById(R.id.et_login_email);
        tv_email = (TextView) findViewById(R.id.tv_login_email);
        btn_continue = (RelativeLayout) findViewById(R.id.btnContinue);

        btn_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnContinue) {
            attemptForgot();
        }
    }

    private void attemptForgot() {
        tv_email.setText(getResources().getString(R.string.tv_login_email));
        tv_email.setTextColor(getResources().getColor(R.color.dark_gray));
        String getemail = et_email.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(getemail)) {
//            tv_email.setTextColor(getResources().getColor(R.color.black));
//            focusView = et_email;
            Toast.makeText(this, "Enter email id", Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (!Utils.isEmailValid(getemail)) {
            Toast.makeText(this, "Enter valid email id", Toast.LENGTH_SHORT).show();
//            tv_email.setTextColor(getResources().getColor(R.color.black));
//            focusView = et_email;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (focusView != null)
                focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (NetworkConnection.connectionChecking(ForgotActivity.this)){
                makeForgotRequest(getemail);
            }else {
                Toast.makeText(ForgotActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * Method to make json object request where json response starts wtih
     */
    private void makeForgotRequest(String email) {
        alertDialog.show();
        String url = BaseURL.FORGOT_URL+"?email="+email;
        RequestQueue queue = Volley.newRequestQueue(ForgotActivity.this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.e("forgotrepo", response.toString());
                try {
                    alertDialog.dismiss();
                    Boolean status = response.getBoolean("responce");
                    String error = response.getString("message");
                    if (status) {
                        Toast.makeText(ForgotActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ForgotActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Toast.makeText(ForgotActivity.this, "" + response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(ForgotActivity.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);
    }
}
