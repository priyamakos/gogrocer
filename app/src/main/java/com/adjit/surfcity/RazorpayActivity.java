package com.adjit.surfcity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import Fragment.Thanks_fragment;;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Config.BaseURL;
import util.Session_management;

public class RazorpayActivity extends AppCompatActivity  implements PaymentResultListener{
    String userid, amount, useremailid, usermobile, billid, username, firstname, lastname, msg;
    private static final String TAG = RazorpayActivity.class.getSimpleName();
    Button btnpayment;
    TextView billidtxt, amottxt;
    Boolean is_payment_done = false ;
    LinearLayout billlayout, ll_thanks;
    private Session_management sessionManagement;
    AlertDialog alertDialog;
    AlertDialog.Builder mDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razorpay);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        billidtxt = findViewById(R.id.billidtxt);
        amottxt = findViewById(R.id.amottxt);
        billlayout = findViewById(R.id.billlayout);
        ll_thanks = findViewById(R.id.ll_thanks);

        mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        sessionManagement = new Session_management(RazorpayActivity.this);
        username = sessionManagement.getUserDetails().get(BaseURL.KEY_NAME);
        userid = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);

        Intent intent = getIntent();
        amount = intent.getStringExtra("amount");
        useremailid = intent.getStringExtra("useremailid");
        usermobile = intent.getStringExtra("usermobile");
        billid = intent.getStringExtra("billid");//orderid
        msg = intent.getStringExtra("msg");

        billidtxt.setText("Bill Id: " + billid);
        amottxt.setText("INR: " + amount);

        Checkout.preload(getApplicationContext());
        // Payment button created by you in XML layout
        btnpayment = findViewById(R.id.btn_pay);
        btnpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(RazorpayActivity.this, "Please wait redirecting to payment page..", Toast.LENGTH_SHORT).show();
                billlayout.setVisibility(View.GONE);
                startPayment();
            }
        });
    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", username);
            //options.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            //options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            float amountstr = Float.parseFloat(amount);
            options.put("amount", 1 * 100);
            //Log.d("testamount", String.valueOf(amountstr*100));
            //amount,useremailid,usermobile
            JSONObject preFill = new JSONObject();
            preFill.put("email", useremailid);
            preFill.put("contact", usermobile);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            is_payment_done = true ;
            ll_thanks.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            Log.d("razorpayPaymentID",razorpayPaymentID);
            if (NetworkConnection.connectionChecking(RazorpayActivity.this)){
                edit_orderpayemnt_api(razorpayPaymentID, billid);
            }
        } catch (Exception e) {
            is_payment_done = false ;
            Log.e(TAG, "Exception in onPaymentSuccess", e);
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(RazorpayActivity.this, MainActivity.class));
            finishAffinity();
        }
    }


    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            is_payment_done = false;
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(RazorpayActivity.this, MainActivity.class));
            finishAffinity();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               if(is_payment_done){
                   startActivity(new Intent(RazorpayActivity.this, MainActivity.class));
                   finishAffinity();
               }else {
                   finish();
               }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void edit_orderpayemnt_api(final String razorpayPaymentID, final String orderid) {
        alertDialog.show();
        if (NetworkConnection.connectionChecking(this)) {
            RequestQueue rq = Volley.newRequestQueue(this);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, BaseURL.edit_order_payment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    alertDialog.dismiss();
                    Log.e("orderpaytm",response);
//                    sms_api();
//                    sms_api_customer();
                    //db_cart.clearCart();
                    is_payment_done = true ;
                    billlayout.setVisibility(View.GONE);
                    ll_thanks.setVisibility(View.VISIBLE);

                    /*Bundle args = new Bundle();
                    Fragment fm = new Thanks_fragment();
                    args.putString("msg", msg);
                    args.putString("sale_id", orderid);
                    fm.setArguments(args);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();*/
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertDialog.dismiss();
                    is_payment_done = false ;
                    msg = "Failed to update transaction id" ;
                    Toast.makeText(RazorpayActivity.this, ""+msg, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RazorpayActivity.this, MainActivity.class));
                    finishAffinity();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map=new HashMap<String, String>();
                    Log.e("params",orderid+" "+orderid+"  "+razorpayPaymentID);
                    map.put("sale_id", orderid);
                    map.put("txn_id",razorpayPaymentID);
                    map.put("order_id ", orderid);
                    map.put("user_id ", userid);
                    return map;
                }
            };
            rq.add(stringRequest);
        }
    }

    @Override
    public void onBackPressed() {
        if(is_payment_done){
            startActivity(new Intent(RazorpayActivity.this, MainActivity.class));
            finishAffinity();
        }else {
            finish();
        }
    }
}
