package com.adjit.surfcity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import Model.MyAddressDetails;
import fcm.Const;
import fcm.SharePreferenceUtility;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class GeoLocationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, LocationListener, View.OnClickListener {
    Toolbar mtoolbar;
    TextView tvCurrentLocation;
    TextView tvSelectedAddress;
    Button btn_saveAddress;
    LinearLayout ll_search_location;
    EditText edt_location;

    Boolean fromHome = false ;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 100 * 60000;  /* 10 secs */
    private long FASTEST_INTERVAL = 50 * 60000; /* 20 sec */
    private LocationManager locationManager;
    private boolean isPermission;
    private LatLng latLng;
    private GoogleMap mGoogleMap;
    private double latitude;
    private double longitude;
    private String city, postal_code, state, merchant_id, address, sub_total, del_date, del_time, street;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_location);

        mtoolbar = findViewById(R.id.toolbar);
        tvCurrentLocation = findViewById(R.id.txt_currentLocation);
                tvSelectedAddress = findViewById(R.id.txt_selectedAddress);
        btn_saveAddress = findViewById(R.id.btn_saveAddress);
        btn_saveAddress.setOnClickListener(this);
                ll_search_location = findViewById(R.id.ll_search_location);
        edt_location = findViewById(R.id.edt_location);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

       /* isPermission = ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED;*/
        if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            isPermission = false ;
        }else {
            isPermission = true ;
        }
        fromHome = getIntent().getBooleanExtra("fromHome", false);
        //checked network connection
        if (NetworkConnection.connectionChecking(this)){
            //check permission for location
            if (isPermission) {
                //set Map
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(this);
                }
                //called presenter to get changed location
                getChangedLocation();
            }else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION}, 200);
            }
        }else {
            Snackbar.make(findViewById(android.R.id.content), R.string.no_internet, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        /*if(fromHome){
            Geocoder geocoder = new Geocoder(GeoLocationActivity.this);
            List<Address> list = null;
            try {
                list = geocoder.getFromLocation(latitude, longitude, 1);
                String str_selectedAddress = list.get(0).getAddressLine(0);
                SharePreferenceUtility.saveStringPreferences(GeoLocationActivity.this, Const.DEL_ADDRESS, str_selectedAddress);
                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else {
            finish();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                    finish();
                    break;
            case R.id.menu_search:
                InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                if(ll_search_location.getVisibility() == View.VISIBLE){
                    ll_search_location.setVisibility(View.GONE);
                }else {
                    edt_location.setText("");
                    ll_search_location.setVisibility(View.VISIBLE);
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps

        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        //it was pre written
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;

        if (latLng != null) {
            Address address = new Address(Locale.getDefault());
            address.setLocality("Jule Solapur, Solapur, Maharashtra, India");

            // mGoogleMap.addMarker(new MarkerOptions().position(latLng).title("Marker in Current Location"));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f));
            mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {

                    //got new LatLng
                    LatLng midLatLng = mGoogleMap.getCameraPosition().target;
                    Log.i("newLatLang", midLatLng.latitude + " " + midLatLng.longitude);

                    //initialized GeoCoder
                    Geocoder geocoder = new Geocoder(GeoLocationActivity.this);
                    latitude = midLatLng.latitude;
                    longitude = midLatLng.longitude;

                    try {
                        List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
                        Log.e("newAddr", list.get(0).getAddressLine(0));
                        tvCurrentLocation.setText(list.get(0).getAddressLine(0));
                        tvSelectedAddress.setText(list.get(0).getAddressLine(0));
                        street = list.get(0).getAddressLine(0);
                        city=list.get(0).getLocality();
                        postal_code = list.get(0).getPostalCode();
                        state = list.get(0).getAdminArea();
                        String str_selectedAddress = list.get(0).getAddressLine(0);
                        Log.e("location", str_selectedAddress);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void searchLocation(View view) {
        if(NetworkConnection.connectionChecking(this)){
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            String location = edt_location.getText().toString();
            List<Address> addressList = null;

            if (location != null || !location.equalsIgnoreCase("")) {
                Geocoder geocoder = new Geocoder(this);
                try {
                    addressList = geocoder.getFromLocationName(location, 1);
                    if(addressList.size() > 0) {
                        Address address = addressList.get(0);
                        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(location));
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    }else {
                        Toast.makeText(this, "No such place found, please enter another location", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(this, "Enter locatio to search", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, ""+getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 200: {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermission = true ;
                    // {Some Code}
                    //set Map
                    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.map);
                    if (mapFragment != null) {
                        mapFragment.getMapAsync(this);
                    }
                    //called presenter to get changed location
                    getChangedLocation();
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        //get last known location
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "not detected location", Toast.LENGTH_SHORT).show();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getUpdatedLocation(mGoogleApiClient, mLocationRequest);
        // LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
        //    mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }
    public void getUpdatedLocation(GoogleApiClient googleApiClient, LocationRequest mLocationRequest) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, (LocationListener) this);
    }
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void getChangedLocation() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkLocation();
    }
    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }
    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Location Setting")
                .setPositiveButton("Seting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_saveAddress :
                try {
                    if(isPermission) {
                        MyAddressDetails address = new MyAddressDetails(city, postal_code, state, street, String.valueOf(latitude), String.valueOf(longitude), "");
                        Geocoder geocoder = new Geocoder(GeoLocationActivity.this);
                        List<Address> list = null;
                        list = geocoder.getFromLocation(latitude, longitude, 1);
                        String str_selectedAddress = list.get(0).getAddressLine(0);
                        SharePreferenceUtility.saveStringPreferences(GeoLocationActivity.this, Const.DEL_ADDRESS, str_selectedAddress);
                        finish();
                    }else {
                        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION}, 200);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
