package com.adjit.surfcity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.Login;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Config.BaseURL;
import Model.Subscription_model;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.Session_management;
import util.Utils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = LoginActivity.class.getSimpleName();

    private RelativeLayout btn_continue, btn_register;
    private EditText et_password, et_email;
    private TextView tv_password, tv_email, btn_forgot;
    private Session_management sessionManagement;
    private DatabaseHandler db;
    Intent intent;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
// remove title
        setContentView(R.layout.activity_login);

        et_password = (EditText) findViewById(R.id.et_login_pass);
        et_email = (EditText) findViewById(R.id.et_login_email);
        tv_password = (TextView) findViewById(R.id.tv_login_password);
        tv_email = (TextView) findViewById(R.id.tv_login_email);
        btn_continue = (RelativeLayout) findViewById(R.id.btnContinue);
        btn_register = (RelativeLayout) findViewById(R.id.btnRegister);
        btn_forgot = (TextView) findViewById(R.id.btnForgot);
        btn_continue.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        btn_forgot.setOnClickListener(this);
        db = new DatabaseHandler(getApplicationContext());
        intent = getIntent();
        sessionManagement = new Session_management(LoginActivity.this);

        mDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnContinue) {
            if (NetworkConnection.connectionChecking(LoginActivity.this)){
                attemptLogin();
            }else {
                Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.btnRegister) {
            Intent startRegister = new Intent(LoginActivity.this, RegisterActivity.class);
            if (intent != null) {
                startRegister.putExtra("flag", intent.getIntExtra("flag", 0));
            }
            startActivity(startRegister);
            finish();
        }else if(id == R.id.btnForgot){
            startActivity(new Intent(LoginActivity.this, ForgotActivity.class));
        }
    }

    private void attemptLogin() {
        tv_email.setText(getResources().getString(R.string.et_login_phone_hint));
        tv_password.setText(getResources().getString(R.string.tv_login_password));
        tv_password.setTextColor(getResources().getColor(R.color.black));
        tv_email.setTextColor(getResources().getColor(R.color.black));
        String getpassword = et_password.getText().toString();
        String getemail = et_email.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (getemail.toString().equals("")) {
            Toast.makeText(LoginActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (!Utils.isValidMobileNumber(getemail)) {
            Toast.makeText(LoginActivity.this, "Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (getpassword.toString().equals("")) {
            Toast.makeText(LoginActivity.this, "Enter Password", Toast.LENGTH_SHORT).show();
        }else {
            if (NetworkConnection.connectionChecking(LoginActivity.this)){
                makeLoginRequest(getemail, getpassword);
            }else {
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isPhoneValid(String getemail) {
        boolean check = false;
//if(!Pattern.matches("[a-zA-Z]+", phone))
//{
        if (getemail.length() == 10)// || phone.length() > 0
        {
//if(phone.length() != 10)
// {
            check = true;

            Log.d("motest", "false");

//txtPhone.setError("Not Valid Number");
// }
        } else {
            Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
            Log.d("motest", "true");
            check = false;
        }
//} else
// {
// check=false;
//}
        return check;

    }

    private boolean isEmailValid(String email) {
//TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
//TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /*
     * Method to make json object request where json response starts wtih
     */
    private void makeLoginRequest(String email, final String password) {
        String fcm = (String) SharePreferenceUtility.getPreferences(this, Const.FCM_TOKEN, SharePreferenceUtility.PREFTYPE_STRING);
        alertDialog.show();
// Tag used to cancel the request
        String tag_json_obj = "json_login_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_email", email);
        params.put("password", password);

        String url_login= BaseURL.LOGIN_URL+
                "?user_email="+email+"&password="+password+"&user_fcm_code="+fcm;

        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url_login, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                alertDialog.dismiss();
                Log.d(TAG, response.toString());
                Log.e("loginresponse", response.toString());
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        JSONObject obj = response.getJSONObject("data");
                        String user_id = obj.getString("user_id");
                        String user_fullname = obj.getString("user_fullname");
                        String user_email = obj.getString("user_email");
                        String user_phone = obj.getString("user_phone");
                        String user_image = obj.getString("user_image");
                        String wallet_ammount = obj.getString("wallet");
                        String reward_points = obj.getString("rewards");
                        Session_management sessionManagement = new Session_management(LoginActivity.this);
                        sessionManagement.createLoginSession(user_id, user_email, user_fullname, user_phone, user_image, wallet_ammount, reward_points, "", "", "", "", password);

                        JSONArray jsonArrayBody = response.getJSONArray("sub_details");
                        if (jsonArrayBody.length() > 0) {
                            Log.d("sub_id", jsonArrayBody.toString());
                            JSONObject jsonObject = jsonArrayBody.getJSONObject(0);
                            String subs_id = jsonObject.getString("subscription_id");
                            BaseURL.sub_id = subs_id.trim();
                            SharePreferenceUtility.saveStringPreferences(LoginActivity.this, Const.SUBSCRIPTION_ID, subs_id);
                            String subscription_id = (String) SharePreferenceUtility.getPreferences(LoginActivity.this, Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
                        }

                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        if (intent != null) {
                            if (intent.getIntExtra("flag", 0) == 1) {
                                startActivity(i);
                            } else if (intent.getIntExtra("flag", 0) == 2) {
                                startActivity(i);
                            } else {
                                startActivity(i);
                            }
                        } else {
                            startActivity(i);
                        }
                        fetch_cartList(user_id, "0", "0");
                        finish();
                    } else {
                        String error = response.getString("error");
                        Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);
    }

    private void fetch_cartList(String userId, String coupan_id, String wallet_amt) {

        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", userId);
        map.put("coup_id", coupan_id);
        map.put("wallet_amount", wallet_amt);
        Log.d("body_cartList", map.toString());

        String url = BaseURL.CART_LIST+"?user_id="+userId+"&coup_id="+coupan_id+"&wallet_amount="+wallet_amt;

        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                try {
                    Log.d("cartResponse", response.toString());
                    if (response.getBoolean("responce")) {
                        if (response.getJSONArray("subscribe_data").length() > 0) {
                            Subscription_model model = new Gson().fromJson(response.getJSONArray("subscribe_data").getJSONObject(0).toString(), new TypeToken<Subscription_model>() {
                            }.getType());
                            Log.d("sub_model", model.toString());
                            sessionManagement.update_subscription(model);
                        }
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsonRequest);
    }

}