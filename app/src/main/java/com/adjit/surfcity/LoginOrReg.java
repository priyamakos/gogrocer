package com.adjit.surfcity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.internal.service.Common;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import Config.BaseURL;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.Session_management;
import util.Utils;

public class LoginOrReg extends AppCompatActivity implements View.OnClickListener {
    private  Button btn_login ;
    private View txtv_create_acc;
    private LinearLayout ll_fb_login, ll_gmail_login;
    private static String TAG = LoginOrReg.class.getSimpleName();
    //Fb login
    private static final String EMAIL = "email";
    LoginButton loginButton;
    CallbackManager callbackManager ;
    Intent intent;
    //Login Wih Gmail
    GoogleSignInClient mGoogleSignInClient;
    final int RC_SIGN_IN = 1001 ;


    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_or_reg);
        init();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //FB login callback
        //If you register the callback with LoginButton, don't need to register the callback on Login manager.
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                    }
                    @Override
                    public void onCancel() {
                        // App code
                    }
                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
        logoutAllSession();
        loginFb();
    }

    private void loginFb() {
        callbackManager = CallbackManager.Factory.create();
        // If you are using in a fragment, call loginButton.setFragment(this);
        //loginButton.setFragment(this);
        loginButton.setReadPermissions(Arrays.asList(EMAIL,"email"));
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                if(loginResult != null) {
                    try {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Application code
                                        try {
                                            final String email = object.getString("email");
                                            final String name = object.getString("name"); // 01/31/1980 format
//                                            Toast.makeText(LoginOrReg.this, ""+loginResult.getAccessToken().getToken(), Toast.LENGTH_SHORT).show();
                                            /*call login */
                                            final BottomSheetDialog getMobileFromUser = new BottomSheetDialog(LoginOrReg.this);
                                            getMobileFromUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            final View showBottomPopup = getLayoutInflater().inflate(R.layout.get_user_mobile_number, null);
                                            getMobileFromUser.setContentView(showBottomPopup);
                                            getMobileFromUser.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                                            getMobileFromUser.setCancelable(false);
                                            getMobileFromUser.show();
                                            final EditText mobile = (EditText) getMobileFromUser.findViewById(R.id.edt_mobile);
                                            final TextView txt_cancel_login = (TextView) getMobileFromUser.findViewById(R.id.txt_cancel_login);
                                            Button submit = (Button) getMobileFromUser.findViewById(R.id.btn_submit);
                                            submit.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    if (TextUtils.isEmpty(mobile.getText().toString().trim())
                                                            || !(Utils.isValidMobileNumber(mobile.getText().toString().trim()))
                                                            || mobile.getText().toString().trim().length() != 10) {
                                                        Toast.makeText(LoginOrReg.this, "Please enter valid mobile number to proceed", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        String mob = mobile.getText().toString().trim();
                                                        if(NetworkConnection.connectionChecking(LoginOrReg.this)) {
                                                            getMobileFromUser.dismiss();
                                                            loginWithFacebook(email, "", mob, name, loginResult.getAccessToken().getToken());
                                                        }else {
                                                            Toast.makeText(LoginOrReg.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                }
                                            });
                                            txt_cancel_login.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    getMobileFromUser.dismiss();
                                                    logoutAllSession();
                                                }
                                            });
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }catch (Exception e){

                    }
                }
                /*Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                Log.i("FB_Token",""+loginResult.getAccessToken());*/
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginOrReg.this, "Cancelled by User", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginOrReg.this, ""+error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void init() {
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        txtv_create_acc = findViewById(R.id.txtv_create_acc);
        txtv_create_acc.setOnClickListener(this);
        ll_fb_login = findViewById(R.id.ll_fb_login);
        ll_fb_login.setOnClickListener(this);
        ll_gmail_login = findViewById(R.id.ll_gmail_login);
        ll_gmail_login.setOnClickListener(this);
        loginButton = findViewById(R.id.login_button);
        mDialogBuilder = new AlertDialog.Builder(LoginOrReg.this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login :
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("flag", 2);
                startActivity(intent);
                break;

            case R.id.txtv_create_acc :
                Intent reg_intent = new Intent(LoginOrReg.this, RegisterActivity.class);
                if (reg_intent != null) {
                    reg_intent.putExtra("flag", reg_intent.getIntExtra("flag", 0));
                }
                startActivity(reg_intent);
                break;
            case R.id.ll_gmail_login :
                signIn();
                break;
            case R.id.ll_fb_login :
                loginButton.performClick();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            Log.w("GoogleSign", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(final GoogleSignInAccount account) {
        if(account != null) {
            try {
                /*call login */
                final BottomSheetDialog getMobileFromUser = new BottomSheetDialog(LoginOrReg.this);
                getMobileFromUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
                final View showBottomPopup = getLayoutInflater().inflate(R.layout.get_user_mobile_number, null);
                getMobileFromUser.setContentView(showBottomPopup);
                getMobileFromUser.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                getMobileFromUser.setCancelable(false);
                getMobileFromUser.show();
                final EditText mobile = (EditText) getMobileFromUser.findViewById(R.id.edt_mobile);
                final TextView txt_cancel_login = (TextView) getMobileFromUser.findViewById(R.id.txt_cancel_login);
                Button submit = (Button) getMobileFromUser.findViewById(R.id.btn_submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (TextUtils.isEmpty(mobile.getText().toString().trim())
                                || !(Utils.isValidMobileNumber(mobile.getText().toString().trim()))
                                || mobile.getText().toString().trim().length() != 10) {
                            Toast.makeText(LoginOrReg.this, "Please enter valid mobile number to proceed", Toast.LENGTH_SHORT).show();
                        } else {
                            String mob = mobile.getText().toString().trim();
                            if(NetworkConnection.connectionChecking(LoginOrReg.this)) {
                                getMobileFromUser.dismiss();
                                loginWithGmail(account.getEmail(),"", mob, account.getDisplayName(), account.getId() );
                            }else {
                                Toast.makeText(LoginOrReg.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                txt_cancel_login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getMobileFromUser.dismiss();
                        logoutAllSession();
                    }
                });
            }catch (Exception e){
                Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loginWithGmail(String email, final String password, final String mobile, String u_name, String g_token) {
        String fcm = (String) SharePreferenceUtility.getPreferences(this, Const.FCM_TOKEN, SharePreferenceUtility.PREFTYPE_STRING);
        alertDialog.show();

        String tag_json_obj = "json_login_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_email", email);
        params.put("password", password);

        String url_login= BaseURL.LOGIN_WITH_GMAIL_URL+ "?user_name="+u_name+"&user_mobile="+mobile+"&user_email="+email
                +"&password="+"" +"&user_fcm_code="+fcm+"&google_token="+g_token;

        RequestQueue queue = Volley.newRequestQueue(LoginOrReg.this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url_login, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                alertDialog.dismiss();
                Log.d(TAG, response.toString());
                Log.e("loginresponse", response.toString());
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        JSONObject obj = response.getJSONObject("data");
                        String user_id = obj.getString("user_id");
                        String user_fullname = obj.getString("user_fullname");
                        String user_email = obj.getString("user_email");
                        String user_phone = obj.getString("user_phone");
                        String user_image = obj.getString("user_image");
                        String wallet_ammount = obj.getString("wallet");
                        String reward_points = obj.getString("rewards");
                        Session_management sessionManagement = new Session_management(LoginOrReg.this);
                        sessionManagement.createLoginSession(user_id, user_email, user_fullname, user_phone, user_image, wallet_ammount, reward_points, "", "", "", "", password);
                        Toast.makeText(LoginOrReg.this, "Login Ok", Toast.LENGTH_SHORT).show();
                        JSONArray jsonArrayBody = response.getJSONArray("sub_details");
                        if (jsonArrayBody.length() > 0) {
                            Log.d("sub_id", jsonArrayBody.toString());
                            JSONObject jsonObject = jsonArrayBody.getJSONObject(0);
                            String subs_id = jsonObject.getString("subscription_id");
                            BaseURL.sub_id = subs_id.trim();
                            SharePreferenceUtility.saveStringPreferences(LoginOrReg.this, Const.SUBSCRIPTION_ID, subs_id);
                            SharePreferenceUtility.saveStringPreferences(LoginOrReg.this, Const.MOB_NUM, mobile);
                            String subscription_id = (String) SharePreferenceUtility.getPreferences(LoginOrReg.this, Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
                        }

                        Intent i = new Intent(LoginOrReg.this, MainActivity.class);
                        if (intent != null) {
                            if (intent.getIntExtra("flag", 0) == 1) {

                            } else if (intent.getIntExtra("flag", 0) == 2) {

                            } else {
                                startActivity(i);
                            }
                        } else {
                            startActivity(i);
                        }
                       /* if (NetworkConnection.connectionChecking(LoginOrReg.this)){
                            fetch_cartList(user_id, "0", "0");
                        }*/
                        finish();
                    } else {
                        String error = response.getString("error");
                        Toast.makeText(LoginOrReg.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(LoginOrReg.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);
    }

    private void loginWithFacebook(String email, final String password, final String mobile, String u_name, String g_token) {
        String fcm = (String) SharePreferenceUtility.getPreferences(this, Const.FCM_TOKEN, SharePreferenceUtility.PREFTYPE_STRING);
        alertDialog.show();

        String tag_json_obj = "json_login_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_email", email);
        params.put("password", password);

        String url_login= BaseURL.LOGIN_WITH_FB_URL+ "?user_name="+u_name+"&user_mobile="+mobile+"&user_email="+email
                +"&password="+"" +"&user_fcm_code="+fcm+"&facebook_token="+g_token;

        RequestQueue queue = Volley.newRequestQueue(LoginOrReg.this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url_login, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                alertDialog.dismiss();
                Log.d(TAG, response.toString());
                Log.e("loginresponse", response.toString());
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        JSONObject obj = response.getJSONObject("data");
                        String user_id = obj.getString("user_id");
                        String user_fullname = obj.getString("user_fullname");
                        String user_email = obj.getString("user_email");
                        String user_phone = obj.getString("user_phone");
                        String user_image = obj.getString("user_image");
                        String wallet_ammount = obj.getString("wallet");
                        String reward_points = obj.getString("rewards");
                        Session_management sessionManagement = new Session_management(LoginOrReg.this);
                        sessionManagement.createLoginSession(user_id, user_email, user_fullname, user_phone, user_image, wallet_ammount, reward_points, "", "", "", "", password);
                        Toast.makeText(LoginOrReg.this, "Login Ok", Toast.LENGTH_SHORT).show();
                        JSONArray jsonArrayBody = response.getJSONArray("sub_details");
                        if (jsonArrayBody.length() > 0) {
                            Log.d("sub_id", jsonArrayBody.toString());
                            JSONObject jsonObject = jsonArrayBody.getJSONObject(0);
                            String subs_id = jsonObject.getString("subscription_id");
                            BaseURL.sub_id = subs_id.trim();
                            SharePreferenceUtility.saveStringPreferences(LoginOrReg.this, Const.SUBSCRIPTION_ID, subs_id);
                            SharePreferenceUtility.saveStringPreferences(LoginOrReg.this, Const.MOB_NUM, mobile);
                            String subscription_id = (String) SharePreferenceUtility.getPreferences(LoginOrReg.this, Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
                        }

                        Intent i = new Intent(LoginOrReg.this, MainActivity.class);
                        if (intent != null) {
                            if (intent.getIntExtra("flag", 0) == 1) {

                            } else if (intent.getIntExtra("flag", 0) == 2) {

                            } else {
                                startActivity(i);
                            }
                        } else {
                            startActivity(i);
                        }
                       /* if (NetworkConnection.connectionChecking(LoginOrReg.this)){
                            fetch_cartList(user_id, "0", "0");
                        }*/
                        finish();
                    } else {
                        String error = response.getString("error");
                        Toast.makeText(LoginOrReg.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(LoginOrReg.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);
    }


    private void logoutAllSession() {
        try{
            LoginManager.getInstance().logOut();
        }catch (Exception e){

        }
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if(account != null){
            GoogleSignInClient mGoogleSignInClient;
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            // Build a GoogleSignInClient with the options specified by gso.
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // ...
                        }
                    });
        }
    }
}
