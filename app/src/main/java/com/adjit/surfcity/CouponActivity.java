package com.adjit.surfcity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import Adapter.Coupon_adapter;
import Config.BaseURL;
import Fragment.Coupon_list_fragment;
import Model.Coupon_list_model;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.Session_management;

public class CouponActivity extends AppCompatActivity {
    private static String TAG = CouponActivity.class.getSimpleName();
    RecyclerView recycler;
    public ArrayList<Coupon_list_model> coupon_list_model=new ArrayList<>();
    RecyclerView.Adapter coupon_adapter;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    Coupon_adapter adapter ;
    onCoupenCodeDownloaded listner ;
    private Session_management sessionManagement;
    String userId, coupon_id, cpn_code;
    private String wallet_percent_amt;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupon_list);
        sessionManagement = new Session_management(this);
        userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Apply Coupons");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initUI();
        if (NetworkConnection.connectionChecking(CouponActivity.this)){
            getCoupenCodes();
        }else {
            Toast.makeText(CouponActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
        recycler = findViewById(R.id.rv_coupon_list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home :
                Intent resultIntent = new Intent();
                resultIntent.putExtra("coupon_id", "0");
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                break;
        }
        return false ;
    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("coupon_id", "0");
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private void initUI() {
//        Bundle bundle = this.getArguments();
        wallet_percent_amt = getIntent().getStringExtra("wallet_amt");
    }

    private void getCoupenCodes() {
        String tag_json_obj = "json_coupen_req";

        mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        alertDialog.show();
        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.GET,
                BaseURL.DownloadCoupenCode, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                try {
                    ArrayList<Coupon_list_model> al_coupen_code = new ArrayList<>();
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Coupon_list_model>>() {
                            }.getType();
                            coupon_list_model = gson.fromJson(response.getString("data"), listType);
                            if(coupon_list_model.size() > 0) {
                                // Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                adapter = new Coupon_adapter(CouponActivity.this, coupon_list_model, wallet_percent_amt);
                                recycler.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(CouponActivity.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    interface onCoupenCodeDownloaded {
        void onSuccess();
        void onFail();
    }
}
