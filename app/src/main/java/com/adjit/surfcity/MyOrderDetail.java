package com.adjit.surfcity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapter.My_order_detail_adapter;
import Config.BaseURL;
import Fragment.My_order_detail_fragment;
import Model.My_order_detail_model;
import de.hdodenhof.circleimageview.CircleImageView;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonArrayRequest;
import util.CustomVolleyJsonRequest;
import util.Session_management;
import util.Utils;

public class MyOrderDetail extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = My_order_detail_fragment.class.getSimpleName();

    private TextView tv_date, tv_time, tv_net_total, tv_delivery_charge, tv_wallet, tv_coupon, tv_cashback, tv_total;
    private RelativeLayout btn_cancle;
    private LinearLayout ll_driver, layoutBottomSheet;
    private RecyclerView rv_detail_order;
    private String sale_id;
    ImageView back_button;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    private List<My_order_detail_model> my_order_detail_modelList = new ArrayList<>();
    private String total_rs, date, time, status, deli_charge ;
    private String driver_profile, driver_name, driver_phone, driver_veh ;

    public MyOrderDetail() {
        // Required empty public constructor
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_order_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Orders Detail");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        tv_date = (TextView) findViewById(R.id.tv_order_Detail_date);
        tv_time = (TextView) findViewById(R.id.tv_order_Detail_time);
        tv_delivery_charge = (TextView) findViewById(R.id.tv_order_Detail_deli_charge);
        tv_wallet = (TextView) findViewById(R.id.tv_wallet);
        tv_coupon = (TextView) findViewById(R.id.tv_coupon);
        tv_cashback = (TextView) findViewById(R.id.tv_cashback);
        tv_net_total = (TextView) findViewById(R.id.tv_order_Detail_total);
        tv_total = (TextView) findViewById(R.id.tv_total);
        btn_cancle = (RelativeLayout) findViewById(R.id.btn_order_detail_cancle);
        btn_cancle.setOnClickListener(this);
        rv_detail_order = (RecyclerView) findViewById(R.id.rv_order_detail);
        ll_driver = findViewById(R.id.ll_driver);
        ll_driver.setOnClickListener(this);
        layoutBottomSheet = findViewById(R.id.layoutBottomSheet);
        rv_detail_order.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        sale_id = getIntent().getStringExtra("sale_id");
        total_rs = getIntent().getStringExtra("total");
        date = getIntent().getStringExtra("date");
        time = getIntent().getStringExtra("time");
        status = getIntent().getStringExtra("status");
        deli_charge = getIntent().getStringExtra("deli_charge");

        /*if (status.equals("0")) {
            btn_cancle.setVisibility(View.VISIBLE);
        } else {
            btn_cancle.setVisibility(View.GONE);
        }*/

        if (NetworkConnection.connectionChecking(MyOrderDetail.this)){
            alertDialog.show();
            makeGetOrderDetailRequest(sale_id);
        }else {
            Toast.makeText(MyOrderDetail.this, MyOrderDetail.this.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    // alertdialog for cancle order
    private void showDeleteDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyOrderDetail.this);
        alertDialog.setMessage(getResources().getString(R.string.cancle_order_note));
        alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                Session_management sessionManagement = new Session_management(MyOrderDetail.this);
                String user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
                // check internet connection
                if (NetworkConnection.connectionChecking(MyOrderDetail.this)){
                    makeDeleteOrderRequest(sale_id, user_id);
                }else {
                    Toast.makeText(MyOrderDetail.this, MyOrderDetail.this.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    /**
     * Method to make json array request where json response starts wtih
     */
    private void makeGetOrderDetailRequest(String sale_id) {
        // Tag used to cancel the request
        alertDialog.show();
        String tag_json_obj = "json_order_detail_req";
        String url = BaseURL.ORDER_DETAIL_URL+"?sale_id="+sale_id;
        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                tv_date.setText(getResources().getString(R.string.date) + date);
                try{
                    String start = time.trim().split("-")[0];
                    String end = time.trim().split("-")[1];
                    tv_time.setText(getResources().getString(R.string.time) + Utils.convert24To12(start.trim()) + " - "
                            + Utils.convert24To12(end.trim()));
                }catch (Exception e) {
                    tv_time.setText(getResources().getString(R.string.time) + time);
                }
                tv_delivery_charge.setText(getResources().getString(R.string.delivery_charge) +"\u20B9 " + deli_charge);
                JSONObject sale_object = null ;
                try {
                    sale_object = response.getJSONObject("sale");
//                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    tv_wallet.setText("Wallet Amt : \u20B9 "+sale_object.getString("wallet_amount"));
                    tv_coupon.setText("Coupon Amt : \u20B9 "+sale_object.getString("coupon_amount"));
                    tv_cashback.setText("Cashback Amt : \u20B9 "+sale_object.getString("cash_back").trim());
                    tv_total.setText("Total Amount :  \u20B9 " + sale_object.getString("total_amount"));
                    tv_net_total.setText("\u20B9 " + sale_object.getString("total_net_amt").trim());
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }

                try {
                    JSONObject driverObject = sale_object.getJSONObject("driver");
                    ll_driver.setVisibility(View.VISIBLE);
                    driver_profile = driverObject.getString("profile_photo");
                    driver_name = driverObject.getString("first_name")+" "+driverObject.getString("last_name");
                    driver_phone = driverObject.getString("phone");
                    driver_veh = driverObject.getString("licence_plate");
                }catch (JSONException e){
                    ll_driver.setVisibility(View.GONE);
                    alertDialog.dismiss();
                }
                Log.d(TAG, response.toString());
                Gson gson = new Gson();
                Type listType = new TypeToken<List<My_order_detail_model>>() {
                }.getType();
                try {
                    my_order_detail_modelList = gson.fromJson(response.getJSONArray("item").toString(), listType);
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
                My_order_detail_adapter adapter = new My_order_detail_adapter(my_order_detail_modelList);
                rv_detail_order.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                if (my_order_detail_modelList.isEmpty()) {
                    Toast.makeText(MyOrderDetail.this, getResources().getString(R.string.no_rcord_found), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(MyOrderDetail.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    /**
     * Method to make json object request where json response starts wtih
     */
    private void makeDeleteOrderRequest(String sale_id, String user_id) {
        // Tag used to cancel the request
        alertDialog.show();
        String tag_json_obj = "json_delete_order_req";
        final Map<String, String> params = new HashMap<String, String>();
        params.put("sale_id", sale_id);
        params.put("user_id", user_id);
        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.DELETE_ORDER_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                Log.d(TAG, response.toString());
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        String msg = response.getString("message");
                        Toast.makeText(MyOrderDetail.this, "" + msg, Toast.LENGTH_SHORT).show();
                        finish();
                        // ((MainActivity) getActivity()).onBackPressed();
                    } else {
                        String error = response.getString("error");
                        Toast.makeText(MyOrderDetail.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(MyOrderDetail.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    @Override
    public void onClick(View view) {
      switch (view.getId()){
          case R.id.btn_order_detail_cancle :
              showDeleteDialog();
              break;
          case R.id.ll_driver :
              final BottomSheetDialog getMobileFromUser = new BottomSheetDialog(MyOrderDetail.this);
              getMobileFromUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
              final View showBottomPopup = getLayoutInflater().inflate(R.layout.layout_bottom_sheet, null);
              getMobileFromUser.setContentView(showBottomPopup);
              getMobileFromUser.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
              getMobileFromUser.setCancelable(true);
              getMobileFromUser.show();
              CircleImageView iv_driver_profile ;
              TextView tv_driver_name, tv_driver_phone, tv_driver_veh ;
              iv_driver_profile = (CircleImageView) getMobileFromUser.findViewById(R.id.iv_driver_profile);
              tv_driver_name = (TextView) getMobileFromUser.findViewById(R.id.tv_driver_name);
              tv_driver_phone = (TextView) getMobileFromUser.findViewById(R.id.tv_driver_phone);
              tv_driver_veh = (TextView) getMobileFromUser.findViewById(R.id.tv_driver_veh);
              Picasso.with(MyOrderDetail.this).load(driver_profile).into(iv_driver_profile);
              tv_driver_name.setText("Driver Name : "+driver_name);
              tv_driver_phone.setText("Driver Mobile : "+driver_phone);
              tv_driver_veh.setText("Vehicle Number : "+driver_veh);
              /*if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                  sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                  toggleBottomSheet.setText("Collapse BottomSheet");
              } else {
                  sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                  toggleBottomSheet.setText("Expand BottomSheet");
              }*/
              break;
      }
    }
}
