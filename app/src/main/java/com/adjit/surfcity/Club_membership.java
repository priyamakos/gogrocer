package com.adjit.surfcity;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import Adapter.Subscription_Adapter;
import Config.BaseURL;
import Interfaces.Interfaces;
import Model.Subscription_model;
import fcm.Const;
import fcm.SharePreferenceUtility;

public class Club_membership extends AppCompatActivity implements Interfaces.onPurchase, PaymentResultListener {
    private ArrayList<Subscription_model> subscription = new ArrayList<Subscription_model>();
    RecyclerView club_recycler;
    Subscription_Adapter sub_Adapter;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    String sub_id , sub_amt, txn_id, user_id, order_id ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_membership);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Club");
        club_recycler = findViewById(R.id.club_recycler);
        mDialogBuilder = new AlertDialog.Builder(Club_membership.this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        if (NetworkConnection.connectionChecking(Club_membership.this)){
            fetch_subscription();
        }

    }

    private void fetch_subscription() {
        alertDialog.show();
        Log.d("url", BaseURL.GET_SUBSCRIPTION_URL);
        JsonObjectRequest subreq = new JsonObjectRequest(Request.Method.GET, BaseURL.GET_SUBSCRIPTION_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                try {
                    if (response.getBoolean("responce")) {
                        Log.d("response",response.toString());
                        JSONArray data =  response.getJSONArray("data");
                        subscription.addAll((Collection<? extends Subscription_model>) new Gson().fromJson(data.toString(),new TypeToken<ArrayList<Subscription_model>>(){}.getType()));

                        Subscription_model priceSubscription_model = null;
                        String subscription_id = (String) SharePreferenceUtility.getPreferences(Club_membership.this, Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
                        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

                        if (!subscription_id.equals("")) {
                            for (int i = 0; i < subscription.size(); i++) {
                                if (subscription.get(i).getSubscription_id().equals(subscription_id)) {
                                    priceSubscription_model = subscription.get(i);
                                }
                            }
                            sub_Adapter = new Subscription_Adapter(Club_membership.this, subscription, 0, priceSubscription_model, Club_membership.this);
                        } else {
                            sub_Adapter = new Subscription_Adapter(Club_membership.this, subscription, 0, Club_membership.this);
                        }
                        club_recycler.setLayoutManager(layoutManager2);
                        club_recycler.setAdapter(sub_Adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(subreq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startPayment(String sub_id, String sub_amount) {
        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();
        /**
         * Set your logo here
         */
        checkout.setImage(R.drawable.logo);
        /**
         * Reference to current activity
         */
        final Activity a = Club_membership.this;
        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();
            /**
             * Merchant Name
             * eg: ACME Corp || HasGeek etc.
             */
            options.put("name", "Surfcity Food Order");
            /**
             * Description can be anything
             * eg: Order #123123
             *     Invoice Payment
             *     etc.
             */
            options.put("description", "Surfcity Subscription ID #"+sub_id);
            options.put("currency", "INR");
            /**
             * Amount is always passed in PAISE
             * Eg: "500" = Rs 5.00
             */
            float amountstr = Float.parseFloat(sub_amount);
            options.put("amount", amountstr * 100);
            checkout.open(a, options);
        } catch(Exception e) {
            Log.e("SubscriptionPlan", "Error in starting Razorpay Checkout", e);
        }
    }

    private void updateTxnId(final String sub_id, String sub_amt, String s, String order_id, String user_id) {
        String url = BaseURL.PURCHASE_PLAN+"?user_id="+user_id+"&subscription_id="+sub_id+"&order_id="+order_id+"&txt_id="+s+"&amount="+sub_amt;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo", response.toString());
                try {
                    if (response.getBoolean("responce")) {
                        SharePreferenceUtility.saveStringPreferences(Club_membership.this, Const.SUBSCRIPTION_ID, sub_id);
                        Toast.makeText(Club_membership.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        subscription.clear();
                        club_recycler.setAdapter(null);
                        if (NetworkConnection.connectionChecking(Club_membership.this)){
                            fetch_subscription();
                        }

                    }else {
                        Toast.makeText(Club_membership.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(Club_membership.this, "Failed", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Club_membership.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(jsonRequest);
    }

    @Override
    public void onClickPurchase(String sub_id, String sub_amt, String s, String order_id, String user_id) {
        this.sub_id = sub_id ; this.sub_amt = sub_amt ;  this.order_id = order_id ;   this.user_id = user_id ;
        Toast.makeText(this, "Please wait redirecting to payment page...", Toast.LENGTH_SHORT).show();
        startPayment(sub_id, sub_amt);
    }

    @Override
    public void onPaymentSuccess(String s) {
        txn_id = s ;
        updateTxnId(sub_id, sub_amt, txn_id, order_id, user_id);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(this, "Payment Cancelled By User", Toast.LENGTH_SHORT).show();
    }
}
