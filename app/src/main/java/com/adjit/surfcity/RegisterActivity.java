package com.adjit.surfcity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Config.BaseURL;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;

public class RegisterActivity extends AppCompatActivity {

    private static String TAG = RegisterActivity.class.getSimpleName();

    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    private EditText et_phone, et_name, et_password, et_email,et_reg_refer_code;
    private RelativeLayout btn_register;
    private TextView tv_phone, tv_name, tv_password, tv_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        setContentView(R.layout.activity_register);

        et_phone = (EditText) findViewById(R.id.et_reg_phone);
        et_reg_refer_code = (EditText) findViewById(R.id.et_reg_refer_code);
        et_name = (EditText) findViewById(R.id.et_reg_name);
        et_password = (EditText) findViewById(R.id.et_reg_password);
        et_email = (EditText) findViewById(R.id.et_reg_email);
        tv_password = (TextView) findViewById(R.id.tv_reg_password);
        tv_phone = (TextView) findViewById(R.id.tv_reg_phone);
        tv_name = (TextView) findViewById(R.id.tv_reg_name);
        tv_email = (TextView) findViewById(R.id.tv_reg_email);
        btn_register = (RelativeLayout) findViewById(R.id.btnRegister);

        mDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (NetworkConnection.connectionChecking(RegisterActivity.this)){
                    attemptRegister();
                }else {
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void attemptRegister() {

        tv_phone.setText(getResources().getString(R.string.et_reg_phone_hint));
        tv_email.setText(getResources().getString(R.string.tv_login_email));
        tv_name.setText(getResources().getString(R.string.tv_reg_name_hint));
        tv_password.setText(getResources().getString(R.string.tv_reg_password));

        tv_name.setTextColor(getResources().getColor(R.color.dark_gray));
        tv_phone.setTextColor(getResources().getColor(R.color.dark_gray));
        tv_password.setTextColor(getResources().getColor(R.color.dark_gray));
        tv_email.setTextColor(getResources().getColor(R.color.dark_gray));
        et_reg_refer_code.setTextColor(getResources().getColor(R.color.dark_gray));

        String getphone = et_phone.getText().toString().trim();
        String getname = et_name.getText().toString().trim();
        String getpassword = et_password.getText().toString().trim();
        String getemail = et_email.getText().toString().trim();
        String getReferCode = et_reg_refer_code.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(getname)) {
            tv_name.setTextColor(getResources().getColor(R.color.black));
            Toast.makeText(RegisterActivity.this,"Enter Name",Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(getemail)) {
            tv_phone.setTextColor(getResources().getColor(R.color.black));
            Toast.makeText(RegisterActivity.this,"Enter Email Address",Toast.LENGTH_SHORT).show();
        }else if(!isEmailValid(getemail)) {
            Toast.makeText(RegisterActivity.this,"Enter Valid Email Address",Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(getphone)) {
            tv_phone.setTextColor(getResources().getColor(R.color.black));
            Toast.makeText(RegisterActivity.this,"Enter Mobile Number",Toast.LENGTH_SHORT).show();
        }else if(!isPhoneValid(getphone)) {
            Toast.makeText(RegisterActivity.this,"Enter Valid Mobile Number",Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(getpassword)) {
            tv_password.setTextColor(getResources().getColor(R.color.black));
            Toast.makeText(RegisterActivity.this,"Enter Password",Toast.LENGTH_SHORT).show();
        }else if (!isPasswordValid(getpassword)) {
            Toast.makeText(RegisterActivity.this,"Password Should Greater Than 6",Toast.LENGTH_SHORT).show();
        }else {
            if (NetworkConnection.connectionChecking(RegisterActivity.this)){
                makeRegisterRequest(getname, getphone, getemail, getpassword,getReferCode);
            }else {
                Toast.makeText(RegisterActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        boolean check;
        Pattern p;
        Matcher m;
        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        p = Pattern.compile(EMAIL_STRING);
        m = p.matcher(email);
        check = m.matches();
        if (!check) {
            Toast.makeText(getApplicationContext(), "Enter valid email", Toast.LENGTH_SHORT).show();
            //txtEmail.setError("Not Valid Email");
        }
        return check;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 6;
    }

    private boolean isPhoneValid(String phoneno) {
        //TODO: Replace this with your own logic
        boolean check = false;
        if ((phoneno.length() == 10 ) || (phoneno.length() > 10)) {
            check = true;
            Log.d("motest", "false");
        } else {
            Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
            Log.d("motest", "true");
            check = false;
        }
        return check;
    }

    /**
     * Method to make json object request where json response starts wtih
     */
    @SuppressLint("MissingPermission")
    private void makeRegisterRequest(String name, String mobile, String email, String password, String refercode) {
      alertDialog.show();
        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String androidID ;
        try {
            androidID = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }catch (Exception e) {
            androidID = "000000000000000";
        }
       // final String androidID =  android.provider.Settings.System.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        // Tag used to cancel the request
        String tag_json_obj = "json_register_req";
        String fcm_token = (String) SharePreferenceUtility.getPreferences(RegisterActivity.this, Const.FCM_TOKEN,SharePreferenceUtility.PREFTYPE_STRING);


        Log.e("ids",androidID + " "+ fcm_token);

        Map<String, String> params = new HashMap<String, String>();
        params.put("from_referral_code", refercode);
        params.put("user_name", name);
        params.put("user_mobile", mobile);
        params.put("user_email", email);
        params.put("password", password);
        params.put("imeino", "");
        params.put("user_fcm_code", fcm_token);
        Log.d("registerApi",params.toString());


        String url=BaseURL.REGISTER_URL
                +"?from_referral_code="+refercode
                +"&user_name="+name
                +"&user_mobile="+mobile
                +"&user_email="+email
                +"&password="+password
                +"&imeino="+""
                +"&user_fcm_code="+fcm_token;
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                Log.e("Response", response.toString());
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        String msg = response.getString("message");
                        Toast.makeText(RegisterActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                        if(getIntent() != null) {
                            i.putExtra("flag",getIntent().getIntExtra("flag",0));
                        }
                        startActivity(i);
                        finish();

                    } else {
                        String error = response.getString("error");
                        Toast.makeText(RegisterActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alertDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                error.printStackTrace();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Volley.newRequestQueue(this).add(jsonRequest);

     /*   CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST, BaseURL.REGISTER_URL,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                Log.d(TAG, response.toString());
                try {
                    Boolean status = response.getBoolean("responce");

                    if (status) {

                        String msg = response.getString("message");
                        Toast.makeText(RegisterActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(RegisterActivity.this, LoginActivity.class);

                        if(getIntent() != null)
                        {
                            i.putExtra("flag",getIntent().getIntExtra("flag",0));
                        }
                        startActivity(i);
                        finish();

                    } else {
                        String msg = response.getString("message");
                        Toast.makeText(RegisterActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                        String error = response.getString("error");
                        Toast.makeText(RegisterActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alertDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
*/
    }

}
