package com.adjit.surfcity;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class Product_Details extends AppCompatActivity {
    ImageView plus1, plus2, plus3;
    ImageView minus1, minus2, minus3;
    TextView quan1, quan2, quan3;
    TextView change1, change2;

    int count1 = 0, count2 = 0, count3 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product__details);

        minus1 = findViewById(R.id.plus1);
        minus2 = findViewById(R.id.plus2);
        minus3 = findViewById(R.id.plus3);

        plus1 = findViewById(R.id.minus1);
        plus2 = findViewById(R.id.minus2);
        plus3 = findViewById(R.id.minus3);

        quan1 = findViewById(R.id.quan1);
        quan2 = findViewById(R.id.quan2);
        quan3 = findViewById(R.id.quan3);

        change1 = findViewById(R.id.change1);
        change2 = findViewById(R.id.change2);

        change1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                news_popup();
            }
        });

        change2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                milk_popup();
            }
        });

        plus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                minus1.setVisibility(View.VISIBLE);
                count1 = count1 + 1;
                quan1.setText(String.valueOf(count1));
            }
        });
        minus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count1 == 0) {
                    minus1.setVisibility(View.INVISIBLE);
                } else {
                    count1 = count1 - 1;
                    quan1.setText(String.valueOf(count1));
                }
            }
        });

        plus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                minus2.setVisibility(View.VISIBLE);
                count2 = count2 + 1;
                quan2.setText(String.valueOf(count2));
            }
        });
        minus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count2 == 0) {
                    minus2.setVisibility(View.INVISIBLE);
                } else {
                    count2 = count2 - 1;
                    quan2.setText(String.valueOf(count2));
                }
            }
        });

        plus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                minus3.setVisibility(View.VISIBLE);
                count3 = count3 + 1;
                quan3.setText(String.valueOf(count3));
            }
        });
        minus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count3 == 0) {
                    minus3.setVisibility(View.INVISIBLE);
                } else {
                    count3 = count3 - 1;
                    quan3.setText(String.valueOf(count3));
                }
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Product Details");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void news_popup() {
        Dialog dialog = new Dialog(Product_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.newspaper_dialog);
        dialog.setCancelable(true);

        dialog.show();
    }

    private void milk_popup() {
        Dialog dialog = new Dialog(Product_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.milk_dialog);
        dialog.setCancelable(true);

        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
