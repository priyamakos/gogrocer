package Model;

/**
 * Created by Rajesh Dabhi on 26/6/2017.
 */

public class Product_model {

    String product_id;
    String product_name;
    String category_id;
    String product_description;
    String deal_price;
    String start_date;
    String start_time;
    String end_date;
    String end_time;
    String price;
    String product_image;
    String status;
    String in_stock;
    String unit_value;
    String unit;
    String increament;
    String rewards;
    String stock;
    String title;
    String surfcity_price;
    String subscription_price;
    String price1 ;
    String surfcity_price1 ;
    String subscription_price1 ;
    String unit_value1 ;
    String unit1 ;
    String price2 ;
    String surfcity_price2 ;
    String subscription_price2 ;
    String unit_value2 ;
    String unit2 ;
    Boolean isSingleItem ;


    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public String getSurfcity_price1() {
        return surfcity_price1;
    }

    public void setSurfcity_price1(String surfcity_price1) {
        this.surfcity_price1 = surfcity_price1;
    }

    public String getSubscription_price1() {
        return subscription_price1;
    }

    public void setSubscription_price1(String subscription_price1) {
        this.subscription_price1 = subscription_price1;
    }

    public String getUnit_value1() {
        return unit_value1;
    }

    public void setUnit_value1(String unit_value1) {
        this.unit_value1 = unit_value1;
    }

    public String getUnit1() {
        return unit1;
    }

    public void setUnit1(String unit1) {
        this.unit1 = unit1;
    }

    public String getPrice2() {
        return price2;
    }

    public void setPrice2(String price2) {
        this.price2 = price2;
    }

    public String getSurfcity_price2() {
        return surfcity_price2;
    }

    public void setSurfcity_price2(String surfcity_price2) {
        this.surfcity_price2 = surfcity_price2;
    }

    public String getSubscription_price2() {
        return subscription_price2;
    }

    public void setSubscription_price2(String subscription_price2) {
        this.subscription_price2 = subscription_price2;
    }

    public String getUnit_value2() {
        return unit_value2;
    }

    public void setUnit_value2(String unit_value2) {
        this.unit_value2 = unit_value2;
    }

    public String getUnit2() {
        return unit2;
    }

    public void setUnit2(String unit2) {
        this.unit2 = unit2;
    }

    public Boolean getSingleItem() {
        return isSingleItem;
    }

    public void setSingleItem(Boolean singleItem) {
        isSingleItem = singleItem;
    }

    public String getSurfcity_price() {
        return surfcity_price;
    }

    public void setSurfcity_price(String surfcity_price) {
        this.surfcity_price = surfcity_price;
    }

    public String getSubscription_price() {
        return subscription_price;
    }

    public void setSubscription_price(String subscription_price) {
        this.subscription_price = subscription_price;
    }

    public String  getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getDeal_price() {
        return deal_price;
    }

    public void setDeal_price(String deal_price) {
        this.deal_price = deal_price;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getUnit_value() {
        return unit_value;
    }

    public void setUnit_value(String unit_value) {
        this.unit_value = unit_value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIncreament() {
        return increament;
    }

    public void setIncreament(String increament) {
        this.increament = increament;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
    public String getIn_stock() {
        return in_stock;
    }

    public void setIn_stock(String in_stock) {
        this.in_stock = in_stock;
    }

}
