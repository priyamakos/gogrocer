package Model;

public class CartModel{
    private String cart_id;
    private String user_id;
    private String product_id;
    private String subscription_id;
    private String qty;
    private String product_name;
    private String product_description;
    private String product_image;
    private String category_id;
    private String in_stock;
    private String MRP;
    private String price;
    private String surfcity_price;
    private String subscription_price;
    private String cashback;
    private String unit_value;
    private String unit;
    private String increament;
    private String rewards;
    private String deal_price, deal_unit, deal_unit_value ;
    private Boolean isdealofproduct;

    public String getDeal_unit() {
        return deal_unit;
    }

    public void setDeal_unit(String deal_unit) {
        this.deal_unit = deal_unit;
    }

    public String getDeal_unit_value() {
        return deal_unit_value;
    }

    public void setDeal_unit_value(String deal_unit_value) {
        this.deal_unit_value = deal_unit_value;
    }

    public Boolean getIsdealofproduct() {
        return isdealofproduct;
    }

    public void setIsdealofproduct(Boolean isdealofproduct) {
        this.isdealofproduct = isdealofproduct;
    }

    public String getDeal_price() {
        return deal_price;
    }

    public void setDeal_price(String deal_price) {
        this.deal_price = deal_price;
    }

    // Getter Methods
    public String getCart_id() {
        return cart_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public String getQty() {
        return qty;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public String getProduct_image() {
        return product_image;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getIn_stock() {
        return in_stock;
    }

    public String getMRP() {
        return MRP;
    }

    public String getPrice() {
        return price;
    }

    public String getSurfcity_price() {
        return surfcity_price;
    }

    public String getSubscription_price() {
        return subscription_price;
    }

    public String getCashback() {
        return cashback;
    }

    public String getUnit_value() {
        return unit_value;
    }

    public String getUnit() {
        return unit;
    }

    public String getIncreament() {
        return increament;
    }

    public String getRewards() {
        return rewards;
    }

    // Setter Methods

    public void setCart_id( String cart_id ) {
        this.cart_id = cart_id;
    }

    public void setUser_id( String user_id ) {
        this.user_id = user_id;
    }

    public void setProduct_id( String product_id ) {
        this.product_id = product_id;
    }

    public void setSubscription_id( String subscription_id ) {
        this.subscription_id = subscription_id;
    }

    public void setQty( String qty ) {
        this.qty = qty;
    }

    public void setProduct_name( String product_name ) {
        this.product_name = product_name;
    }

    public void setProduct_description( String product_description ) {
        this.product_description = product_description;
    }

    public void setProduct_image( String product_image ) {
        this.product_image = product_image;
    }

    public void setCategory_id( String category_id ) {
        this.category_id = category_id;
    }

    public void setIn_stock( String in_stock ) {
        this.in_stock = in_stock;
    }

    public void setMRP( String MRP ) {
        this.MRP = MRP;
    }

    public void setPrice( String price ) {
        this.price = price;
    }

    public void setSurfcity_price( String surfcity_price ) {
        this.surfcity_price = surfcity_price;
    }

    public void setSubscription_price( String subscription_price ) {
        this.subscription_price = subscription_price;
    }

    public void setCashback( String cashback ) {
        this.cashback = cashback;
    }

    public void setUnit_value( String unit_value ) {
        this.unit_value = unit_value;
    }

    public void setUnit( String unit ) {
        this.unit = unit;
    }

    public void setIncreament( String increament ) {
        this.increament = increament;
    }

    public void setRewards( String rewards ) {
        this.rewards = rewards;
    }
}