package Model;

public class SubscriptionModel1 {
    Subscription_model model;
    String sub_id;

    public Subscription_model getModel() {
        return model;
    }

    public void setModel(Subscription_model model) {
        this.model = model;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }
}
