package Model;

public class WalletHistroy {

    private String status;
    private String amount;
    private String trancdate;

    public WalletHistroy() {
    }

    public WalletHistroy(String status, String amount, String trancdate) {
        this.status = status;
        this.amount = amount;
        this.trancdate = trancdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTrancdate() {
        return trancdate;
    }

    public void setTrancdate(String trancdate) {
        this.trancdate = trancdate;
    }

    @Override
    public String toString() {
        return "WalletHistroy{" +
                "status='" + status + '\'' +
                ", amount='" + amount + '\'' +
                ", trancdate='" + trancdate + '\'' +
                '}';
    }
}
