package Model;

/**
 * Created by PC1 on 28-02-2019.
 */

public class Subscription_model
{
    private String subscription_id;
    private String subscription_name;
    private String subscription_days;
    private String subscription_price;
    private String subscription_image;
    private boolean isSelected;
    private String subscription_details1;
    private String sub_id;

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }
     /*  private String min_limit_apply;
    private String max_limit_apply;

    private String is_active;*/

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public String getSubscription_name() {
        return subscription_name;
    }

    public void setSubscription_name(String subscription_name) {
        this.subscription_name = subscription_name;
    }

    public String getSubscription_days() {
        return subscription_days;
    }

    public void setSubscription_days(String subscription_days) {
        this.subscription_days = subscription_days;
    }

    public String getSubscription_price() {
        return subscription_price;
    }

    public void setSubscription_price(String subscription_price) {
        this.subscription_price = subscription_price;
    }

    public String getSubscription_image() {
        return subscription_image;
    }

    public void setSubscription_image(String subscription_image) {
        this.subscription_image = subscription_image;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getSubscription_details1() {
        return subscription_details1;
    }

    public void setSubscription_details1(String subscription_details1) {
        this.subscription_details1 = subscription_details1;
    }

    public String getSubscription_details2() {
        return subscription_details2;
    }

    public void setSubscription_details2(String subscription_details2) {
        this.subscription_details2 = subscription_details2;
    }

    public String getSubscription_details3() {
        return subscription_details3;
    }

    public void setSubscription_details3(String subscription_details3) {
        this.subscription_details3 = subscription_details3;
    }

    private String subscription_details2;
    private String subscription_details3;
/*
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getPlan_duration() {
        return plan_duration;
    }

    public void setPlan_duration(String plan_duration) {
        this.plan_duration = plan_duration;
    }

    public String getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(String original_price) {
        this.original_price = original_price;
    }

    public String getOffer_pack() {
        return offer_pack;
    }

    @Override
    public String toString() {
        return "Subscription_model{" +
                "plan_id='" + plan_id + '\'' +
                ", plan_name='" + plan_name + '\'' +
                ", plan_duration='" + plan_duration + '\'' +
                ", original_price='" + original_price + '\'' +
                ", offer_pack='" + offer_pack + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }

    public void setOffer_pack(String offer_pack) {
        this.offer_pack = offer_pack;
    }*/

    @Override
    public String toString() {
        return "Subscription_model{" +
                "subscription_id='" + subscription_id + '\'' +
                ", subscription_name='" + subscription_name + '\'' +
                ", subscription_days='" + subscription_days + '\'' +
                ", subscription_price='" + subscription_price + '\'' +
                ", subscription_image='" + subscription_image + '\'' +
                ", isSelected=" + isSelected +
                ", subscription_details1='" + subscription_details1 + '\'' +
                ", subscription_details2='" + subscription_details2 + '\'' +
                ", subscription_details3='" + subscription_details3 + '\'' +
                '}';
    }
}
