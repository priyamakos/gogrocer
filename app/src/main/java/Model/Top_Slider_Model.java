package Model;

/**
 * Created by PC1 on 27-02-2019.
 */

public class Top_Slider_Model
{
    private String id;
    private String slider_title;
    private String slider_url;
    private String sub_cat;
    private String slider_status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlider_title() {
        return slider_title;
    }

    public void setSlider_title(String slider_title) {
        this.slider_title = slider_title;
    }

    public String getSlider_url() {
        return slider_url;
    }

    public void setSlider_url(String slider_url) {
        this.slider_url = slider_url;
    }

    public String getSub_cat() {
        return sub_cat;
    }

    public void setSub_cat(String sub_cat) {
        this.sub_cat = sub_cat;
    }

    public String getSlider_status() {
        return slider_status;
    }

    public void setSlider_status(String slider_status) {
        this.slider_status = slider_status;
    }
}
