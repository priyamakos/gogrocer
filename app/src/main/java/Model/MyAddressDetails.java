package Model;

import java.io.Serializable;

public class MyAddressDetails implements Serializable {

    String city, postal_code, state, street, lat, lng, mobilenumber;

    public MyAddressDetails() {
    }

    public MyAddressDetails(String city, String postal_code, String state, String street, String lat, String lng, String mobilenumber) {
        this.city = city;
        this.postal_code = postal_code;
        this.state = state;
        this.street = street;
        this.lat = lat;
        this.lng = lng;
        this.mobilenumber = mobilenumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }
}
