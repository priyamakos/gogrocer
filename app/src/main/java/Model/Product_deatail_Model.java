package Model;

public class Product_deatail_Model{
	private String product_image;
	private String surfcity_price;
	private String subscription_price;
	private String MRP;
	private String increament;
	private String in_stock;
	private String product_name;
	private String cashback;
	private String unit;
	private String category_id;
	private String price;
	private String product_id;
	private String unit_value;
	private String product_description;
	private String qty;
	private String deal_price;
	private String price1 ;
	private String surfcity_price1 ;
	private String subscription_price1 ;
	private String unit_value1 ;
	private String unit1 ;
	private String price2 ;
	private String surfcity_price2 ;
	private String subscription_price2 ;
	private String unit_value2 ;
	private String unit2 ;
	private Boolean isSingleItem ;
	private int cart_count, cart_count1, cart_count2;
	private String deal_unit, deal_unit_value, deal_cart_count ;

	public String getDeal_unit() {
		return deal_unit;
	}

	public void setDeal_unit(String deal_unit) {
		this.deal_unit = deal_unit;
	}

	public String getDeal_unit_value() {
		return deal_unit_value;
	}

	public void setDeal_unit_value(String deal_unit_value) {
		this.deal_unit_value = deal_unit_value;
	}

	public String getDeal_cart_count() {
		return deal_cart_count;
	}

	public void setDeal_cart_count(String deal_cart_count) {
		this.deal_cart_count = deal_cart_count;
	}

	public int getCart_count() {
		return cart_count;
	}

	public void setCart_count(int cart_count) {
		this.cart_count = cart_count;
	}

	public int getCart_count1() {
		return cart_count1;
	}

	public void setCart_count1(int cart_count1) {
		this.cart_count1 = cart_count1;
	}

	public int getCart_count2() {
		return cart_count2;
	}

	public void setCart_count2(int cart_count2) {
		this.cart_count2 = cart_count2;
	}

	public String getDeal_price() {
		return deal_price;
	}

	public void setDeal_price(String deal_price) {
		this.deal_price = deal_price;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "Product_deatail_Model{" +
				"product_image='" + product_image + '\'' +
				", surfcity_price='" + surfcity_price + '\'' +
				", subscription_price='" + subscription_price + '\'' +
				", MRP='" + MRP + '\'' +
				", increament='" + increament + '\'' +
				", in_stock='" + in_stock + '\'' +
				", product_name='" + product_name + '\'' +
				", cashback='" + cashback + '\'' +
				", unit='" + unit + '\'' +
				", category_id='" + category_id + '\'' +
				", price='" + price + '\'' +
				", product_id='" + product_id + '\'' +
				", unit_value='" + unit_value + '\'' +
				", product_description='" + product_description + '\'' +
				", rewards='" + rewards + '\'' +
				", qty='" + qty + '\'' +
				'}';
	}

	private String rewards;

	public String getPrice1() {
		return price1;
	}

	public void setPrice1(String price1) {
		this.price1 = price1;
	}

	public String getSurfcity_price1() {
		return surfcity_price1;
	}

	public void setSurfcity_price1(String surfcity_price1) {
		this.surfcity_price1 = surfcity_price1;
	}

	public String getSubscription_price1() {
		return subscription_price1;
	}

	public void setSubscription_price1(String subscription_price1) {
		this.subscription_price1 = subscription_price1;
	}

	public String getUnit_value1() {
		return unit_value1;
	}

	public void setUnit_value1(String unit_value1) {
		this.unit_value1 = unit_value1;
	}

	public String getUnit1() {
		return unit1;
	}

	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}

	public String getPrice2() {
		return price2;
	}

	public void setPrice2(String price2) {
		this.price2 = price2;
	}

	public String getSurfcity_price2() {
		return surfcity_price2;
	}

	public void setSurfcity_price2(String surfcity_price2) {
		this.surfcity_price2 = surfcity_price2;
	}

	public String getSubscription_price2() {
		return subscription_price2;
	}

	public void setSubscription_price2(String subscription_price2) {
		this.subscription_price2 = subscription_price2;
	}

	public String getUnit_value2() {
		return unit_value2;
	}

	public void setUnit_value2(String unit_value2) {
		this.unit_value2 = unit_value2;
	}

	public String getUnit2() {
		return unit2;
	}

	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}

	public Boolean getSingleItem() {
		return isSingleItem;
	}

	public void setSingleItem(Boolean singleItem) {
		isSingleItem = singleItem;
	}

	public String getProduct_image() {
		return product_image;
	}

	public void setProduct_image(String product_image) {
		this.product_image = product_image;
	}

	public String getSurfcity_price() {
		return surfcity_price;
	}

	public void setSurfcity_price(String surfcity_price) {
		this.surfcity_price = surfcity_price;
	}

	public String getSubscription_price() {
		return subscription_price;
	}

	public void setSubscription_price(String subscription_price) {
		this.subscription_price = subscription_price;
	}

	public String getMRP() {
		return MRP;
	}

	public void setMRP(String MRP) {
		this.MRP = MRP;
	}

	public String getIncreament() {
		return increament;
	}

	public void setIncreament(String increament) {
		this.increament = increament;
	}

	public String getIn_stock() {
		return in_stock;
	}

	public void setIn_stock(String in_stock) {
		this.in_stock = in_stock;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getCashback() {
		return cashback;
	}

	public void setCashback(String cashback) {
		this.cashback = cashback;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getUnit_value() {
		return unit_value;
	}

	public void setUnit_value(String unit_value) {
		this.unit_value = unit_value;
	}

	public String getProduct_description() {
		return product_description;
	}

	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}

	public String getRewards() {
		return rewards;
	}

	public void setRewards(String rewards) {
		this.rewards = rewards;
	}
}
