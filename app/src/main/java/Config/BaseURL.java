package Config;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseURL {
    static final String APP_NAME = "Grocery";
    public static final String PREFS_NAME = "GroceryLoginPrefs";
    public static final String PREFS_NAME2 = "GroceryLoginPrefs2";
    public static final String IS_LOGIN = "isLogin";
    public static final String KEY_NAME = "user_fullname";
    public static final String KEY_EMAIL = "user_email";
    public static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
    public static final String KEY_ID = "user_id";
    public static final String KEY_MOBILE = "user_phone";
    public static final String KEY_IMAGE = "user_image";
    public static final String KEY_WALLET_Ammount = "wallet_ammount";
    public static final String KEY_REWARDS_POINTS = "rewards_points";
    public static final String KEY_PAYMENT_METHOD = "payment_method";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_SOCITY_ID = "Socity_id";
    public static final String KEY_REWARDS = "rewards";
    public static final String KEY_SOCITY_NAME = "socity_name";
    public static final String KEY_HOUSE = "house_no";
    public static final String KEY_DATE = "date";
    public static final String KEY_TIME = "time";

    //Store Selection
    public static final String KEY_STORE_COUNT = "STORE_COUNT";
    public static final String KEY_NOTIFICATION_COUNT = "NOTIFICATION_COUNT";

    //Firebase
    public static final String SHARED_PREF = "ah_firebase";
    public static final String TOPIC_GLOBAL = "global";
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String KEY_PASSWORD = "password";

    //City and Store Id
    public static final String CITY_ID = "CITY_ID";
    public static final String STORE_ID = "1";

    // subscription id
    public static String sub_id="0";

//    public static String BASE_URL = "https://adjinfotech.com/ramesh_projects/surf_city_phase2/";
    public static String BASE_URL = "http://oradaily.in/admin/";

    public static String UPDATE_WALLET_URL = BASE_URL+"index.php/Api/wallet_history";
    public static String VERFY_COUPAN_URL = BASE_URL+"index.php/Api/get_coupons";

    public static String IMG_SLIDER_URL = BASE_URL + "uploads/sliders/";
    public static String IMG_CATEGORY_URL = BASE_URL + "uploads/category/";
    public static String IMG_SHOP_URL = BASE_URL + "uploads/shop/";
    public static String IMG_PRODUCT_URL = BASE_URL + "uploads/products/";

    public static String GET_SUBSCRIPTION_URL = BASE_URL + "index.php/Api/subscriptionList";
    public static String IMG_PROFILE_URL = BASE_URL + "uploads/profile/";
    public static String GET_SLIDER_URL = BASE_URL + "index.php/api/get_sliders";
    public static String GET_FEAATURED_SLIDER_URL = BASE_URL + "index.php/api/get_feature_banner";
    public static String GET_BANNER_URL = BASE_URL + "index.php/api/get_banner";

    public static String WALLET_REFRESH = BASE_URL + "index.php/api/wallet?user_id=";
    public static String REWARDS_REFRESH = BASE_URL + "index.php/api/rewards?user_id=";
    public static String CART_REMOVE = BASE_URL + "index.php/api/subscriptionremove";

    public static String GET_CATEGORY_URL = BASE_URL + "index.php/api/get_categories";//get_categories    categoriesList
    public static String GET_PRODUCT_URL = BASE_URL + "index.php/api/selectProductlist";//get_products

    public static String GET_SLIDER_CATEGORY_URL = BASE_URL + "index.php/api/get_sub_cat";
    public static String GET_CATEGORY_ICON_URL = BASE_URL + "index.php/api/icon";

    //Home PAGE
    public static String GET_MENU_PRODUCTS = BASE_URL + "index.php/api/icon";
    public static String GET_MENU_ICON_PRODUCT_URL = BASE_URL + "index.php/api/get_header_products";
    public static String GET_DEAL_OF_DAY_PRODUCTS = BASE_URL + "index.php/api/deal_product";
    public static String GET_ALL_DEAL_OF_DAY_PRODUCTS = BASE_URL + "index.php/api/get_all_deal_product";

    public static String GET_TOP_SELLING_PRODUCTS = BASE_URL + "index.php/api/top_selling_product";
    public static String GET_ALL_TOP_SELLING_PRODUCTS = BASE_URL + "index.php/Api/top_selling_product";

   // public static String GET_PRODUCT_URL = BASE_URL + "index.php/api/get_products";
    public static String GET_PRODUCT_DETAILS = BASE_URL + "index.php/Api/productDetails";
    public static String ADD_TO_CART = BASE_URL + "index.php/Api/add_cart"; //add_cart123
    public static String CART_LIST = BASE_URL + "index.php/Api/get_cart_list_dev"; //get_cart_list
    public static String CART_DELETE= BASE_URL + "index.php/Api/cart_delete";
    public static String GET_ABOUT_URL = BASE_URL + "index.php/api/aboutus";

    public static String GET_SUPPORT_URL = BASE_URL + "index.php/api/support";

    public static String GET_TERMS_URL = BASE_URL + "index.php/api/terms";

    public static String GET_TIME_SLOT_URL = BASE_URL + "index.php/api/deliver_timing";//get_time_slot

    public static String LOGIN_URL = BASE_URL + "index.php/api/login";

    public static String LOGIN_WITH_GMAIL_URL = BASE_URL + "index.php/api/loginwithgoogle";

    public static String LOGIN_WITH_FB_URL = BASE_URL + "index.php/api/loginwithfacebook";

    public static String REGISTER_URL = BASE_URL + "index.php/Api/signup";

    public static String GET_SOCITY_URL = BASE_URL + "index.php/api/get_society";

    public static String EDIT_PROFILE_URL = BASE_URL + "index.php/api/update_userdata";

    public static String ADD_ORDER_URL = BASE_URL + "index.php/api/send_order_dev"; //send_order

    public static String GET_ORDER_URL = BASE_URL + "index.php/api/my_orders";

    public static String GET_DELIVERD_ORDER_URL = BASE_URL + "index.php/api/delivered_complete";

    public static String ORDER_DETAIL_URL = BASE_URL + "index.php/api/order_details";

    public static String DELETE_ORDER_URL = BASE_URL + "index.php/api/cancel_order";

    public static String GET_LIMITE_SETTING_URL = BASE_URL + "index.php/api/get_limit_settings";

    public static String ADD_ADDRESS_URL = BASE_URL + "index.php/api/add_address";

    public static String GET_ADDRESS_URL = BASE_URL + "index.php/api/get_address";

    public static String FORGOT_URL = BASE_URL + "index.php/api/forgot_password";

    public static String JSON_RIGISTER_FCM = BASE_URL + "index.php/api/register_fcm";

    public static String CHANGE_PASSWORD_URL = BASE_URL + "index.php/api/change_password";

    public static String DELETE_ADDRESS_URL = BASE_URL + "index.php/api/delete_address";

    public static String EDIT_ADDRESS_URL = BASE_URL + "index.php/api/edit_address";

    public static String REFERAL_CODE_URL = BASE_URL + "index.php/api/get_referral_code";

    public static String WALLET_AMOUNT_URL = BASE_URL + "index.php/Api/wallet";
    //"https://adjinfotech.com/ramesh_projects/surf_city/index.php/Api/wallet?user_id=2147483647";

    public static String SUBSCRIBE_DETAILS_URL = BASE_URL + "index.php/Api/subscribeDetails";
    public static String GET_CART_COUNT=  BASE_URL + "index.php/Api/cart_count";

    //update  tid
    public static String edit_order_payment=  BASE_URL + "index.php/Api/edit_order_payment";

    //search
    public static String PRODUCT_SUGGESTION_URL= BASE_URL+"index.php/Api/get_products_suggestion";

    //shop list
    public static String SHOP_LIST = BASE_URL+"index.php/Api/shopList";
    //shop categories
    public static String SHOP_CAT = BASE_URL+"index.php/Api/ShopcategoriesList";

    //purchase plan
    public static String PURCHASE_PLAN = BASE_URL+"index.php/Api/purchase_subscribe";
    //purchase plan
    public static String  SUBSCRIPTION_DETAILS = BASE_URL+"index.php/Api/get_purchase_subscribe_details";

    //Subscription Image URL

    // global topic to receive app wide push notifications
    public static String  SUBSCRIPTION_IMAGE_URL = BASE_URL+"uploads/subscription/";
    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String DownloadCoupenCode = BASE_URL+"index.php/Api/coupons";
}
