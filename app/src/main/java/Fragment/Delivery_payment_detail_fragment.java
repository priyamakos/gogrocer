package Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import Config.BaseURL;
import Config.SharedPref;

import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.R;

import okhttp3.internal.Util;
import util.ConnectivityReceiver;
import util.DatabaseHandler;
import util.Session_management;
import util.Utils;

/**
 * Created by Rajesh Dabhi on 29/6/2017.
 */

public class Delivery_payment_detail_fragment extends Fragment {

    private static String TAG = Delivery_payment_detail_fragment.class.getSimpleName();

    private TextView tv_timeslot, tv_address, tv_total;
    private LinearLayout btn_order;

    private String getlocation_id = "";
    private String gettime = "";
    private String getdate = "";
    private String getuser_id = "";
    private String getstore_id = "", cashback, coupons_id, total_product_price, society_id, delivery_charge;
    private String sub_plan;
    private int deli_charges;
    String total,total_amout,coupon_amt,wallet_amt_t;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    private Session_management sessionManagement;

    public Delivery_payment_detail_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_order, container, false);
            setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setTitle("Order Details");

        sessionManagement = new Session_management(getActivity());

        tv_timeslot = (TextView) view.findViewById(R.id.textTimeSlot);
        tv_address = (TextView) view.findViewById(R.id.txtAddress);
        //tv_item = (TextView) view.findViewById(R.id.textItems);
        //tv_total = (TextView) view.findViewById(R.id.textPrice);
        tv_total = (TextView) view.findViewById(R.id.txtTotal);
        btn_order = (LinearLayout) view.findViewById(R.id.btn_order_now);

        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        getdate = getArguments().getString("getdate");
        gettime = getArguments().getString("time");
        getlocation_id = getArguments().getString("location_id");
        getstore_id = getArguments().getString("store_id");

        cashback = getArguments().getString("cashback");
        coupons_id = getArguments().getString("coupons_id");
        total_product_price = getArguments().getString("total_product_price");
        delivery_charge = getArguments().getString("delivery_charge");
        society_id = getArguments().getString("society_id");

        //deli_charges = Integer.parseInt(getArguments().getString("deli_charges"));
        String getaddress = getArguments().getString("address");

        String start = gettime.trim().split("-")[0];
        String end = gettime.trim().split("-")[1];

        try {
            tv_timeslot.setText(getdate + " " + start.trim()+ " - " + end.trim());
        }catch (Exception e){
            tv_timeslot.setText(getdate + " " +gettime);
        }
        tv_address.setText(getaddress);

        total = getArguments().getString("total");
        total_amout=getArguments().getString("total_amout");
        coupon_amt=getArguments().getString("coupon_amt");
        wallet_amt_t=getArguments().getString("wallet_amt_t");
        sub_plan=getArguments().getString("sub_plan");

        tv_total.setText(getResources().getString(R.string.tv_cart_item)+sessionManagement.getCartCount()+"\n"+
                getResources().getString(R.string.amount)+total);

        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Fragment fm = new Payment_fragment();
                    Bundle args = new Bundle();
                    args.putString("total", String.valueOf(total));
                    args.putString("total_amout",total_amout);
                    args.putString("coupon_amt",coupon_amt);
                    args.putString("wallet_amt_t",wallet_amt_t);
                    args.putString("getdate", getdate);
                    args.putString("gettime", gettime);
                    args.putString("getlocationid", getlocation_id);
                    args.putString("getstoreid", getstore_id);
                    args.putString("sub_plan", sub_plan);

                    args.putString("cashback", cashback);
                    args.putString("coupons_id", coupons_id);
                    args.putString("total_product_price", total_product_price);
                    args.putString("society_id", society_id);
                    args.putString("delivery_charge", delivery_charge);
                    fm.setArguments(args);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                    SharedPref.putString(getActivity(),BaseURL.TOTAL_AMOUNT, String.valueOf(total));

            }
        });

        return view;
    }

    @Override public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setVisible(false);
    }

}
