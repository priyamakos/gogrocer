package Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import Config.BaseURL;
import com.adjit.surfcity.AppController;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.R;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.Session_management;

/**
 * Created by Rajesh Dabhi on 6/7/2017.
 */

public class Add_delivery_address_fragment extends Fragment implements View.OnClickListener {

    private static String TAG = Add_delivery_address_fragment.class.getSimpleName();

    private EditText et_phone, et_name, et_pin, et_house;
    private RelativeLayout btn_update;
    private TextView tv_phone, tv_name, tv_pin, tv_house, tv_socity, btn_socity;
    private String getsocity = "";
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    private Session_management sessionManagement;

    private boolean isEdit = false;

    private String getlocation_id,getpincode,getsocity_id;
    private String getsocityPincode, getsocityId;

    public Add_delivery_address_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_delivery_address, container, false);

        ((MainActivity) getActivity()).setTitle("Address");

        sessionManagement = new Session_management(getActivity());

        et_phone = (EditText) view.findViewById(R.id.et_add_adres_phone);
        et_name = (EditText) view.findViewById(R.id.et_add_adres_name);
        tv_phone = (TextView) view.findViewById(R.id.tv_add_adres_phone);
        tv_name = (TextView) view.findViewById(R.id.tv_add_adres_name);
        //tv_pin = (TextView) view.findViewById(R.id.tv_add_adres_pin);
        //et_pin = (EditText) view.findViewById(R.id.et_add_adres_pin);
        et_house = (EditText) view.findViewById(R.id.et_add_adres_home);
        tv_house = (TextView) view.findViewById(R.id.tv_add_adres_home);
        tv_socity = (TextView) view.findViewById(R.id.tv_add_adres_socity);
        btn_update = (RelativeLayout) view.findViewById(R.id.btn_add_adres_edit);
        btn_socity = (TextView) view.findViewById(R.id.btn_add_adres_socity);

        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        getpincode = sessionManagement.getUserDetails().get(BaseURL.KEY_PINCODE);
        getsocity_id = sessionManagement.getUserDetails().get(BaseURL.KEY_SOCITY_ID);

        Bundle args = getArguments();

        if (args != null) {
            getlocation_id = getArguments().getString("location_id");
            String get_name = getArguments().getString("name");
            String get_phone = getArguments().getString("mobile");
            getsocityPincode = getArguments().getString("pincode");
            getsocityId = getArguments().getString("socity_id");
            String get_pincode = getArguments().getString("pincode");
            String get_house = getArguments().getString("house");

            Log.e("searchpiconde",getArguments().getString("location_id")+" "+getArguments().getString("name")+" "+getArguments().getString("mobile")+" "+getArguments().getString("socity_id")+" "+getArguments().getString("pincode")+"  "+getArguments().getString("house"));

            if (TextUtils.isEmpty(get_name) && get_name == null) {
                isEdit = false;
            } else {
                isEdit = true;
//                Toast.makeText(getActivity(), "edit", Toast.LENGTH_SHORT).show();
                et_name.setText(get_name);
                et_phone.setText(get_phone);
               // et_pin.setText(get_pine);
                et_house.setText(get_house);
                btn_socity.setText(getsocityPincode);
              //  sessionManagement.updateSocity(get_pincode, get_socity_id);
            }
        }

        if (!TextUtils.isEmpty(getpincode) ) {
            btn_socity.setText(sessionManagement.getUserDetails().get(BaseURL.KEY_PINCODE));
            //sessionManagement.updateSocity(getpincode, getsocity_id);
        }
        btn_update.setOnClickListener(this);
        btn_socity.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.btn_add_adres_edit) {
            attemptEditProfile();
        }else if (id == R.id.btn_add_adres_socity) {
           //String getpincode = et_pin.getText().toString();
           //if(!TextUtils.isEmpty(getpincode)) {
            Bundle args = new Bundle();
            Fragment fm = new Socity_fragment();
            args.putString("pincode", getpincode);
            fm.setArguments(args);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
       // } else {
           // Toast.makeText(getActivity(), getResources().getString(R.string.please_enter_pincode), Toast.LENGTH_SHORT).show();
       // }
        }
    }

    private void attemptEditProfile() {
        String getphone = et_phone.getText().toString().trim();
        String getname = et_name.getText().toString().trim();
//        String getpin = et_pin.getText().toString();
        String gethouse = et_house.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(getname)) {
            cancel = true;
            Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(getphone)) {
            cancel = true;
            Toast.makeText(getActivity(), "Enter Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (!isPhoneValid(getphone)) {
            cancel = true;
            Toast.makeText(getActivity(), "Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(getpincode) && btn_socity.getText().equals("Choose Pincode")){
            Toast.makeText(getActivity(), "Select Pincode", Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (TextUtils.isEmpty(gethouse)) {
            Toast.makeText(getActivity(), "Enter Address", Toast.LENGTH_SHORT).show();
            cancel = true;
        }
        if (!cancel) {
            if (NetworkConnection.connectionChecking(getActivity())){
                String user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
                getsocityPincode = sessionManagement.getUserDetails().get(BaseURL.KEY_PINCODE);
                getsocityId = sessionManagement.getUserDetails().get(BaseURL.KEY_SOCITY_ID);
                Log.e("getscority",getsocityPincode+" "+getsocityId);
                // check internet connection
                if (NetworkConnection.connectionChecking(getActivity())){
                    if (isEdit) {
                        if(TextUtils.isEmpty(getsocityPincode) || getsocityPincode == null){
                            getsocityPincode = getArguments().getString("pincode");
                        }
                        if(TextUtils.isEmpty(getsocityId) || getsocityId == null) {
                            getsocityId = getArguments().getString("socity_id");
                        }
                        makeEditAddressRequest(getlocation_id, getsocityPincode,
                                getsocityId, gethouse, getname, getphone);
                    } else {
                        makeAddAddressRequest(user_id, getsocityPincode, getsocityId, gethouse, getname, getphone);
                    }
                }else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isPhoneValid(String phoneno) {
        //TODO: Replace this with your own logic
        return phoneno.length() > 9;
    }

    /**
     * Method to make json object request where json response starts wtih
     */
    private void makeAddAddressRequest(String user_id, String pincode, String socity_id, String house_no, String receiver_name, String receiver_mobile) {
        alertDialog.show();
        // Tag used to cancel the request
        String tag_json_obj = "json_add_address_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        params.put("pincode", pincode);
        params.put("socity_id", socity_id);
        params.put("house_no", house_no);
        params.put("receiver_name", receiver_name);
        params.put("receiver_mobile", receiver_mobile);

        Log.e("params", params.toString());
        String url =  BaseURL.ADD_ADDRESS_URL
                +"?user_id="+user_id
                +"&pincode="+pincode
                +"&socity_id="+socity_id
                +"&house_no="+house_no
                +"&receiver_name="+receiver_name
                +"&receiver_mobile="+receiver_mobile;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.e("response", response.toString());
                alertDialog.dismiss();
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        Log.e("res", String.valueOf(response));
                        ((MainActivity) getActivity()).onBackPressed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        queue.add(jsonRequest);
    }

    /**
     * Method to make json object request where json response starts wtih
     */
    private void makeEditAddressRequest(String location_id, String pincode, String socity_id, String house_no, String receiver_name, String receiver_mobile) {
        alertDialog.show();
        // Tag used to cancel the request
        String tag_json_obj = "json_edit_address_req";

        Map<String, String> params = new HashMap<String, String>();
        params.put("location_id", location_id);
        params.put("pincode", pincode);
        params.put("socity_id", socity_id);
        params.put("house_no", house_no);
        params.put("receiver_name", receiver_name);
        params.put("receiver_mobile", receiver_mobile);

        String url =  BaseURL.EDIT_ADDRESS_URL+"?location_id="+location_id
                +"&pincode="+pincode
                +"&socity_id="+socity_id
                +"&house_no="+house_no
                +"&receiver_name="+receiver_name
                +"&receiver_mobile="+receiver_mobile;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.e("response", response.toString());
                alertDialog.dismiss();
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        String msg = response.getString("data");
                        Log.e("res", String.valueOf(response));
                        Toast.makeText(getActivity(), "" + msg, Toast.LENGTH_SHORT).show();
                        ((MainActivity) getActivity()).onBackPressed();
                    }
                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);
    }

}
