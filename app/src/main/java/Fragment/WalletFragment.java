package Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Adapter.WalletHistroyAdapter;
import Config.BaseURL;
import Config.SharedPref;

import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.R;
import com.adjit.surfcity.RechargeWallet;

import java.util.ArrayList;

import Model.WalletHistroy;
import util.ConnectivityReceiver;
import util.Session_management;

/**
 * Created by Rajesh Dabhi on 29/6/2017.
 */

public class WalletFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener {

    private static String TAG = WalletFragment.class.getSimpleName();
    String userId = "00";
    TextView Wallet_Ammount,tv_recent;
    RelativeLayout Recharge_Wallet;
    private Session_management sessionManagement;
    ArrayList<WalletHistroy> walletHistroyArrayList;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    StaggeredGridLayoutManager layoutManager ;
    Context mContext;
    String wallet_limit_min, wallet_limit_max, wallet_amt;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout app_bar;
    private LinearLayout llwallet_amount;

    public WalletFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_wallet_ammount, container, false);
//        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.tv_app_name));

        mContext = getActivity();
        setHasOptionsMenu(true);
        Wallet_Ammount = view.findViewById(R.id.wallet_ammount);
        recyclerView = view.findViewById(R.id.wallet_recycler);
        app_bar = view.findViewById(R.id.app_bar);
        llwallet_amount = view.findViewById(R.id.wallet_amount);

        toolbar = view.findViewById(R.id.toolbar);
//        ((MainActivity)getContext()).setSupportActionBar(toolbar);
//        ((MainActivity)getContext()).getSupportActionBar().setDisplayShowHomeEnabled(true);
//        ((MainActivity)getContext()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) view.findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle("");
        app_bar.addOnOffsetChangedListener(this);
        
        mDialogBuilder = new AlertDialog.Builder(mContext);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

//        tv_recent=view.findViewById(R.id.tv_recent);

        walletHistroyArrayList = new ArrayList<>();
        layoutManager =  new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new WalletHistroyAdapter(walletHistroyArrayList, mContext);
        recyclerView.setAdapter(adapter);

        sessionManagement = new Session_management(getActivity());
        if (sessionManagement.isLoggedIn()) {
            userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        }
        if (NetworkConnection.connectionChecking(getActivity())){
            Wallet_amount_api();
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home :
                Fragment fm = new Home_fragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void Wallet_amount_api() {

        if (NetworkConnection.connectionChecking(getActivity())) {
            alertDialog.show();
            String wallet_amt_url = BaseURL.WALLET_AMOUNT_URL + "?user_id=" + userId;

            Log.e("wallet_amt_url", wallet_amt_url);

            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                    wallet_amt_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("wallet_amt_res", response);
                    alertDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String success_status = jsonObject.getString("success");

                        if (success_status.equals("success")) {
                            wallet_amt = jsonObject.getString("wallet");
                            Wallet_Ammount.setText(wallet_amt);
                            JSONArray jsonArray = jsonObject.getJSONArray("details");
                            JSONObject jsonDetails = jsonArray.getJSONObject(0);
                            wallet_limit_min = jsonDetails.getString("wallet_limit_min");
                            wallet_limit_max = jsonDetails.getString("wallet_limit_max");

                            JSONArray jsonWalletHistory = jsonObject.getJSONArray("wallet_history");
                            if (jsonWalletHistory.length() == 0) {
//                                tv_recent.setVisibility(View.GONE);
                            } else {
//                                tv_recent.setVisibility(View.VISIBLE);
                            }
                            if (jsonWalletHistory.length() > 0) {
                                for (int i = 0; i < jsonWalletHistory.length(); i++) {
                                    JSONObject jsonWalletDetails = jsonWalletHistory.getJSONObject(i);
                                    WalletHistroy walletHistroy = new WalletHistroy();
                                    walletHistroy.setAmount(jsonWalletDetails.getString("amount"));
                                    walletHistroy.setStatus(jsonWalletDetails.getString("Status"));
                                    walletHistroy.setTrancdate(jsonWalletDetails.getString("created_at"));
                                    walletHistroyArrayList.add(walletHistroy);
                                }
                                adapter.notifyDataSetChanged();
                            }else {
                                Toast.makeText(mContext, "No Wallet History", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(mContext, "No wallet amt", Toast.LENGTH_SHORT).show();
                            Wallet_Ammount.setText("0");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertDialog.dismiss();
                }
            });
            rq.add(stringRequest);

        }
    }

    public void getRefresrh() {
        String user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        RequestQueue rq = Volley.newRequestQueue(getActivity());
        StringRequest strReq = new StringRequest(Request.Method.GET, BaseURL.WALLET_REFRESH + user_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObj = new JSONObject(response);
                            if (jObj.optString("success").equalsIgnoreCase("success")) {
                                String wallet_amount = jObj.getString("wallet");
                                Wallet_Ammount.setText(wallet_amount);
                                SharedPref.putString(getActivity(), BaseURL.KEY_WALLET_Ammount, wallet_amount);
                            } else {
                                // Toast.makeText(DashboardPage.this, "" + jObj.optString("msg"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

        };
        rq.add(strReq);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        double percentage = (double) Math.abs(verticalOffset) / toolbar.getHeight();
        Log.i("offset", String.valueOf(percentage));
        if (percentage > 0.5) {
            //expanding
            llwallet_amount.setVisibility(View.GONE);
            ((MainActivity) getActivity()).setTitle("Wallet History");
//            collapsingToolbarLayout.setTitle("Wallet History");
        } else {
            llwallet_amount.setVisibility(View.VISIBLE);
//            collapsingToolbarLayout.setTitle("");
            ((MainActivity) getActivity()).setTitle("");
        }
    }

    @Override public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setVisible(false);
    }
}