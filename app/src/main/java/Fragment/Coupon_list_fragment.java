package Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.adjit.surfcity.AppController;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapter.Coupon_adapter;
import Adapter.Shop_Now_adapter;
import Config.BaseURL;
import Model.Coupon_list_model;
import Model.ShopNow_model;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.RecyclerTouchListener;
import util.Session_management;

public class Coupon_list_fragment extends Fragment {
    private static String TAG = Coupon_list_fragment.class.getSimpleName();
    RecyclerView recycler;
    public ArrayList<Coupon_list_model> coupon_list_model=new ArrayList<>();
    RecyclerView.Adapter coupon_adapter;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    Coupon_adapter adapter ;
    onCoupenCodeDownloaded listner ;
    private Session_management sessionManagement;
    String userId,coupon_id,cpn_code;
    private String wallet_percent_amt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.coupon_list, container, false);
        sessionManagement = new Session_management(getActivity());
        userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        ((MainActivity) getActivity()).setTitle("Apply Coupons");
        initUI();
        if (NetworkConnection.connectionChecking(getActivity())){
            getCoupenCodes();
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }

        recycler=view.findViewById(R.id.rv_coupon_list);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        // handle the touch event if true
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // check user can press back button or not
                if( keyCode == KeyEvent.KEYCODE_BACK ) {
                    Bundle args = new Bundle();
                    Fragment fm = new Cart_fragment();
                    args.putString("code", "");
                    args.putString("coupon_id","0");
                    args.putString("wallet_amt", wallet_percent_amt);
                    fm.setArguments(args);
                    FragmentManager fragmentManager=((AppCompatActivity) getActivity()).getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).commit();
                    return true;
                }
                /*if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    return true;
                }*/
                return false;
            }
        });
        return view;
    }

    private void initUI() {
        Bundle bundle = this.getArguments();
        wallet_percent_amt = bundle.getString("wallet_amt");
    }

    private void getCoupenCodes() {
        String tag_json_obj = "json_coupen_req";

        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        alertDialog.show();
        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.GET,
                BaseURL.DownloadCoupenCode, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                try {
                    ArrayList<Coupon_list_model> al_coupen_code = new ArrayList<>();
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Coupon_list_model>>() {
                            }.getType();
                            coupon_list_model = gson.fromJson(response.getString("data"), listType);
                            if(coupon_list_model.size() > 0) {
                               // Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                adapter = new Coupon_adapter(getActivity(),coupon_list_model, wallet_percent_amt);
                                recycler.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    interface onCoupenCodeDownloaded {
        void onSuccess();
        void onFail();
    }

}
