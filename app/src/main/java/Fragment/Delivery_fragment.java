package Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.GeoLocationActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.swipe.util.Attributes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapter.Delivery_get_address_adapter;
import Adapter.View_time_adapter;
import Config.BaseURL;
import Config.SharedPref;
import Model.Delivery_address_model;
import com.adjit.surfcity.AppController;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.R;

import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.RecyclerTouchListener;
import util.Session_management;

/*
 * Created by Rajesh Dabhi on 27/6/2017.
*/

public class Delivery_fragment extends Fragment implements View.OnClickListener {

    private static String TAG = Delivery_fragment.class.getSimpleName();

    private TextView tv_afternoon, tv_morning, tv_total, tv_item, tv_socity, tv_del_address;
    private TextView tv_date, tv_time,tv_fday,tv_fdatem,tv_sday,tv_sdatem,tv_tday,tv_tdatem,tv_frday,tv_frdatem,tv_ffday,tv_ffdatem;
    private EditText et_address;
    private RelativeLayout btn_checkout, tv_add_adress,rl_fday,rl_sday,rl_tday,rl_frday,rl_ffday;
    private RecyclerView rv_address;

    //get address
    private Delivery_get_address_adapter adapter;
    private List<Delivery_address_model> delivery_address_modelList = new ArrayList<>();
    //get time
    private RecyclerView rv_time;
    private List<String> time_list = new ArrayList<>();

    private DatabaseHandler db_cart;

    private Session_management sessionManagement;

    private int mYear, mMonth, mDay, mHour, mMinute;

    private String gettime = "";
    private String getdate = "";
    String user_id, sub_plan, cashback, coupons_id, total_product_price, getsocity, delivery_charge;
    private String deli_charges;
    String store_id, net_total_amout, total_amout, coupon_amt, wallet_amt_t;
    //String store_id;
    boolean date_valid= false;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    private String delivery_address = "";


    public Delivery_fragment() {
        //Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delivery_time, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setTitle("Address and Date & Time");

        store_id = SharedPref.getString(getActivity(), BaseURL.STORE_ID);

        tv_date = (TextView) view.findViewById(R.id.tv_deli_date);
        tv_time = (TextView) view.findViewById(R.id.tv_deli_fromtime);
        tv_add_adress = (RelativeLayout) view.findViewById(R.id.tv_deli_add_address);
        tv_total = (TextView) view.findViewById(R.id.tv_deli_total);
        tv_item = (TextView) view.findViewById(R.id.tv_deli_item);
        btn_checkout = (RelativeLayout) view.findViewById(R.id.btn_deli_checkout);
        rv_address = (RecyclerView) view.findViewById(R.id.rv_deli_address);
        rv_time = view.findViewById(R.id.rv_times);
        //rl date lists
        rl_fday = view.findViewById(R.id.rl_fday);
        rl_sday = view.findViewById(R.id.rl_sday);
        rl_tday = view.findViewById(R.id.rl_tday);
        rl_frday = view.findViewById(R.id.rl_frday);
        rl_ffday = view.findViewById(R.id.rl_ffday);
        //tv date list
        tv_fday = view.findViewById(R.id.tv_fday);
        tv_fdatem = view.findViewById(R.id.tv_fdatem);
        tv_sday = view.findViewById(R.id.tv_sday);
        tv_sdatem = view.findViewById(R.id.tv_sdatem);
        tv_tday = view.findViewById(R.id.tv_tday);
        tv_tdatem = view.findViewById(R.id.tv_tdatem);
        tv_frday = view.findViewById(R.id.tv_frday);
        tv_frdatem = view.findViewById(R.id.tv_frdatem);
        tv_ffday = view.findViewById(R.id.tv_ffday);
        tv_ffdatem = view.findViewById(R.id.tv_ffdatem);
        tv_del_address = view.findViewById(R.id.tv_del_address);

        rv_address.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_time.setLayoutManager(new LinearLayoutManager(getActivity()));

        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //tv_socity = (TextView) view.findViewById(R.id.tv_deli_socity);
       // et_address = (EditText) view.findViewById(R.id.et_deli_address);

        //try to set temp address
        //et_address = (EditText) view.findViewById(R.id.textView9);

        db_cart = new DatabaseHandler(getActivity());

        // get session user data
        sessionManagement = new Session_management(getActivity());

        tv_total.setText(getArguments().getString("total"));
        tv_item.setText("" +sessionManagement.getCartCount());
        getsocity = sessionManagement.getUserDetails().get(BaseURL.KEY_SOCITY_NAME);
        String getaddress = sessionManagement.getUserDetails().get(BaseURL.KEY_HOUSE);

        net_total_amout=getArguments().getString("total");
        total_amout=getArguments().getString("total_amout");
        coupon_amt=getArguments().getString("coupon_amt");
        wallet_amt_t=getArguments().getString("wallet_amt_t");
        sub_plan=getArguments().getString("sub_plan");

        cashback = getArguments().getString("cashback");
        coupons_id = getArguments().getString("coupons_id");
        total_product_price = getArguments().getString("total_product_price");
        delivery_charge = getArguments().getString("delivery_charge");
        //tv_socity.setText("Socity Name: " + getsocity);
        //et_address.setText(getaddress);

        tv_date.setOnClickListener(this);
        tv_time.setOnClickListener(this);
        tv_add_adress.setOnClickListener(this);
        btn_checkout.setOnClickListener(this);
       // rl_fday.setOnClickListener(this);

        String date = sessionManagement.getdatetime().get(BaseURL.KEY_DATE);
        String time = sessionManagement.getdatetime().get(BaseURL.KEY_TIME);
        user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);

        delivery_address = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.DEL_ADDRESS, SharePreferenceUtility.PREFTYPE_STRING);
        if(!delivery_address.equalsIgnoreCase("") || delivery_address != null){
            tv_del_address.setText(delivery_address);
        }

        if (date != null && time != null) {
            getdate = date;
            gettime = time;
            try {
                String inputPattern = "yyyy-MM-dd";
                String outputPattern = "dd-MM-yyyy";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                Date date1 = inputFormat.parse(getdate);
                String str = outputFormat.format(date1);

                tv_date.setText(getResources().getString(R.string.delivery_date) + str);

            } catch (ParseException e) {
                e.printStackTrace();

                tv_date.setText(getResources().getString(R.string.delivery_date) + getdate);
            }
            tv_time.setText(time);
        }
        /*
        if (NetworkConnection.connectionChecking(getActivity())){
            makeGetAddressRequest(user_id);
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
*/
        updateCalenderUi();
        //current date time
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        //get default time
        getdate = "" + mYear + "-" + (mMonth + 1) + "-" + tv_fdatem.getText().toString().split(" ")[0];
//        getdate = "" + mYear + "-" + (mMonth + 1) + "-" + mDay;

        makeGetTimeRequest(getdate);

        //select first date
        rl_fday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(date_valid)*/ {
                    getdate = "" + mYear + "-" + (mMonth + 1) + "-" + tv_fdatem.getText().toString().split(" ")[0];
                    rl_fday.setBackgroundResource(R.drawable.textview_border);
                    rl_sday.setBackgroundResource(R.drawable.textview_border_white);
                    rl_tday.setBackgroundResource(R.drawable.textview_border_white);
                    rl_frday.setBackgroundResource(R.drawable.textview_border_white);
                    rl_ffday.setBackgroundResource(R.drawable.textview_border_white);

                    tv_fday.setTextColor(getResources().getColor(R.color.white));
                    tv_tday.setTextColor(getResources().getColor(R.color.black));
                    tv_sday.setTextColor(getResources().getColor(R.color.black));
                    tv_frday.setTextColor(getResources().getColor(R.color.black));
                    tv_ffday.setTextColor(getResources().getColor(R.color.black));

                    makeGetTimeRequest(getdate);
                }/*else {
                    Toast.makeText(getActivity(),"Select Next Day of Time",Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        rl_sday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getdate = "" + mYear + "-" + (mMonth + 1) + "-" + tv_sdatem.getText().toString().split(" ")[0];
                rl_sday.setBackgroundResource(R.drawable.textview_border);
                rl_tday.setBackgroundResource(R.drawable.textview_border_white);
                rl_frday.setBackgroundResource(R.drawable.textview_border_white);
                rl_ffday.setBackgroundResource(R.drawable.textview_border_white);
                rl_fday.setBackgroundResource(R.drawable.textview_border_white);

                tv_fday.setTextColor(getResources().getColor(R.color.black));
                tv_tday.setTextColor(getResources().getColor(R.color.black));
                tv_sday.setTextColor(getResources().getColor(R.color.white));
                tv_frday.setTextColor(getResources().getColor(R.color.black));
                tv_ffday.setTextColor(getResources().getColor(R.color.black));
                    makeGetTimeRequest(getdate);
            }
        });

        rl_tday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getdate = "" + mYear + "-" + (mMonth + 1) + "-" + tv_tdatem.getText().toString().split(" ")[0];
                rl_fday.setBackgroundResource(R.drawable.textview_border_white);
                rl_sday.setBackgroundResource(R.drawable.textview_border_white);
                rl_tday.setBackgroundResource(R.drawable.textview_border);
                rl_frday.setBackgroundResource(R.drawable.textview_border_white);
                rl_ffday.setBackgroundResource(R.drawable.textview_border_white);

                tv_fday.setTextColor(getResources().getColor(R.color.black));
                tv_tday.setTextColor(getResources().getColor(R.color.white));
                tv_sday.setTextColor(getResources().getColor(R.color.black));
                tv_frday.setTextColor(getResources().getColor(R.color.black));
                tv_ffday.setTextColor(getResources().getColor(R.color.black));
                    makeGetTimeRequest(getdate);
            }
        });

        rl_frday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getdate = "" + mYear + "-" + (mMonth + 1) + "-" + tv_frdatem.getText().toString().split(" ")[0];
                rl_fday.setBackgroundResource(R.drawable.textview_border_white);
                rl_sday.setBackgroundResource(R.drawable.textview_border_white);
                rl_tday.setBackgroundResource(R.drawable.textview_border_white);
                rl_frday.setBackgroundResource(R.drawable.textview_border);
                rl_ffday.setBackgroundResource(R.drawable.textview_border_white);

                tv_fday.setTextColor(getResources().getColor(R.color.black));
                tv_tday.setTextColor(getResources().getColor(R.color.black));
                tv_sday.setTextColor(getResources().getColor(R.color.black));
                tv_frday.setTextColor(getResources().getColor(R.color.white));
                tv_ffday.setTextColor(getResources().getColor(R.color.black));
                    makeGetTimeRequest(getdate);
            }
        });

        rl_ffday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getdate = "" + mYear + "-" + (mMonth + 1) + "-" + tv_ffdatem.getText().toString().split(" ")[0];
                rl_fday.setBackgroundResource(R.drawable.textview_border_white);
                rl_sday.setBackgroundResource(R.drawable.textview_border_white);
                rl_tday.setBackgroundResource(R.drawable.textview_border_white);
                rl_frday.setBackgroundResource(R.drawable.textview_border_white);
                rl_ffday.setBackgroundResource(R.drawable.textview_border);

                tv_fday.setTextColor(getResources().getColor(R.color.black));
                tv_tday.setTextColor(getResources().getColor(R.color.black));
                tv_sday.setTextColor(getResources().getColor(R.color.black));
                tv_frday.setTextColor(getResources().getColor(R.color.black));
                tv_ffday.setTextColor(getResources().getColor(R.color.white));
                    makeGetTimeRequest(getdate);
            }
        });
        //End Calender Ui

        //select time
/*        rv_time.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_time, new RecyclerTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                gettime = time_list.get(position);

                sessionManagement.cleardatetime();

                sessionManagement.creatdatetime(getdate, gettime);
                adapter.notifyDataSetChanged();
               // ((MainActivity) getActivity()).onBackPressed();

            }
            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));*/

        return view;
    }

    @Override public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setVisible(false);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

       if (id == R.id.btn_deli_checkout) {
            attemptOrder();
        } else if (id == R.id.tv_deli_date) {
            getdate();
        } else if (id == R.id.tv_deli_fromtime) {
            if (TextUtils.isEmpty(getdate)) {
                Toast.makeText(getActivity(), getResources().getString(R.string.please_select_date), Toast.LENGTH_SHORT).show();
            }else {
                Bundle args = new Bundle();
                Fragment fm = new View_time_fragment();
                args.putString("date", getdate);
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
            }
        } else if (id == R.id.tv_deli_add_address) {
           if(NetworkConnection.connectionChecking(getActivity())) {
               if(NetworkConnection.connectionChecking(getActivity())) {
                   Intent i = new Intent(getActivity(), GeoLocationActivity.class);
                   i.putExtra("fromHome", true);
                   getActivity().startActivity(i);
               }else {
                   Toast.makeText(getActivity(), ""+getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
               }
           }else {
               Toast.makeText(getActivity(), ""+getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
           }
            /*sessionManagement.updateSocity("", "");
            Fragment fm = new Add_delivery_address_fragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();*/
        }
    }



    private void getdate() {
        //Get Current Date
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        try {
            Date date = inputFormat.parse(getdate);
            String str = outputFormat.format(date);
            if(validate_order(date)) {
                date_valid = true;
               // tv_date.setText(getResources().getString(R.string.delivery_date) + str);
                //attemptOrder();
            }else {
            date_valid = false;
            getdate = "";
            tv_date.setText(getActivity().getResources().getString(R.string.tv_del_date));
            Toast.makeText(getActivity(),"Please select next date",Toast.LENGTH_SHORT).show();
        }
        }catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private boolean validate_order(Date seldate) {
       Date curent = Calendar.getInstance().getTime();
       int hour=  curent.getHours();
         if(hour >= 14) {
             if(curent.getDate() == seldate.getDate()) {
                 return false;
             }
         }
     return  true;
    }

    private void attemptOrder() {
        gettime = sessionManagement.getdatetime().get(BaseURL.KEY_TIME);
       /// String getaddress = et_address.getText().toString();
       String location_id = "001";
       String address = "Select/Add Address";

        boolean cancel = false;

        if (TextUtils.isEmpty(getdate) || getdate == null) {
            Toast.makeText(getActivity(), getResources().getString(R.string.please_select_date_time), Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (TextUtils.isEmpty(gettime) || gettime == null) {
            Toast.makeText(getActivity(), getResources().getString(R.string.please_select_date_time), Toast.LENGTH_SHORT).show();
            cancel = true;
        }else if (TextUtils.isEmpty(delivery_address) || delivery_address == null) {
            Toast.makeText(getActivity(), getResources().getString(R.string.please_add_address), Toast.LENGTH_SHORT).show();
            cancel = true;
            /*if (adapter.ischeckd()) {
                location_id = adapter.getlocation_id();
                address = adapter.getaddress();
                Log.e("address",address);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.please_select_address), Toast.LENGTH_SHORT).show();
                cancel = true;
            }*/
        }
        /*if (TextUtils.isEmpty(getaddress)) {
            Toast.makeText(getActivity(), "Please add your address", Toast.LENGTH_SHORT).show();
            cancel = true;
        }*/

        if (!cancel) {
            //Toast.makeText(getActivity(), "date:"+getdate+"Fromtime:"+getfrom_time+"Todate:"+getto_time, Toast.LENGTH_SHORT).show();
            sessionManagement.cleardatetime();
            Bundle args = new Bundle();
            Fragment fm = new Delivery_payment_detail_fragment();
            args.putString("getdate", getdate);
            args.putString("time", gettime);
            args.putString("total",net_total_amout);
            args.putString("total_amout",total_amout);
            args.putString("coupon_amt",coupon_amt);
            args.putString("wallet_amt_t",wallet_amt_t);
            args.putString("location_id", location_id);
            args.putString("address", delivery_address);
            args.putString("deli_charges", deli_charges);
            args.putString("store_id", "001");//store_id
            args.putString("sub_plan",sub_plan);
            args.putString("cashback", cashback);
            args.putString("coupons_id", coupons_id);
            args.putString("total_product_price", total_product_price);
            args.putString("society_id", ""); //adapter.getSocietyId()
            args.putString("delivery_charge", delivery_charge);
            fm.setArguments(args);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
        }
    }

    /*
     * Method to make json object request where json response starts wtih
     */
    private void makeGetAddressRequest(String user_id) {
        alertDialog.show();
        // Tag used to cancel the request
        String tag_json_obj = "json_get_address_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        String  url  = BaseURL.GET_ADDRESS_URL+"?user_id="+user_id;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.e("location_search", response.toString());
                alertDialog.dismiss();
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        delivery_address_modelList.clear();
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<Delivery_address_model>>() {
                        }.getType();
                        delivery_address_modelList = gson.fromJson(response.getString("data"), listType);
                        //RecyclerView.Adapter adapter1 = new Delivery_get_address_adapter(delivery_address_modelList);
                        adapter = new Delivery_get_address_adapter(delivery_address_modelList);
                        ((Delivery_get_address_adapter) adapter).setMode(Attributes.Mode.Single);
                        rv_address.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        if (delivery_address_modelList.isEmpty()) {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), "No address Found", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        queue.add(jsonRequest);
    }

    @Override
    public void onPause() {
        super.onPause();
        //unregister reciver
        getActivity().unregisterReceiver(mCart);
    }

    @Override
    public void onResume() {
        super.onResume();
        delivery_address = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.DEL_ADDRESS, SharePreferenceUtility.PREFTYPE_STRING);
        if(!delivery_address.equalsIgnoreCase("") || delivery_address != null){
            tv_del_address.setText(delivery_address);
        }
        //register reciver
        getActivity().registerReceiver(mCart, new IntentFilter("Grocery_delivery_charge"));
    }

    // broadcast reciver for receive data
    private BroadcastReceiver mCart = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            if (type.contentEquals("update")) {
                //updateData();
                deli_charges = intent.getStringExtra("charge");
                //Toast.makeText(getActivity(), deli_charges, Toast.LENGTH_SHORT).show();
                Double total = Double.parseDouble(db_cart.getTotalAmount()) + Integer.parseInt(deli_charges);
                tv_total.setText("" +getArguments().getString("total"));
        }
        }
    };

    /**
     * Method to make json object request where json response starts wtih {
     */
    private void makeGetTimeRequest(String date) {
        if (NetworkConnection.connectionChecking(getActivity())){
            alertDialog.show();
            time_list.clear();
            // Tag used to cancel the request
            String tag_json_obj = "json_time_req";
            Map<String, String> params = new HashMap<String, String>();
            params.put("date", date);
            String url = BaseURL.GET_TIME_SLOT_URL+"?date="+date;
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                    url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //TODO: handle success
                    Log.d(TAG, response.toString());
                    alertDialog.dismiss();
                    try {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            JSONArray jsonArray = response.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {//times
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                //time_list.add(""+response.getJSONArray("data").get(i));//times
                                String openingTime = jsonObject.getString("opening_time");
                                String closingTime = jsonObject.getString("closing_time");
                                //Condition check Time if Time>2 show morningTime
                                time_list.add(openingTime + " - " + closingTime);
                            }
                            View_time_adapter adapter = new View_time_adapter(time_list);
                            rv_time.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertDialog.dismiss();
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            queue.add(jsonRequest);
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private void updateCalenderUi() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        getdate = "" + mYear + "-" + (mMonth + 1) + "-" + mDay;

        //1 date
        /*c.add(Calendar.DATE, 0);
        Date todayDate = c.getTime();*/

        c.add(Calendar.DATE, 1);
        Date nextDate = c.getTime();
        tv_fday.setText(""+android.text.format.DateFormat.format("EEE", nextDate));
        tv_fdatem.setText(""+android.text.format.DateFormat.format("dd MMM",nextDate));
        //2 date
        c.add(Calendar.DATE, 1);

        Date nextDates = c.getTime();
        tv_sday.setText(""+android.text.format.DateFormat.format("EEE",nextDates));
        tv_sdatem.setText(""+android.text.format.DateFormat.format("dd MMM",nextDates));
        //3 date
        c.add(Calendar.DATE, 1);
        Date nextDatef = c.getTime();
        tv_tday.setText(""+android.text.format.DateFormat.format("EEE",nextDatef));
        tv_tdatem.setText(""+android.text.format.DateFormat.format("dd MMM",nextDatef));
        //4 date
        c.add(Calendar.DATE, 1);
        Date nextDatessi = c.getTime();
        tv_frday.setText(""+android.text.format.DateFormat.format("EEE",nextDatessi));
        tv_frdatem.setText(""+android.text.format.DateFormat.format("dd MMM",nextDatessi));
        //5 date
        c.add(Calendar.DATE, 1);
        Date nextDate5 = c.getTime();
        tv_ffday.setText(""+android.text.format.DateFormat.format("EEE",nextDate5));
        tv_ffdatem.setText(""+android.text.format.DateFormat.format("dd MMM",nextDate5));
    }


}
