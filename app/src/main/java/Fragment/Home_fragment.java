package Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.AppController;
import com.adjit.surfcity.Club_membership;
import com.adjit.surfcity.CustomSlider;
import com.adjit.surfcity.GeoLocationActivity;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.Product_Details;
import com.adjit.surfcity.R;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapter.Deal_OfDay_Adapter;
import Adapter.Home_Icon_Adapter;
import Adapter.Home_adapter;
import Adapter.Shop_List_Adapter;
import Adapter.Subscription_Adapter;
import Adapter.Top_Selling_Adapter;
import Config.BaseURL;
import Config.SharedPref;
import Interfaces.Interfaces;
import Model.Category_model;
import Model.Deal_Of_Day_model;
import Model.Home_Icon_model;
import Model.Shop_List_Model;
import Model.Subscription_model;
import Model.Top_Selling_model;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.Session_management;

public class Home_fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        Interfaces.onPurchase, PaymentResultListener, View.OnClickListener {
    private static String TAG = Home_fragment.class.getSimpleName();
    private SliderLayout imgSlider, banner_slider, featuredslider;
    private RecyclerView rv_items, rv_top_selling, rv_deal_of_day, rv_headre_icons, rv_branded_shops;
    private List<Category_model> category_modelList = new ArrayList<>();
    private Home_adapter adapter;
    private SwipeRefreshLayout swipRefresh ;
    private TextView tv_del_address ;
    private boolean isSubcat = false;
    LinearLayout Search_layout, ll_location;
    String getid, getcat_title, delivery_address;
    NestedScrollView scrollView;
    RelativeLayout link;
    public String sub_id , sub_amt, txn_id, user_id, order_id ;

    //Home Icons
    private Home_Icon_Adapter menu_adapter;
    private List<Home_Icon_model> menu_models = new ArrayList<>();
    private List<Shop_List_Model> shop_models = new ArrayList<>();

    //Deal O Day
    private Deal_OfDay_Adapter deal_ofDay_adapter;
    private List<Deal_Of_Day_model> deal_of_day_models = new ArrayList<>();
    LinearLayout Deal_Linear_layout, ll_branded_shops;
    FrameLayout  Deal_Frame_layout1;

    //Top Selling Products
    private Top_Selling_Adapter top_selling_adapter;
    private List<Top_Selling_model> top_selling_models = new ArrayList<>();

    //combo pack products
    private List<Top_Selling_model> combo_list = new ArrayList<>();
    FrameLayout combo_frame;
    RecyclerView combo_recycler;
    private Top_Selling_Adapter combo_adapter;

    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    private ArrayList<Subscription_model> subscription = new ArrayList<Subscription_model>();
    RecyclerView club_recycler;
    Subscription_Adapter sub_Adapter;

    private ImageView iv_Call, iv_Whatspp, iv_reviews, iv_share_via;
    private TextView timer;
    Button View_all_deals, View_all_TopSell, view_all_Combo, club_link, btn_all_branded_shops;

    private ImageView Top_Selling_Poster;
    LinearLayout ll_deal_hiddable_txt;

    FrameLayout topselling_framelayout;

    View view;
    private Session_management sessionManagement;

    public Home_fragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        delivery_address = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.DEL_ADDRESS, SharePreferenceUtility.PREFTYPE_STRING);
        if(!TextUtils.isEmpty(delivery_address.trim()) || delivery_address != null){
            tv_del_address.setText(delivery_address);
        }else {
            tv_del_address.setText("Pickup Address");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
//        ((MainActivity) getActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.app_name));
        ((MainActivity) getActivity()).updateHeader();
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    ((MainActivity) getActivity()).finish();
                    return true;
                }
                return false;
            }
        });

        sessionManagement = new Session_management(getActivity());
        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //timer = (TextView) view.findViewById(R.id.timer);
        View_all_deals = (Button) view.findViewById(R.id.view_all_deals);
        View_all_TopSell = (Button) view.findViewById(R.id.view_all_topselling);
        view_all_Combo = view.findViewById(R.id.view_all_combo_pack);
        Deal_Frame_layout1 = (FrameLayout) view.findViewById(R.id.deal_frame_layout1);
        Deal_Linear_layout = (LinearLayout) view.findViewById(R.id.deal_linear_layout);
        club_link = view.findViewById(R.id.club_link);
        ll_deal_hiddable_txt = view.findViewById(R.id.ll_deal_hiddable_txt);
        ll_branded_shops = view.findViewById(R.id.ll_branded_shops);
        btn_all_branded_shops = view.findViewById(R.id.btn_all_branded_shops);
        btn_all_branded_shops.setOnClickListener(this);
        topselling_framelayout = view.findViewById(R.id.topselling_framelayout);
        ll_location = view.findViewById(R.id.ll_location);
        ll_location.setOnClickListener(this);
        tv_del_address = view.findViewById(R.id.tv_del_address);
        club_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(NetworkConnection.connectionChecking(getActivity())){
                    Intent intent = new Intent(getActivity(), Club_membership.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        delivery_address = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.DEL_ADDRESS, SharePreferenceUtility.PREFTYPE_STRING);
        if(!TextUtils.isEmpty(delivery_address.trim()) || delivery_address != null){
            tv_del_address.setText(delivery_address);
        }else {
            tv_del_address.setText("Pickup Address");
        }
        if (NetworkConnection.connectionChecking(getActivity())){
            checkSubscription();
            if(user_id!=null && !(user_id.equalsIgnoreCase("")))
            getCartcount(user_id);
        }


        //Top Selling Poster
        Top_Selling_Poster = (ImageView) view.findViewById(R.id.top_selling_imageview);

        //Scroll View
        scrollView = (NestedScrollView) view.findViewById(R.id.scroll_view);
        scrollView.setSmoothScrollingEnabled(true);

        //Search
      /*  Search_layout = (LinearLayout) view.findViewById(R.id.search_layout);
        Search_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fm = new Search_fragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
            }
        });

      */  //Slider
        swipRefresh = view.findViewById(R.id.swipRefresh);
        swipRefresh.setOnRefreshListener(this);
        imgSlider = view.findViewById(R.id.home_img_slider);
        banner_slider = (SliderLayout) view.findViewById(R.id.relative_banner);
        featuredslider = (SliderLayout) view.findViewById(R.id.featured_img_slider);
        //club membership
        club_recycler = view.findViewById(R.id.club_recycler);
        //Catogary Icons
        rv_items = (RecyclerView) view.findViewById(R.id.rv_home);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        //  RecyclerView.LayoutManager gridLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        rv_items.setLayoutManager(gridLayoutManager);
        rv_items.setItemAnimator(new DefaultItemAnimator());
        rv_items.setNestedScrollingEnabled(false);

        //Deal Of the Day
        rv_deal_of_day = (RecyclerView) view.findViewById(R.id.rv_deal);
        //GridLayoutManager gridLayoutManager1 = new GridLayoutManager(getActivity(), 2);
        RecyclerView.LayoutManager gridLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_deal_of_day.setLayoutManager(gridLayoutManager1);
        rv_deal_of_day.setItemAnimator(new DefaultItemAnimator());
        rv_deal_of_day.setNestedScrollingEnabled(false);
        rv_deal_of_day.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(1), true));

        //Top Selling Products
        rv_top_selling = (RecyclerView) view.findViewById(R.id.top_selling_recycler);
        //GridLayoutManager gridLayoutManager2 = new GridLayoutManager(getActivity(), 2);
        RecyclerView.LayoutManager gridLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_top_selling.setLayoutManager(gridLayoutManager2);
        rv_top_selling.setItemAnimator(new DefaultItemAnimator());
        rv_top_selling.setNestedScrollingEnabled(false);
        rv_top_selling.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(1), true));

        //combo pack products
        combo_recycler = (RecyclerView) view.findViewById(R.id.combo_pack_recycler);
        //GridLayoutManager gridLayoutManager3 = new GridLayoutManager(getActivity(), 2);
        RecyclerView.LayoutManager gridLayoutManager3 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        combo_recycler.setLayoutManager(gridLayoutManager3);
        combo_recycler.setItemAnimator(new DefaultItemAnimator());
        combo_recycler.setNestedScrollingEnabled(false);
        combo_recycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(1), true));

        rv_branded_shops = view.findViewById(R.id.rv_branded_shops);
        //Brandewd Shops
        RecyclerView.LayoutManager gridLayoutManager4 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_branded_shops.setLayoutManager(gridLayoutManager4);
        rv_branded_shops.setItemAnimator(new DefaultItemAnimator());
        rv_branded_shops.setNestedScrollingEnabled(false);
        rv_branded_shops.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(1), true));



        //Check Internet Connection
        if (NetworkConnection.connectionChecking(getActivity())){
            makeGetCategoryRequest();
            fetch_subscription();
            makeGetSliderRequest();
            makeGetBannerSliderRequest();
            make_combo_packs();
            make_shop_items();
            make_menu_items();
            make_deal_od_the_day();

            //            makeGetFeaturedSlider();
            //            make_top_selling();
            //            make_offer_product();
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }



        //make_menu_items Icons
        rv_headre_icons = view.findViewById(R.id.collapsing_recycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity()) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {
                    private static final float SPEED = 2000f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }
                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }
        };
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_headre_icons.setLayoutManager(layoutManager);
        rv_headre_icons.setHasFixedSize(true);
        rv_headre_icons.setItemViewCacheSize(10);
        rv_headre_icons.setDrawingCacheEnabled(true);
        rv_headre_icons.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

        //Call And Whatsapp
        iv_Call = (ImageView) view.findViewById(R.id.iv_call);
        iv_Whatspp = (ImageView) view.findViewById(R.id.iv_whatsapp);
        iv_reviews = (ImageView) view.findViewById(R.id.reviews);
        iv_share_via = (ImageView) view.findViewById(R.id.share_via);

        iv_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /*Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + "919990155993"));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                getActivity().startActivity(callIntent);*/

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "7517970971", null));
                startActivity(intent);

            }
        });
        iv_Whatspp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  String smsNumber = "919990155993";
                Uri uri = Uri.parse("smsto:" + smsNumber);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.putExtra("Test", "Neeraj");
                i.setPackage("com.whatsapp");
                startActivity(i);*/

                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("917517970971") + "@s.whatsapp.net");//phone number without "+" prefix
                startActivity(sendIntent);

            }
        });
        iv_reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewOnApp();
            }
        });
        iv_share_via.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareApp(); }
        });

        //Recycler View Shop By Catogary
        rv_items.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_items, new Home_fragment.RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                getid = category_modelList.get(position).getId();
                getcat_title = category_modelList.get(position).getTitle();
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_id", getid);
                args.putString("cat_title", getcat_title);
                args.putInt("position", position);
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rv_branded_shops.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_branded_shops, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("shop_id", shop_models.get(position).getShop_id());
                args.putString("shop_name", shop_models.get(position).getShop_name());
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        //Recycler View Menu Products
        rv_headre_icons.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_headre_icons, new Home_fragment.RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                getid = menu_models.get(position).getId();
                getcat_title = menu_models.get(position).getTitle();
                Log.e("getid",getid);
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_id", getid);
                args.putString("cat_title", getcat_title);
                args.putInt("position", position);
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        //Recycler View Deal Of Day
        rv_deal_of_day.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("dxdy",""+dx+" - "+dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager)recyclerView.getLayoutManager());
                int firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                if (firstVisiblePosition > 0){
                    ll_deal_hiddable_txt.setVisibility(View.GONE);
                }
                if (firstVisiblePosition == 0){
                    ll_deal_hiddable_txt.setVisibility(View.VISIBLE);
                }
            }
        });

        /*rv_deal_of_day.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_deal_of_day,
                new Home_fragment.RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
              final String getdeal_of_day_prod_id = deal_of_day_models.get(position).getId();

                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_deal","2");
                args.putInt("position",position);
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
                RelativeLayout link = view.findViewById(R.id.subscription_link);
                link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), Club_membership.class);
                        startActivity(intent);
                    }
                });
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), Product_Details.class);
                        startActivity(intent);
                        Bundle args = new Bundle();
                        Fragment fm = new ProductDetails_Fragment();
                        args.putString("product_id",getdeal_of_day_prod_id);
                        fm.setArguments(args);
                        FragmentManager fragmentManager =  ((AppCompatActivity)getActivity()).getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                    }
                });
            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));*/
        //Required Deal Id
        View_all_deals.setVisibility(View.VISIBLE);
        View_all_deals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_deal", "2");
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();

            }
        });

        //REcyclerview Top Selling
        rv_top_selling.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_top_selling, new Home_fragment.RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            /*    getid = top_selling_models.get(position).getProduct_id();
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_top_selling", "2");
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();*/
                RelativeLayout link = view.findViewById(R.id.subscription_link);
                link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), Club_membership.class);
                        startActivity(intent);

                    }
                });
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       /* Intent intent = new Intent(getActivity(), Product_Details.class);
                        startActivity(intent);*/
                        Bundle args = new Bundle();
                        Fragment fm = new ProductDetails_Fragment();
                        args.putString("product_id",top_selling_models.get(position).getProduct_id());
                        fm.setArguments(args);
                        FragmentManager fragmentManager =  ((AppCompatActivity)getActivity()).getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                    }
                });
            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));
      /*  top_slider.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), top_slider, new Home_fragment.RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position){
                getid = deal_of_day_models.get(position).getId();
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_deal","2");
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));*/
        View_all_TopSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_top_selling", "2");
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();

            }
        });

        view_all_Combo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_combo", "2");
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
            }
        });

        //combo recycler
        combo_recycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), combo_recycler, new Home_fragment.RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
            /*    getid = top_selling_models.get(position).getProduct_id();
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_top_selling", "2");
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();*/
                link = view.findViewById(R.id.subscription_link);
                link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), Club_membership.class);
                        startActivity(intent);
                    }
                });
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), Product_Details.class);
                        startActivity(intent);
                    }
                });
            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));
     /*   //combo recycler
        offer_recycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), offer_recycler, new Home_fragment.RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
            *//*    getid = top_selling_models.get(position).getProduct_id();
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("cat_top_selling", "2");
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();*//*
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), Product_Details.class);
                        startActivity(intent);
                    }
                });
            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));*/
        return view;
    }

    private void getCartcount(String user_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        String url = BaseURL.GET_CART_COUNT+"?user_id="+user_id;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo", response.toString());
                try {
                    if (response.getBoolean("responce")) {
                        int item_count = response.getInt("data");
                        sessionManagement.setCartLogin(item_count);
                        ((MainActivity) getActivity()).setCartCounter(String.valueOf(item_count));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsonRequest);
    }

    private void checkSubscription() {
        user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        subscription.clear();
        JsonObjectRequest subreq = new JsonObjectRequest(Request.Method.POST, BaseURL.SUBSCRIPTION_DETAILS+"?user_id="+user_id,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                try {
                    if (response.getBoolean("responce")) {
                        Log.d("response", response.toString());
                        JSONObject data = response.getJSONObject("subscription_plan");
                        SharePreferenceUtility.saveStringPreferences(getActivity(), Const.SUBSCRIPTION_ID, data.getString("subscription_id"));
                    }else {
                        SharePreferenceUtility.saveStringPreferences(getActivity(), Const.SUBSCRIPTION_ID, "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });

        AppController.getInstance().addToRequestQueue(subreq);
    }


    private void fetch_subscription() {
        alertDialog.show();
        Log.d("url", BaseURL.GET_SUBSCRIPTION_URL);
        subscription.clear();
        JsonObjectRequest subreq = new JsonObjectRequest(Request.Method.GET, BaseURL.GET_SUBSCRIPTION_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                try {
                    if (response.getBoolean("responce")) {
                        Log.d("response", response.toString());
                        JSONArray data = response.getJSONArray("data");
                        subscription.addAll((Collection<? extends Subscription_model>) new Gson().fromJson(data.toString(), new TypeToken<ArrayList<Subscription_model>>() {
                        }.getType()));

                        Subscription_model priceSubscription_model = null;
                        String subscription_id = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
                        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

                        if (!subscription_id.equals("")) {
                            for (int i = 0; i < subscription.size(); i++) {
                                if (subscription.get(i).getSubscription_id().equals(subscription_id)) {
                                    priceSubscription_model = subscription.get(i);
                                }
                            }
                            sub_Adapter = new Subscription_Adapter(getActivity(), subscription, 0, priceSubscription_model, Home_fragment.this);
                        }else {
                            SharePreferenceUtility.saveStringPreferences(getActivity(), Const.SUBSCRIPTION_ID, "");
                            sub_Adapter = new Subscription_Adapter(getActivity(), subscription, 0, Home_fragment.this);
                        }
                        club_recycler.setLayoutManager(layoutManager2);
                        club_recycler.setAdapter(sub_Adapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });

        AppController.getInstance().addToRequestQueue(subreq);
    }

    private void makeGetSubscriptionPlan() {

    }

    private void makeGetSliderRequest() {
        Log.e("log", BaseURL.GET_SLIDER_URL);
        JsonArrayRequest req = new JsonArrayRequest(BaseURL.GET_SLIDER_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            imgSlider.removeAllSliders();
                            ArrayList<HashMap<String, String>> listarray = new ArrayList<>();
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                HashMap<String, String> url_maps = new HashMap<String, String>();
                                url_maps.put("slider_title", jsonObject.getString("slider_title"));
                                url_maps.put("sub_cat", jsonObject.getString("sub_cat"));
                                url_maps.put("slider_image", BaseURL.IMG_SLIDER_URL + jsonObject.getString("slider_image"));
                                url_maps.put("slider_url", jsonObject.getString("slider_url"));
                                if(!jsonObject.getString("slider_status").equalsIgnoreCase("0")) {
                                    listarray.add(url_maps);
                                }
                            }
                            for (HashMap<String, String> name : listarray) {
                                CustomSlider textSliderView = new CustomSlider(getActivity());
                                textSliderView.description(name.get("")).image(name.get("slider_image")).setScaleType(BaseSliderView.ScaleType.Fit);
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle().putString("slider_title", name.get("slider_title"));
                                textSliderView.getBundle().putString("extra", name.get("sub_cat"));
                                textSliderView.getBundle().putString("slider_url", name.get("slider_url"));
                                imgSlider.addSlider(textSliderView);
                                final String sub_cat = (String) textSliderView.getBundle().get("extra");
                                final String slider_url = (String) textSliderView.getBundle().get("slider_url");
                                final String slider_title = (String) textSliderView.getBundle().get("slider_title");
                                textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                        //   Toast.makeText(getActivity(), "" + sub_cat, Toast.LENGTH_SHORT).show();
                                        if(!slider_url.equalsIgnoreCase("") && slider_url!=null){
                                            reviewOnApp(slider_url.trim());
                                        }else {
                                            Bundle args = new Bundle();
                                            Fragment fm = new Product_fragment();
                                            args.putString("id", sub_cat);
                                            args.putString("cat_title", slider_title);
                                            fm.setArguments(args);
                                            FragmentManager fragmentManager = getFragmentManager();
                                            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                                                    .addToBackStack(null).commit();
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                   Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        AppController.getInstance().addToRequestQueue(req);
    }

    private void makeGetBannerSliderRequest() {
        JsonArrayRequest req = new JsonArrayRequest(BaseURL.GET_BANNER_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Banar_slider", response.toString());
                        try {
                            banner_slider.removeAllSliders();
                            ArrayList<HashMap<String, String>> listarray = new ArrayList<>();
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                HashMap<String, String> url_maps = new HashMap<String, String>();
                                url_maps.put("slider_title", jsonObject.getString("slider_title"));
                                url_maps.put("sub_cat", jsonObject.getString("sub_cat"));
                                url_maps.put("slider_image", BaseURL.IMG_SLIDER_URL + jsonObject.getString("slider_image"));
                                url_maps.put("slider_url", jsonObject.getString("slider_url"));
                                if(!jsonObject.getString("slider_status").equalsIgnoreCase("0")) {
                                    listarray.add(url_maps);
                                }
                            }
                            for (HashMap<String, String> name : listarray) {
                                CustomSlider textSliderView = new CustomSlider(getActivity());
                                textSliderView.description(name.get("")).image(name.get("slider_image")).setScaleType(BaseSliderView.ScaleType.Fit);
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle().putString("slider_title", name.get("slider_title"));
                                textSliderView.getBundle().putString("extra", name.get("sub_cat"));
                                textSliderView.getBundle().putString("slider_url", name.get("slider_url"));
                                banner_slider.addSlider(textSliderView);
                                final String sub_cat = (String) textSliderView.getBundle().get("extra");
                                final String slider_url = (String) textSliderView.getBundle().get("slider_url");
                                final String slider_title = (String) textSliderView.getBundle().get("slider_title");
                                textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                        //   Toast.makeText(getActivity(), "" + sub_cat, Toast.LENGTH_SHORT).show();
                                        if(!slider_url.equalsIgnoreCase("") && slider_url!=null){
                                            reviewOnApp(slider_url.trim());
                                        }else {
                                            Bundle args = new Bundle();
                                            Fragment fm = new Product_fragment();
                                            args.putString("id", sub_cat);
                                            args.putString("cat_title", slider_title);
                                            fm.setArguments(args);
                                            FragmentManager fragmentManager = getFragmentManager();
                                            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                                        }
                                    }
                                });

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        AppController.getInstance().addToRequestQueue(req);

    }


    private void makeGetFeaturedSlider() {
        JsonArrayRequest req = new JsonArrayRequest(BaseURL.GET_FEAATURED_SLIDER_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Featured_slider", response.toString());
                        try {
                            ArrayList<HashMap<String, String>> listarray = new ArrayList<>();
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                HashMap<String, String> url_maps = new HashMap<String, String>();
                                url_maps.put("slider_title", jsonObject.getString("slider_title"));
                                url_maps.put("sub_cat", jsonObject.getString("sub_cat"));
                                url_maps.put("slider_image", BaseURL.IMG_SLIDER_URL + jsonObject.getString("slider_image"));
                                listarray.add(url_maps);
                            }
                            for (HashMap<String, String> name : listarray) {
                                CustomSlider textSliderView = new CustomSlider(getActivity());
                                textSliderView.description(name.get("")).image(name.get("slider_image")).setScaleType(BaseSliderView.ScaleType.Fit);
                                textSliderView.bundle(new Bundle());
                                //  textSliderView.getBundle().putString("extra", name.get("slider_title"));
                                textSliderView.getBundle().putString("extra", name.get("sub_cat"));
                                featuredslider.addSlider(textSliderView);
                                final String sub_cat = (String) textSliderView.getBundle().get("extra");
                               /* textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                        //   Toast.makeText(getActivity(), "" + sub_cat, Toast.LENGTH_SHORT).show();
                                        Bundle args = new Bundle();
                                        Fragment fm = new Product_fragment();
                                        args.putString("id", sub_cat);
                                        fm.setArguments(args);
                                        FragmentManager fragmentManager = getFragmentManager();
                                        fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                                                .addToBackStack(null).commit();
                                    }
                                });
*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        AppController.getInstance().addToRequestQueue(req);

    }


    private void makeGetCategoryRequest() {

        String tag_json_obj = "json_category_req";
        isSubcat = false;
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;
        /*if (parent_id != null && parent_id != "") {}*/

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BaseURL.GET_CATEGORY_URL+"?parent=",
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d(TAG, response.toString());
                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Category_model>>() {
                            }.getType();
                            category_modelList = gson.fromJson(response.getString("data"), listType);
                            adapter = new Home_adapter(category_modelList);
                            rv_items.setNestedScrollingEnabled(false);
                            rv_items.setHasFixedSize(false);
                            rv_items.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        Volley.newRequestQueue(getActivity()).add(jsonRequest);

       /* CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.GET_CATEGORY_URL, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Category_model>>() {
                            }.getType();
                            category_modelList = gson.fromJson(response.getString("data"), listType);
                            adapter = new Home_adapter(category_modelList);
                            rv_items.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);*/

    }

    private void make_deal_od_the_day() {
        String tag_json_obj = "json_category_req";
        isSubcat = false;
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;
       /* if (parent_id != null && parent_id != "") {
        }*/
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET,
                BaseURL.GET_DEAL_OF_DAY_PRODUCTS+"?parent=", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.e("deal_of_day", response.toString());
                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        Gson gson = new Gson();
                        if (status == true) {
                            if (!response.names().toJSONObject(response.names()).has("Deal_of_the_day")) {
                                //Toast.makeText(getActivity(), "No Deal For Day", Toast.LENGTH_SHORT).show();
                                Deal_Linear_layout.setVisibility(View.GONE);
                            } else {
                                Deal_Linear_layout.setVisibility(View.VISIBLE);
                                JSONObject jsonObject = response.getJSONObject("global_setting");
                                SharedPref sharedPref = new SharedPref();
                                sharedPref.putString(getActivity().getApplicationContext(),"deal_max_count",
                                        jsonObject.getString("deal_max_count"));
                                Type listType = new TypeToken<List<Deal_Of_Day_model>>() {
                                }.getType();
                                deal_of_day_models = gson.fromJson(response.getString("Deal_of_the_day"), listType);
                                Log.e("list_size", String.valueOf(deal_of_day_models.size()));
                                deal_ofDay_adapter = new Deal_OfDay_Adapter(deal_of_day_models, getActivity());
                                rv_deal_of_day.setAdapter(deal_ofDay_adapter);
                                deal_ofDay_adapter.notifyDataSetChanged();
                            }
                           /* if (getActivity() != null) {
                            }*/
                        } else {
                            Toast.makeText(getActivity(), "No Response", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);
        /*CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.GET,
                BaseURL.GET_DEAL_OF_DAY_PRODUCTS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("deal_of_day", response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        Gson gson = new Gson();
                        if (status == true) {
                            if (!response.names().toJSONObject(response.names()).has("Deal_of_the_day")) {
                                //Toast.makeText(getActivity(), "No Deal For Day", Toast.LENGTH_SHORT).show();
                                rv_deal_of_day.setVisibility(View.GONE);
                                Deal_Frame_layout.setVisibility(View.GONE);
                                Deal_Frame_layout1.setVisibility(View.GONE);
                                Deal_Linear_layout.setVisibility(View.GONE);
                            } else {
                                Type listType = new TypeToken<List<Deal_Of_Day_model>>() {
                                }.getType();
                                deal_of_day_models = gson.fromJson(response.getString("Deal_of_the_day"), listType);
                                Log.e("list_size", String.valueOf(deal_of_day_models.size()));
                                deal_ofDay_adapter = new Deal_OfDay_Adapter(deal_of_day_models, getActivity());
                                rv_deal_of_day.setAdapter(deal_ofDay_adapter);
                                deal_ofDay_adapter.notifyDataSetChanged();
                            }
                           *//* if (getActivity() != null) {
                            }*//*
                        } else {
                            Toast.makeText(getActivity(), "No Response", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);*/
    }

    //Check get_all_top_selling_product api
    private void make_top_selling() {

        String tag_json_obj = "json_category_req";
        isSubcat = false;
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;
       /* if (parent_id != null && parent_id != "") {
        }*/
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                BaseURL.GET_TOP_SELLING_PRODUCTS+"?parent=", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d(TAG, response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {

                            if (!response.names().toJSONObject(response.names()).has("top_selling_product")) {
                                //Toast.makeText(getActivity(), "No Deal For Day", Toast.LENGTH_SHORT).show();
                                topselling_framelayout.setVisibility(View.GONE);
                               /* Deal_Frame_layout.setVisibility(View.GONE);
                                Deal_Frame_layout1.setVisibility(View.GONE);
                                Deal_Linear_layout.setVisibility(View.GONE);*/
                            } else {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<Top_Selling_model>>() {
                                }.getType();
                                top_selling_models = gson.fromJson(response.getString("top_selling_product"), listType);
                                top_selling_adapter = new Top_Selling_Adapter(top_selling_models);
                                rv_top_selling.setAdapter(top_selling_adapter);
                                top_selling_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);


/*
        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.GET_TOP_SELLING_PRODUCTS, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {

                            if (!response.names().toJSONObject(response.names()).has("top_selling_product")) {
                                //Toast.makeText(getActivity(), "No Deal For Day", Toast.LENGTH_SHORT).show();
                                topselling_framelayout.setVisibility(View.GONE);
                               *//* Deal_Frame_layout.setVisibility(View.GONE);
                                Deal_Frame_layout1.setVisibility(View.GONE);
                                Deal_Linear_layout.setVisibility(View.GONE);*//*
                            } else {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<Top_Selling_model>>() {
                                }.getType();
                                top_selling_models = gson.fromJson(response.getString("top_selling_product"), listType);
                                top_selling_adapter = new Top_Selling_Adapter(top_selling_models);
                                rv_top_selling.setAdapter(top_selling_adapter);
                                top_selling_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);*/
    }

    private void make_combo_packs() {
        String tag_json_obj = "json_category_req";
        isSubcat = false;
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;
       /* if (parent_id != null && parent_id != "") {
        }*/


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                BaseURL.GET_TOP_SELLING_PRODUCTS+"?parent=", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d(TAG, response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            if (!response.names().toJSONObject(response.names()).has("top_selling_product")) {
                                //Toast.makeText(getActivity(), "No Deal For Day", Toast.LENGTH_SHORT).show();
                                topselling_framelayout.setVisibility(View.GONE);
                               /* Deal_Frame_layout.setVisibility(View.GONE);
                                Deal_Frame_layout1.setVisibility(View.GONE);
                                Deal_Linear_layout.setVisibility(View.GONE);*/
                            } else {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<Top_Selling_model>>() {
                                }.getType();
                                combo_list = gson.fromJson(response.getString("top_selling_product"), listType);
                                combo_adapter = new Top_Selling_Adapter(combo_list);
                                combo_recycler.setAdapter(combo_adapter);
                                combo_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(jsonRequest);

        /*CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.GET_TOP_SELLING_PRODUCTS, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            if (!response.names().toJSONObject(response.names()).has("top_selling_product")) {
                                //Toast.makeText(getActivity(), "No Deal For Day", Toast.LENGTH_SHORT).show();
                                topselling_framelayout.setVisibility(View.GONE);
                               *//* Deal_Frame_layout.setVisibility(View.GONE);
                                Deal_Frame_layout1.setVisibility(View.GONE);
                                Deal_Linear_layout.setVisibility(View.GONE);*//*
                            } else {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<Top_Selling_model>>() {
                                }.getType();
                                combo_list = gson.fromJson(response.getString("top_selling_product"), listType);
                                combo_adapter = new Top_Selling_Adapter(combo_list);
                                combo_recycler.setAdapter(combo_adapter);
                                combo_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);*/
    }


    //Check top_selling_product api for offer product
    /*private void make_offer_product()
    {
        String tag_json_obj = "json_category_req";
        isSubcat = false;
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;
       *//* if (parent_id != null && parent_id != "") {
        }*//*

        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.GET_TOP_SELLING_PRODUCTS, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Top_Selling_model>>() {
                            }.getType();
                            offers_list = gson.fromJson(response.getString("top_selling_product"), listType);
                            offer_adapter = new Top_Selling_Adapter(offers_list);
                            offer_recycler.setAdapter(offer_adapter);
                            offer_adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }*/


    private void make_menu_items() {
        String tag_json_obj = "json_category_req";
        isSubcat = false;
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;
       /* if (parent_id != null && parent_id != "") {
        }*/
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BaseURL.GET_CATEGORY_URL+"?parent=",
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d(TAG, response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Home_Icon_model>>() {
                            }.getType();
                            menu_models = gson.fromJson(response.getString("data"), listType);
                            menu_adapter = new Home_Icon_Adapter(menu_models);
                            rv_headre_icons.setAdapter(menu_adapter);
                            menu_adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        Volley.newRequestQueue(getActivity()).add(jsonRequest);

        /*CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.GET_CATEGORY_URL, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Home_Icon_model>>() {
                            }.getType();
                            menu_models = gson.fromJson(response.getString("data"), listType);
                            menu_adapter = new Home_Icon_Adapter(menu_models);
                            rv_headre_icons.setAdapter(menu_adapter);
                            menu_adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);*/

    }

    private void make_shop_items() {
        isSubcat = false;
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BaseURL.SHOP_LIST,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d(TAG, response.toString());
                try {
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Shop_List_Model>>() {
                            }.getType();
                            shop_models = gson.fromJson(response.getString("data"), listType);
                            if(shop_models.size() > 0){
                                ll_branded_shops.setVisibility(View.VISIBLE);
                                Shop_List_Adapter menu_adapter = new Shop_List_Adapter(shop_models);
                                rv_branded_shops.setAdapter(menu_adapter);
                                menu_adapter.notifyDataSetChanged();
                            }else {
                                ll_branded_shops.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }

    @Override
    public void onRefresh() {
        swipRefresh.setRefreshing(true);
        swipRefresh.setColorSchemeResources(R.color.colorAccent,  R.color.light_orange, R.color.green_light);
        if (NetworkConnection.connectionChecking(getActivity())) {
            makeGetCategoryRequest();
            fetch_subscription();
            makeGetSliderRequest();
            makeGetBannerSliderRequest();
//            makeGetFeaturedSlider();
            make_menu_items();
            make_deal_od_the_day();
//            make_top_selling();
            make_combo_packs();
            make_shop_items();
            //make_offer_product();
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
        swipRefresh.setRefreshing(false);
    }

    @Override
    public void onClickPurchase(String sub_id, String sub_amt, String s, String order_id, String user_id) {
        this.sub_id = sub_id ; this.sub_amt = sub_amt ;  this.order_id = order_id ;   this.user_id = user_id ;
        Toast.makeText(getActivity(), "Please wait redirecting to payment page...", Toast.LENGTH_SHORT).show();
        startPayment(sub_id, sub_amt);
    }

    @Override
    public void onPaymentSuccess(String s) {
        txn_id = s ;
        if (NetworkConnection.connectionChecking(getActivity())){
            updateTxnId(sub_id, sub_amt, txn_id, order_id, user_id);
        }
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(getActivity(), "Payment Cancelled By User", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_all_branded_shops :
                Fragment fm = new BrandedShopsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
                break;
            case R.id.ll_location :
                Intent i = new Intent(getActivity(), GeoLocationActivity.class);
                i.putExtra("fromHome", true);
                getActivity().startActivity(i);
                break;
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {
        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        //Defining retrofit api service

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public void reviewOnApp() {
        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
        }
    }

    public void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi friends i am using ." + " http://play.google.com/store/apps/details?id=" + getActivity().getPackageName() + " APP");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }

        public interface ClickListener {
            void onClick(View view, int position);

            void onLongClick(View view, int position);
        }
    }

    public void startPayment(String sub_id, String sub_amount) {
        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();
        /**
         * Set your logo here
         */
        checkout.setImage(R.drawable.logo);
        /**
         * Reference to current activity
         */
        final Activity a = getActivity();

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();
            /**
             * Merchant Name
             * eg: ACME Corp || HasGeek etc.
             */
            options.put("name", "Surfcity Food Order");
            /**
             * Description can be anything
             * eg: Order #123123
             *     Invoice Payment
             *     etc.
             */
            options.put("description", "Surfcity Subscription ID #"+sub_id);
            options.put("currency", "INR");
            /**
             * Amount is always passed in PAISE
             * Eg: "500" = Rs 5.00
             */
            float amountstr = Float.parseFloat(sub_amount);
            options.put("amount", amountstr * 100);
            checkout.open(a, options);
        } catch(Exception e) {
            Log.e("SubscriptionPlan", "Error in starting Razorpay Checkout", e);
        }
    }

    public void updateTxnId(final String sub_id, String sub_amt, String s, String order_id, String user_id) {
        String url = BaseURL.PURCHASE_PLAN+"?user_id="+user_id+"&subscription_id="+sub_id+"&order_id="+order_id+"&txt_id="+s+"&amount="+sub_amt;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo", response.toString());
                try {
                    if (response.getBoolean("responce")) {
                        SharePreferenceUtility.saveStringPreferences(getActivity(), Const.SUBSCRIPTION_ID, sub_id);
                        Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                        subscription.clear();
                        club_recycler.setAdapter(null);
                        if (NetworkConnection.connectionChecking(getActivity())){
                            fetch_subscription();
                        }

                    }else {
                        Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(jsonRequest);
    }
    public void reviewOnApp(String url) {
//        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Uri uri = Uri.parse(url);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }
}
