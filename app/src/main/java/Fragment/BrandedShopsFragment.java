package Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapter.BrandedShopAdapter;
import Adapter.Shop_List_Adapter;
import Adapter.Shop_Now_adapter;
import Config.BaseURL;
import Model.ShopNow_model;
import Model.Shop_List_Model;
import util.RecyclerTouchListener;

public class BrandedShopsFragment extends Fragment {
    private static String TAG = Shop_Now_fragment.class.getSimpleName();
    private RecyclerView rv_items;
    private List<Shop_List_Model> shop_models = new ArrayList<>();
    private BrandedShopAdapter adapter;
    private boolean isSubcat = false;
    TextView firebase ;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    public BrandedShopsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_now, container, false);
        setHasOptionsMenu(true);

        ((MainActivity) getActivity()).setTitle("Branded Shops");

        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        rv_items = (RecyclerView) view.findViewById(R.id.rv_home);
        firebase = (TextView) view.findViewById(R.id.firebase);
        firebase.setText("Branded Shops");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        rv_items.setLayoutManager(gridLayoutManager);
        // rv_items.addItemDecoration(new GridSpacingItemDecoration(10, dpToPx(-25), true));
        rv_items.setItemAnimator(new DefaultItemAnimator());
        rv_items.setNestedScrollingEnabled(true);

        if (NetworkConnection.connectionChecking(getActivity())) {
            make_shop_items();
        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }

        rv_items.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_items, new RecyclerTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Bundle args = new Bundle();
                Fragment fm = new Product_fragment();
                args.putString("shop_id", shop_models.get(position).getShop_id());
                args.putString("shop_name", shop_models.get(position).getShop_name());
                fm.setArguments(args);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                        .addToBackStack(null).commit();
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));


        return view;
    }
    private void make_shop_items() {
        isSubcat = false;
        alertDialog.show();
        Map<String, String> params = new HashMap<String, String>();
        params.put("parent", "");
        isSubcat = true;

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BaseURL.SHOP_LIST,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d(TAG, response.toString());
                try {
                    alertDialog.dismiss();
                    if (response != null && response.length() > 0) {
                        Boolean status = response.getBoolean("responce");
                        if (status) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Shop_List_Model>>() {
                            }.getType();
                            shop_models = gson.fromJson(response.getString("data"), listType);
                            if(shop_models.size() > 0){
                                adapter = new BrandedShopAdapter(shop_models);
                                rv_items.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }else {
                                Toast.makeText(getActivity(), "No Shops Available", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }
}
