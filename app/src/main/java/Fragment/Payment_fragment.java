package Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.RazorpayActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import Config.BaseURL;
import Config.SharedPref;
import com.adjit.surfcity.AppController;
import com.adjit.surfcity.MainActivity;

/*import com.adjit.bhaijan.PaymentGatWay;
import com.adjit.bhaijan.Paytm;*/
import com.adjit.surfcity.R;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.Session_management;

import static com.android.volley.VolleyLog.TAG;
import static com.android.volley.VolleyLog.e;


public class Payment_fragment extends Fragment {
    RelativeLayout confirm;
    private DatabaseHandler db_cart;
    private Session_management sessionManagement;
    TextView payble_ammount, my_wallet_ammount;
    private String getlocation_id = "";
    private String getstore_id = "";
    private String gettime = "";
    private String getdate = "";
    private String getuser_id = "";
    String sub_plan;
    private Double rewards;
    RadioButton rb_Store, rb_Cod, rb_card, rb_Netbanking, rb_paytm, tv_net_banking, tv_wallet, tv_upi;

    CheckBox checkBox_Wallet, checkBox_coupon;
    EditText et_Coupon;
    String getvalue = "";
    String text;
    String cp;
    String net_total_amount,total_amout,coupon_amt,wallet_amt_t, cashback, coupons_id, total_product_price, society_id, delivery_charge;
    RadioGroup radioGroup;
    String Prefrence_TotalAmmount;
    String getwallet;
    LinearLayout Promo_code_layout;
    TextView Apply_Coupon_Code;
    String msg;
    //String orderid;
    String mobile_num="",emailid="", del_address;
    String name;
    public  String sale_id;
    PaytmPGService Service;
    HashMap<String, String> paramMap;
    JSONObject jsoNdata;
    PaytmOrder Order;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;


    public Payment_fragment() {

    }

    public static Payment_fragment newInstance(String param1, String param2) {
        Payment_fragment fragment = new Payment_fragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_payment_method, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.payment));

        Prefrence_TotalAmmount = SharedPref.getString(getActivity(), BaseURL.TOTAL_AMOUNT);

        sessionManagement = new Session_management(getActivity());
        mobile_num=sessionManagement.getUserDetails().get(BaseURL.KEY_MOBILE);
        name = sessionManagement.getUserDetails().get(BaseURL.KEY_NAME);
        emailid=sessionManagement.getUserDetails().get(BaseURL.KEY_EMAIL);
        del_address = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.DEL_ADDRESS, SharePreferenceUtility.PREFTYPE_STRING);

        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        getuser_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);

        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                if(checkedId == R.id.use_COD) {
                    getvalue = "COD"; //radioButton.getText().toString();
                }
                if(checkedId == R.id.use_wallet_ammount || checkedId == R.id.tv_net_banking || checkedId == R.id.tv_wallet || checkedId == R.id.tv_upi) {
                    getvalue = "Online Payment";
                }
                Log.e("valueradiobtn",getvalue);
            }
        });
        
        //Random r = new Random(System.currentTimeMillis());
       // orderid = "ORDER" + (1 + r.nextInt(2)) * 10000 + r.nextInt(10000);
      //  Log.e("orderId",orderid);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "Font/Bold.otf");

        rb_Cod = (RadioButton) view.findViewById(R.id.use_COD);   rb_Cod.setTypeface(font);

        tv_net_banking = (RadioButton) view.findViewById(R.id.tv_net_banking); tv_net_banking.setTypeface(font);
        tv_wallet = (RadioButton) view.findViewById(R.id.tv_wallet); tv_wallet.setTypeface(font);
        tv_upi = (RadioButton) view.findViewById(R.id.tv_upi); tv_upi.setTypeface(font);
        rb_paytm = (RadioButton) view.findViewById(R.id.use_wallet_ammount);rb_paytm.setTypeface(font);

        et_Coupon = (EditText) view.findViewById(R.id.et_coupon_code);
        cp = et_Coupon.getText().toString();
        Promo_code_layout = (LinearLayout) view.findViewById(R.id.prommocode_layout);
        Apply_Coupon_Code = (TextView) view.findViewById(R.id.apply_coupoun_code);

        //Show  Wallet
        getwallet = SharedPref.getString(getActivity(), BaseURL.KEY_WALLET_Ammount);

        db_cart = new DatabaseHandler(getActivity());
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Fragment fm = new Home_fragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                            .addToBackStack(null).commit();
                    return true;
                }
                return false;
            }
        });

        net_total_amount = getArguments().getString("total");
        total_amout=getArguments().getString("total_amout");
        coupon_amt=getArguments().getString("coupon_amt");
        wallet_amt_t=getArguments().getString("wallet_amt_t");
        getdate = getArguments().getString("getdate");
        gettime = getArguments().getString("gettime");
        getlocation_id = getArguments().getString("getlocationid");
        getstore_id = getArguments().getString("getstoreid");
        sub_plan = getArguments().getString("sub_plan");

        cashback = getArguments().getString("cashback");
        coupons_id = getArguments().getString("coupons_id");
        total_product_price = getArguments().getString("total_product_price");
        society_id = getArguments().getString("society_id");
        delivery_charge = getArguments().getString("delivery_charge");

        Log.e("stroreid",getArguments().getString("getstoreid"));

        payble_ammount =  view.findViewById(R.id.payable_ammount);
        payble_ammount.setText(getActivity().getString(R.string.currency) + net_total_amount);
        confirm = (RelativeLayout) view.findViewById(R.id.confirm_order);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    checked();
            }
        });
        return view;
    }

    @Override public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setVisible(false);
    }

    private void checked() {
        if(getvalue != null && !TextUtils.isEmpty(getvalue)) {
            if (NetworkConnection.connectionChecking(getActivity())) {
                makeAddOrderRequest(getdate, gettime, getuser_id, getlocation_id, getstore_id, total_amout, coupon_amt, wallet_amt_t, net_total_amount);
            } else {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(getActivity(), "Please select payment method", Toast.LENGTH_SHORT).show();
        }
    }

    //PLACE PAYMENT API ON COD
    private void makeAddOrderRequest(String date, String gettime, final String userid, String location, String store_id, final String total_amout,
                                     String coupon_amt, final String wallet_amt_t, String net_total_amount) {
        alertDialog.show();
        String tag_json_obj = "json_add_order_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("date", date);
        params.put("time", gettime);
        params.put("user_id", userid);
        params.put("location", location);//location
        params.put("store_id", "1");//store_id
        params.put("payment_method", getvalue);//COD * OnlinePayment
        params.put("net_total_amt", net_total_amount);
        params.put("total_amt", total_amout);
        params.put("cou_amt", coupon_amt);
        params.put("wallet_amt", wallet_amt_t);
        params.put("socity_id", society_id);
        params.put("cashback", cashback);
        params.put("total_product_amt", total_product_price);
        params.put("coupons_id", coupons_id);
        params.put("delivery_charge", delivery_charge);

        Log.e("valuesorder",date +""+ gettime+" "+userid+" "+location+" "+" "+store_id+" "+getvalue );

        String url = BaseURL.ADD_ORDER_URL+"?date="+date+"&time="+gettime+"&user_id="+userid
                +"&location="+location+"&store_id=1"+"&payment_method="+getvalue
                +"&net_total_amt="+net_total_amount+"&total_amt="+total_amout
                +"&cou_amt="+coupon_amt+"&wallet_amt="+wallet_amt_t
                +"&socity_id="+society_id
                +"&cashback="+cashback
                +"&total_product_amt="+total_product_price
                +"&coupons_id="+coupons_id
                +"&delivery_charge="+delivery_charge+"&mobile="+mobile_num+"&address="+del_address;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d(TAG, response.toString());
                Log.e("resposeorder", response.toString());
                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        msg = response.getString("message");
                        sale_id=response.getString("order_id");
                        Log.e("saleId_orderId",sale_id);
                        if(getvalue.equals("Online Payment")){//start razer payment
/*                            Intent intent = new Intent(getActivity(),RazorpayActivity.class);
                            intent.putExtra("amount", total_amout);//total_amount
                            intent.putExtra("useremailid", emailid);
                            intent.putExtra("usermobile", mobile_num);
                            intent.putExtra("billid", sale_id);
                            intent.putExtra("msg", msg);
                            startActivity(intent);*/
                            alertDialog.dismiss();
                            Toast.makeText(getActivity(), "Please wait, redirecting to payment page...", Toast.LENGTH_SHORT).show();
                            startPayment(sale_id);
                        }else {
                            alertDialog.dismiss();
                            Bundle args = new Bundle();
                            Fragment fm = new Thanks_fragment();
                            args.putString("msg", msg);
                            args.putString("sale_id", sale_id);
                            fm.setArguments(args);
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                        }
                    }else {
                        alertDialog.dismiss();
                        Toast.makeText(getActivity(), ""+response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        queue.add(jsonRequest);
    }

    private void sms_api_customer() {
        String ordemsg="Your Order is successfully placed, Your orderID is  "+ sale_id+" For any help call us at 9405454673 ";
        String sms_url= null;
        try {
            sms_url = "http://trans.surfcity.in/api/sendmsg.php?user=surfcityotp&pass=nihal@balaji@77&sender=SRFCTY&phone=="+mobile_num+
                    "&text=" + URLEncoder.encode(ordemsg,"UTF-8").toString()+
                    "&priority=ndnd&stype=normal";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.e("smsurl",sms_url);
        if (NetworkConnection.connectionChecking(getActivity())) {
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, sms_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("smsres",response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            rq.add(stringRequest);
        }
    }

    private void sms_api() {
        String ordemsg="You have received new order from "+name+" with order id "+sale_id+" mobile number "+mobile_num;
        String sms_url= null;
        try {
            //sms_url = "http://trans.surfcity.in/api/sendmsg.php?user=surfcityotp&pass=nihal@balaji@77&sender=SRFCTY&phone=9405454673&text=Test%20SMS&priority=ndnd&stype=normal";
            /*sms_url = "http://sms.adjinfotech.com/api/sendmsg.php?user=bhaijaan&pass=bhaijaan&sender=BHIJAN&phone=8906005050,9867777860"+
                    "&text=" + URLEncoder.encode(ordemsg,"UTF-8").toString()+
                    "&priority=ndnd&stype=normal";*/
            sms_url = "http://trans.surfcity.in/api/sendmsg.php?user=surfcityotp&pass=nihal@balaji@77&sender=SRFCTY&phone=8862025200"+
                    "&text=" + URLEncoder.encode(ordemsg,"UTF-8").toString()+
                    "&priority=ndnd&stype=normal";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.e("smsurl",sms_url);
        if (NetworkConnection.connectionChecking(getActivity())) {
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, sms_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("smsres",response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            rq.add(stringRequest);
        }
    }

    /*
    * Razor Payment start
    * */
    public void startPayment(String orderid) {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Checkout co = new Checkout();
        try {
            JSONObject options = new JSONObject();
            options.put("name", name);
            //options.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            //options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            float amountstr = Float.parseFloat(net_total_amount);
            options.put("amount", amountstr * 100);//amountstr
            //Log.d("testamount", String.valueOf(amountstr*100));
            //amount,useremailid,usermobile
            JSONObject preFill = new JSONObject();
            preFill.put("email", emailid);
            preFill.put("contact", mobile_num);
            options.put("prefill", preFill);

            co.open(getActivity(), options);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void edit_orderpayemnt_api(final String razorpayPaymentID, final String orderid) {
        alertDialog.show();
        if (NetworkConnection.connectionChecking(getActivity())) {
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            String url = BaseURL.edit_order_payment+"?sale_id="+sale_id+"&txn_id="+razorpayPaymentID+"&order_id="+sale_id+"&user_id="+getuser_id;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    alertDialog.dismiss();
                    Log.e("orderpaytm",response);
                    Bundle args = new Bundle();
                    Fragment fm = new Thanks_fragment();
                    args.putString("msg", msg);
                    args.putString("sale_id", sale_id);
                    fm.setArguments(args);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertDialog.dismiss();
                }
            });
            rq.add(stringRequest);
        }
    }

    /**
     *  wallet amount updating api
     * **/
    private void makeWalletUpdateRequest(String wallet_amt,String user_id) {
        HashMap<String,String> wallet_body = new HashMap<>();

        wallet_body.put("user_id",user_id);
        wallet_body.put("amt",wallet_amt);
        wallet_body.put("flag","1");

        CustomVolleyJsonRequest wallet_update_req = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.UPDATE_WALLET_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("wallet_update_response",response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(wallet_update_req,"update_wallet_amount");
    }
}
