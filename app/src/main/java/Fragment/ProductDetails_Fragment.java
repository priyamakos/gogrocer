package Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adjit.surfcity.LoginOrReg;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Adapter.Units_Adaptor;
import Config.BaseURL;
import Model.Product_deatail_Model;
import Model.Unit_Model;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.Session_management;

public class ProductDetails_Fragment extends Fragment implements TabLayout.OnTabSelectedListener, View.OnClickListener {
    DatabaseHandler dbcart;
    TabLayout rl_tabview;
    RelativeLayout rl_highlights,rl_info;
    RecyclerView rv_unit;
    ArrayList<Unit_Model> units;
    RecyclerView.LayoutManager manager;
    HashMap<String,String> mMap = null;
    LinearLayout btn_bottom_view;
    ImageView iv_plus,iv_minus,iv_image;
    TextView tv_contetiy,tv_pay_amount,tv_title,tv_detail,tv_mrphval,tv_clubhval,tv_nonclubhval,
            tv_unit_details,tv_productInfo, tv_deal, tv_deal_price, tv_unit, tv_unit1, tv_unit2;
    Session_management cart_mangement;
    Units_Adaptor unit_adaptor;
    int Login_AT_TO_CART = 1;
    double pay_amount = 0 ; int selected = 1 ;
    Product_deatail_Model product_model = null ;
    int qty ;
    private Session_management sessionManagement ;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    String max_qty, subscription_id, user_id;
    private String cart_count = "0", cart_count1 = "0", cart_count2 = "0", unit, unit_value ; Boolean is_deal_prod = false ;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_product_detail, container, false);

        iv_image = view.findViewById(R.id.iv_product_detail_img);
        iv_minus = view.findViewById(R.id.iv_subcat_minus);
        iv_plus = view.findViewById(R.id.iv_subcat_plus);
        tv_title = view.findViewById(R.id.tv_product_detail_title);
        tv_detail = view.findViewById(R.id.tv_product_detail);
        tv_clubhval = view.findViewById(R.id.tv_clubhval);
        tv_nonclubhval = view.findViewById(R.id.tv_nonclubhval);
        tv_unit_details = view.findViewById(R.id.tv_unit_detail);
        tv_productInfo = view.findViewById(R.id.tv_productInfo);
        rl_info = view.findViewById(R.id.rl_info);
        btn_bottom_view = view.findViewById(R.id.btn_bottom_view);
        tv_deal = view.findViewById(R.id.tv_deal);
        tv_deal_price = view.findViewById(R.id.tv_deal_price);

        tv_unit = view.findViewById(R.id.tv_unit);
        tv_unit.setOnClickListener(this);
        tv_unit1 = view.findViewById(R.id.tv_unit1);
        tv_unit1.setOnClickListener(this);
        tv_unit2 = view.findViewById(R.id.tv_unit2);
        tv_unit2.setOnClickListener(this);

        TabLayout tb_tabview = view.findViewById(R.id.tb_tabview);
        tv_mrphval = view.findViewById(R.id.tv_mrphval);
        rl_tabview = view.findViewById(R.id.tb_tabview);
        rl_highlights = view.findViewById(R.id.rl_highlights);
        rv_unit = view.findViewById(R.id.rv_units);
        tv_pay_amount = view.findViewById(R.id.tv_pay_amount);
        tv_contetiy = view.findViewById(R.id.tv_subcat_contetiy);
        final TextView tv_add = view.findViewById(R.id.tv_subcat_add);
        cart_mangement = new Session_management(getActivity());
        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        tb_tabview.addTab(tb_tabview.newTab().setText("Highlights"));
        tb_tabview.addTab(tb_tabview.newTab().setText("Info"));

        dbcart = new DatabaseHandler(getActivity());

        tb_tabview.setOnTabSelectedListener(this);

        units = new ArrayList<Unit_Model>();
        manager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        rv_unit.setLayoutManager(manager);
        unit_adaptor = new Units_Adaptor(units,getActivity());
        rv_unit.setAdapter(unit_adaptor);
        btn_bottom_view.setOnClickListener(this);

        sessionManagement = new Session_management(getActivity());
        user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);

        BaseURL.sub_id = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
        if(BaseURL.sub_id == null || BaseURL.sub_id.equalsIgnoreCase("")) {
            BaseURL.sub_id = "0";
        }

        Bundle bundle = getArguments();

        if(bundle != null) {
            if (NetworkConnection.connectionChecking(getActivity())){
                fetch_item_details(bundle.getString("product_id"));
            }else {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
            if(bundle.getString("max_qty")!=null) {
                ((MainActivity) getActivity()).setTitle("Deal of the day");
                max_qty = bundle.getString("max_qty");
                is_deal_prod = true ;
            }else{
                max_qty = "";
                is_deal_prod = false ;
            }
        }

        if(sessionManagement.isLoggedIn()) {
            Log.d("userId",sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
        }

        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* HashMap<String, String> map = new HashMap<>();
                map.put("product_id", modelList.get(position).getProduct_id());
                map.put("product_name", modelList.get(position).getProduct_name());
                map.put("category_id", modelList.get(position).getCategory_id());
                map.put("product_description", modelList.get(position).getProduct_description());
                map.put("deal_price", modelList.get(position).getDeal_price());
                map.put("start_date", modelList.get(position).getStart_date());
                map.put("start_time", modelList.get(position).getStart_time());
                map.put("end_date", modelList.get(position).getEnd_date());
                map.put("end_time", modelList.get(position).getEnd_time());
                map.put("price", modelList.get(position).getPrice());
                map.put("product_image", modelList.get(position).getProduct_image());
                map.put("status", modelList.get(position).getStatus());
                map.put("in_stock", modelList.get(position).getIn_stock());
                map.put("unit_value", modelList.get(position).getUnit_value());
                map.put("unit", modelList.get(position).getUnit());
                map.put("increament", modelList.get(position).getIncreament());
                map.put("rewards", modelList.get(position).getRewards());
                map.put("stock", modelList.get(position).getStock());
                map.put("title", modelList.get(position).getTitle());
                if (!tv_contetiy.getText().toString().equalsIgnoreCase("0")) {
                if (dbcart.isInCart(map.get("product_id"))) {
                dbcart.setCart(map, Float.valueOf(tv_contetiy.getText().toString()));
                tv_add.setText(context.getResources().getString(R.string.tv_pro_update));
                } else {
                dbcart.setCart(map, Float.valueOf(tv_contetiy.getText().toString()));
                tv_add.setText(context.getResources().getString(R.string.tv_pro_update));
                }
                } else {
                dbcart.removeItemFromCart(map.get("product_id"));
                tv_add.setText(context.getResources().getString(R.string.tv_pro_add));
                }
                Double items = Double.parseDouble(dbcart.getInCartItemQty(map.get("product_id")));
                Double price = Double.parseDouble(map.get("price"));
                ((MainActivity) context).setCartCounter("" + dbcart.getCartCount());
                notifyItemChanged(position);*/
            }
        });

        iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if(max_qty.equals("")) {*/
                    qty = Integer.valueOf(tv_contetiy.getText().toString());
                    qty = qty + 1;
                   /* if(Integer.parseInt(product_model.getIn_stock()) >= qty){*/
                        if (product_model != null) {
                            if(is_deal_prod){
                                pay_amount = pay_amount + Double.parseDouble(product_model.getDeal_price());
                                   unit = product_model.getDeal_unit();
                                   unit_value = product_model.getDeal_unit_value();
                            }else {
                                if (BaseURL.sub_id.equals("0")) {
                                    if (selected == 1) {
                                        pay_amount = pay_amount + Double.parseDouble(product_model.getSurfcity_price());
                                    }else if(selected == 2){
                                        pay_amount = pay_amount + Double.parseDouble(product_model.getSurfcity_price1());
                                    }else if(selected == 3){
                                        pay_amount = pay_amount + Double.parseDouble(product_model.getSurfcity_price2());
                                    }
                                } else {
                                    if(selected == 1) {
                                        pay_amount = pay_amount + Double.parseDouble(product_model.getSubscription_price());
                                    }else if(selected == 2){
                                        pay_amount = pay_amount +  Double.parseDouble(product_model.getSubscription_price1());
                                    }else if(selected == 3){{
                                        pay_amount = pay_amount +  Double.parseDouble(product_model.getSubscription_price2());
                                    }
                                    }
                                }
                                if(selected == 1) {
                                  unit = product_model.getUnit();
                                  unit_value = product_model.getUnit_value();
                                }else if(selected == 2){
                                    unit = product_model.getUnit1();
                                    unit_value = product_model.getUnit_value1();
                                }else if(selected == 3){{
                                    unit = product_model.getUnit2();
                                    unit_value = product_model.getUnit_value2();
                                }
                                }
                            }
                            add_to_cart_api(user_id, BaseURL.sub_id, product_model.getProduct_id(), "1",
                                    unit, unit_value);
                        }
            }
        });

        iv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = 0;
                if (!tv_contetiy.getText().toString().equalsIgnoreCase(""))
                    qty = Integer.valueOf(tv_contetiy.getText().toString());
                if (qty > 0) {
                    qty = qty - 1;
                    //tv_contetiy.setText(String.valueOf(qty))
                    if (product_model != null) {
                        if (is_deal_prod) {
                            pay_amount = pay_amount - Double.parseDouble(product_model.getDeal_price());
                            unit = product_model.getDeal_unit();
                            unit_value = product_model.getDeal_unit_value();
                        } else {
                            if (BaseURL.sub_id.equals("0")) {
                                if (selected == 1) {
                                    pay_amount = pay_amount -  Double.parseDouble(product_model.getSurfcity_price());
                                    unit = product_model.getUnit();
                                    unit_value = product_model.getUnit_value();
                                } else if (selected == 2) {
                                    pay_amount = pay_amount - Double.parseDouble(product_model.getSurfcity_price1());
                                    unit = product_model.getUnit1();
                                    unit_value = product_model.getUnit_value1();
                                } else if (selected == 3) {
                                    pay_amount = pay_amount - Double.parseDouble(product_model.getSurfcity_price2());
                                    unit = product_model.getUnit2();
                                    unit_value = product_model.getUnit_value2();
                                }
                            } else {
                                if (selected == 1) {
                                    pay_amount = pay_amount - Double.parseDouble(product_model.getSubscription_price());
                                    unit = product_model.getUnit();
                                    unit_value = product_model.getUnit_value();
                                } else if (selected == 2) {
                                    pay_amount = pay_amount - Double.parseDouble(product_model.getSubscription_price1());
                                    unit = product_model.getUnit1();
                                } else if (selected == 3) {
                                    pay_amount = pay_amount - Double.parseDouble(product_model.getSubscription_price2()) ;
                                    unit_value = product_model.getUnit_value1();
                                }
                            }
                        }
                        add_to_cart_api(user_id, BaseURL.sub_id, product_model.getProduct_id(), "-1", unit, unit_value);
                    }
                }
                 //pay_amount = Double.parseDouble(product_model.getPrice()) * qty;
                //tv_pay_amount.setText("You Pay "+ getString(R.string.currency)+String.valueOf(pay_amount));
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        if(sessionManagement.getsubscription() != null) {
            BaseURL.sub_id = (String) SharePreferenceUtility.getPreferences(getActivity(), Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
            if(BaseURL.sub_id == null || BaseURL.sub_id.equalsIgnoreCase("")) {
                BaseURL.sub_id = "0";
            }
        }
        Log.d("sub_id",BaseURL.sub_id);
        super.onResume();
    }

    private void fetch_item_details(String product_id) {
        alertDialog.show();
        String tag_json_obj = "json_item_details_req";
        HashMap<String,String> body = new HashMap<>();
        body.put("pro_id",product_id);

        String url = BaseURL.GET_PRODUCT_DETAILS+"?pro_id="+product_id+"&user_id="+user_id ;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("product_details",response.toString());
                alertDialog.dismiss();
                try {
                    if(response.getBoolean("responce")) {
                        Product_deatail_Model model = new Gson().fromJson(response.getJSONArray("data")
                                .getJSONObject(0).toString(),new TypeToken<Product_deatail_Model>(){}.getType());
                        Log.d("product_details",model.toString());
//                        cart_count = String.valueOf(model.getCart_count());
//                        cart_count1 = String.valueOf(model.getCart_count1());
//                        cart_count2 = String.valueOf(model.getCart_count2());
                        show_item_details(model);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure
                alertDialog.dismiss();
            }
        });
        queue.add(jsonRequest);
    }

    private void show_item_details(Product_deatail_Model product_model) {
        Unit_Model model = new Unit_Model();
        model.setName(product_model .getUnit_value()+" "+product_model.getUnit());
        model.setSelect(true);
        units.add(model);
        this.product_model = product_model;
        unit_adaptor.notifyDataSetChanged();
        if(product_model!= null) {
         /*   tv_productInfo.setText(product_model.getProduct_description());
            tv_title.setText(product_model.getProduct_name());
            tv_detail.setText(product_model.getProduct_description());*/
//tv_nonclubhval.setText(getActivity().getResources().getString(R.string.currency) + product_model.getPrice());

            tv_productInfo.setText(product_model.getProduct_description());
            tv_title.setText(product_model.getProduct_name());
            tv_detail.setText(product_model.getProduct_description());
            //tv_nonclubhval.setText(getActivity().getResources().getString(R.string.currency) + product_model.getPrice());

            tv_nonclubhval.setText("₹" + product_model.getSurfcity_price());
            tv_mrphval.setText("₹" + product_model.getPrice());
            tv_clubhval.setText("₹" + product_model.getSubscription_price());

            tv_unit_details.setText(product_model.getUnit());
           // Log.d("imageurl", BaseURL.IMG_PRODUCT_URL + produc|t_model.getProduct_image());

            if(is_deal_prod){
                tv_unit.setText(product_model.getDeal_unit_value() +" "+ product_model.getDeal_unit());
                tv_deal.setVisibility(View.VISIBLE);
                tv_deal_price.setVisibility(View.VISIBLE);
                tv_deal_price.setText("₹" +product_model.getDeal_price());
            }else {
                tv_unit.setText(product_model.getUnit_value() +" "+ product_model.getUnit());
                tv_deal.setVisibility(View.GONE);
                tv_deal_price.setVisibility(View.GONE);
            }
                tv_nonclubhval.setText("₹" + product_model.getSurfcity_price());
                tv_mrphval.setText("₹" + product_model.getPrice());
                tv_clubhval.setText("₹" + product_model.getSubscription_price());

            tv_unit_details.setText(product_model.getUnit());
            if(product_model.getPrice1().equalsIgnoreCase("0") || is_deal_prod){
                tv_unit1.setVisibility(View.GONE);
            }else {
                tv_unit1.setVisibility(View.VISIBLE);
                tv_unit1.setText(product_model.getUnit_value1() +" "+ product_model.getUnit1());
            }

            if(product_model.getPrice2().equalsIgnoreCase("0") || is_deal_prod){
                tv_unit2.setVisibility(View.GONE);
            }else {
                tv_unit2.setVisibility(View.VISIBLE);
                tv_unit2.setText(product_model.getUnit_value2() +" "+ product_model.getUnit2());
            }

            Log.d("imageurl",BaseURL.IMG_PRODUCT_URL + product_model.getProduct_image());

            if (NetworkConnection.connectionChecking(getActivity())){
                Glide.with(getActivity().getApplicationContext())
                        .load(BaseURL.IMG_PRODUCT_URL + product_model.getProduct_image()).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                }).centerCrop()
                        .placeholder(R.drawable.icon)
                        .crossFade()
                        .into(iv_image);
            }
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if(tab.getPosition() == 0) {
            rl_highlights.setVisibility(View.VISIBLE);
            rl_info.setVisibility(View.GONE);
        }else if(tab.getPosition() == 1) {
            rl_highlights.setVisibility(View.GONE);
            rl_info.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.btn_bottom_view) {
            if (NetworkConnection.connectionChecking(getActivity())){
                if (sessionManagement.isLoggedIn()) {
                    String userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
                    int currentQty = Integer.valueOf(tv_contetiy.getText().toString());
                    currentQty += Integer.parseInt(cart_count) ;
                    /*if(max_qty.equalsIgnoreCase(""))*/{
                        if (!tv_contetiy.getText().toString().equalsIgnoreCase("0")) {
                            if(!BaseURL.sub_id.equals("0"))
                                add_to_cart_api(userId, BaseURL.sub_id, product_model.getProduct_id(), String.valueOf(qty), unit, unit_value);
                            else {
                                add_to_cart_api(userId, "0", product_model.getProduct_id(), String.valueOf(qty), unit, unit_value);
                            }
                        }
                    }/*else if(currentQty <= Integer.parseInt(max_qty)){
                        if (!tv_contetiy.getText().toString().equalsIgnoreCase("0")) {
                            if(!BaseURL.sub_id.equals("0"))
                                add_to_cart_api(userId, BaseURL.sub_id, product_model.getProduct_id(), String.valueOf(qty));
                            else {
                                add_to_cart_api(userId, "0", product_model.getProduct_id(), String.valueOf(qty));
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), "Already added maximum products.", Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Intent intent = new Intent(getActivity(), LoginOrReg.class);
                    intent.putExtra("flag",Login_AT_TO_CART);
                    startActivity(intent);
                }
            }else {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }
        if(id == R.id.tv_unit){
            selected = 1 ;
            tv_contetiy.setText(cart_count);
            tv_unit.setBackgroundColor(getContext().getResources().getColor(R.color.green_light));
            tv_unit1.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            tv_unit2.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            if(is_deal_prod){
                tv_deal.setVisibility(View.VISIBLE);
                tv_deal_price.setVisibility(View.VISIBLE);
                tv_deal_price.setText("₹" +product_model.getDeal_price());
            }else {
                tv_deal.setVisibility(View.GONE);
                tv_deal_price.setVisibility(View.GONE);
            }
            tv_nonclubhval.setText("₹" + product_model.getSurfcity_price());
            tv_mrphval.setText("₹" + product_model.getPrice());
            tv_clubhval.setText("₹" + product_model.getSubscription_price());
        }
        if(id == R.id.tv_unit1){
            selected = 2 ;
            tv_contetiy.setText(cart_count1);
            tv_unit1.setBackgroundColor(getContext().getResources().getColor(R.color.green_light));
            tv_unit.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            tv_unit2.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            if(is_deal_prod){
                tv_deal.setVisibility(View.VISIBLE);
                tv_deal_price.setVisibility(View.VISIBLE);
                tv_deal_price.setText("₹" +product_model.getDeal_price());
            }else {
                tv_deal.setVisibility(View.GONE);
                tv_deal_price.setVisibility(View.GONE);
            }
            tv_nonclubhval.setText("₹" + product_model.getSurfcity_price1());
            tv_mrphval.setText("₹" + product_model.getPrice1());
            tv_clubhval.setText("₹" + product_model.getSubscription_price1());
        }
        if(id == R.id.tv_unit2){
            selected = 3 ;
            tv_contetiy.setText(cart_count2);
            tv_unit2.setBackgroundColor(getContext().getResources().getColor(R.color.green_light));
            tv_unit1.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            tv_unit.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            if(is_deal_prod){
                tv_deal.setVisibility(View.VISIBLE);
                tv_deal_price.setVisibility(View.VISIBLE);
                tv_deal_price.setText("₹" +product_model.getDeal_price());
            }else {
                tv_deal.setVisibility(View.GONE);
                tv_deal_price.setVisibility(View.GONE);
            }
            tv_nonclubhval.setText("₹" + product_model.getSurfcity_price2());
            tv_mrphval.setText("₹" + product_model.getPrice2());
            tv_clubhval.setText("₹" + product_model.getSubscription_price2());
        }
    }

    private void add_to_cart_api(String user_id, String sub_id, String pro_id, String quantity, String unit, String unit_value) {
        alertDialog.show();
        String tag_json_obj = "json_item_details_req";

        HashMap<String,String> body = new HashMap<>();
        body.put("user_id",user_id);
        body.put("sub_id", sub_id);
        body.put("pro_id",pro_id);
        body.put("qty", quantity);
        Log.d("body_cart",body.toString());

        String url = BaseURL.ADD_TO_CART+"?user_id="+user_id
                +"&sub_id="+sub_id
                +"&pro_id="+pro_id
                +"&qty="+quantity
                +"&price=0&unit="+unit+"&unit_value="+unit_value;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("product_details",response.toString());
                alertDialog.dismiss();
                try {
                    if(response.getBoolean("responce")) {
                        if(sessionManagement.isLoggedIn()) {
                            if (NetworkConnection.connectionChecking(getActivity())){
                                if (selected == 1) {
                                    cart_count = String.valueOf(qty);
                                }else if(selected == 2){
                                    cart_count1 = String.valueOf(qty);
                                }else if(selected == 3){
                                    cart_count2 = String.valueOf(qty);
                                }
                                tv_contetiy.setText(String.valueOf(qty));
                                tv_pay_amount.setText("You Pay " + getString(R.string.currency) + String.valueOf(pay_amount));
                                getCartcount(sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
                            }
                        }
                        Toast.makeText(getActivity(),response.getString("message"),Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        queue.add(jsonRequest);
    }

    private void getCartcount(String user_id) {
        HashMap<String,String> map = new HashMap<>();
        map.put("user_id",user_id);
        String url = BaseURL.GET_CART_COUNT+"?user_id="+user_id;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo",response.toString());
                try {
                    if (response.getBoolean("responce")) {
                        int item_count = response.getInt("data");
                        sessionManagement.setCartLogin(item_count);
                        ((MainActivity) getActivity()).setCartCounter(String.valueOf(item_count));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        queue.add(jsonRequest);
    }
}