package Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.AppController;
import com.adjit.surfcity.Club_membership;
import com.adjit.surfcity.CouponActivity;
import com.adjit.surfcity.LoginOrReg;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import Adapter.Cart_adapter;
import Adapter.Subscription_Adapter;
import Config.BaseURL;
import Model.CartModel;
import Model.Deal_Of_Day_model;
import Model.Subscription_model;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.Session_management;

/*
 * Created by Rajesh Dabhi on 26/6/2017.
 */
public class Cart_fragment extends Fragment implements View.OnClickListener {
    private static String TAG = Cart_fragment.class.getSimpleName();
    RelativeLayout rl_get_walletamt, rlProductDetails, rl_sublplan;
    private RecyclerView rv_cart;//,rv_subscriptions
    private TextView subplan_amt, tv_clear, tv_item, txttotalamt, txtwallet_amt, txtcoupon_amt, tv_subscript_title,
            tv_sub_duration, tv_sub_price, tv_sub_remove, tv_walletamt_textview,tv_total_product;//tv_total,
    private LinearLayout btn_checkout, bottom_layout, rv_promocde_layout, ll_cashback;
    private TextView tv_subscription_amount, tv_ftotal,tv_delivery_charges, tv_cashback;//,tv_subscript_plan
    private  ImageView imgv_getcoupencode, imgv_remove_wallet, imgv_remove_coupon;
    Subscription_Adapter sub_Adapter;
    ArrayList<Subscription_model> subscription;
    public ArrayList<Deal_Of_Day_model> temp ;
    private Context mContext;
    private DatabaseHandler db;
    String userId, coupan_id = "0";
    ArrayList<CartModel> cartdeails;
    private Session_management sessionManagement;
    Cart_adapter adapter;
    double total; Double wallet_amt_d ;
    Button btnVeifyCoupan;
    TextView tv_promocode;
    String net_total_amt, total_amout, coupon_amt, wallet_amt_t, sub_plan, subscription_id = "0", delivery_charges, total_product_price, delivery_charge;
    DecimalFormat twoDForm = new DecimalFormat("##.##");
    String cashback ;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;
    private String user_id;  Boolean isCouponClicked = false , isWalletClicked = false ;

    public Cart_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    double subscription_price;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        ((MainActivity) getActivity()).setTitle("Cart");
        mContext = getActivity();
        setHasOptionsMenu(true);

        sessionManagement = new Session_management(mContext);
        sessionManagement.cleardatetime();

        mDialogBuilder = new AlertDialog.Builder(mContext);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        db = new DatabaseHandler(getActivity());

        //  tv_clear = (TextView) view.findViewById(R.id.tv_cart_clear);
        //tv_total = (TextView) view.findViewById(R.id.tv_total);
        tv_total_product = view.findViewById(R.id.tv_total_product);
        tv_delivery_charges=view.findViewById(R.id.tv_delivery_charges);
        tv_item = (TextView) view.findViewById(R.id.tv_cart_item);
        btn_checkout = view.findViewById(R.id.btn_cart_checkout);
        rv_cart = (RecyclerView) view.findViewById(R.id.rv_cart);
        tv_subscription_amount = view.findViewById(R.id.tv_subscription_amount);
        rl_get_walletamt = view.findViewById(R.id.rl_get_walletamt);
        rv_promocde_layout = view.findViewById(R.id.rv_promocde_layout);
        rv_promocde_layout.setOnClickListener(this);
        bottom_layout = view.findViewById(R.id.bottom_layout);
        //tv_subscript_plan= view.findViewById(R.id.tv_subscript_plan);
        btnVeifyCoupan = view.findViewById(R.id.btnVeifyCoupan);
        tv_promocode = view.findViewById(R.id.tv_promocode);
        tv_ftotal = view.findViewById(R.id.tv_total);
        //rv_subscriptions = view.findViewById(R.id.rv_subscriptions);
        rl_sublplan = view.findViewById(R.id.rl_sublplan);
        txttotalamt = view.findViewById(R.id.totalamt);
        txtwallet_amt = view.findViewById(R.id.wallet_amt);
        txtcoupon_amt = view.findViewById(R.id.coupon_amt);
        rlProductDetails = view.findViewById(R.id.rlProductDetails);
        subplan_amt = view.findViewById(R.id.subplan_amt);
        tv_subscript_title = view.findViewById(R.id.tv_subscript_title);
        tv_sub_duration = view.findViewById(R.id.tv_sub_duration);
        tv_sub_price = view.findViewById(R.id.tv_sub_price);
        tv_sub_remove = view.findViewById(R.id.tv_sub_remove);
        tv_walletamt_textview = view.findViewById(R.id.tv_walletamt_textview);
        imgv_getcoupencode = view.findViewById(R.id.imgv_getcoupencode);
        imgv_getcoupencode.setOnClickListener(this);
        imgv_remove_wallet = view.findViewById(R.id.imgv_remove_wallet);
        imgv_remove_wallet.setOnClickListener(this);
        imgv_remove_coupon = view.findViewById(R.id.imgv_remove_coupon);
        imgv_remove_coupon.setOnClickListener(this);
        ll_cashback = view.findViewById(R.id.ll_cashback);
        tv_cashback= view.findViewById(R.id.tv_cashback);

        //subscription = new ArrayList<>();
        rv_cart.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Get user id  from session
        userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        tv_item.setText(String.valueOf(sessionManagement.getCartCount()));

        if (sessionManagement.getCartCount() == 0) {
            Fragment fm = new Empty_cart_fragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
        }

        db = new DatabaseHandler(getActivity());

        tv_subscription_amount.setText(String.valueOf(subscription_price));

        if (NetworkConnection.connectionChecking(getActivity())){
            if (sessionManagement.isLoggedIn()) {
                fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
            }
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
        // tv_clear.setOnClickListener(this);
        btn_checkout.setOnClickListener(this);
        rl_get_walletamt.setOnClickListener(this);
        btnVeifyCoupan.setOnClickListener(this);
        tv_sub_remove.setOnClickListener(this);
        return view;
    }

    @Override public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
            MenuItem menuItem = menu.findItem(R.id.action_cart);
            menuItem.setVisible(false);
        }

    private void checkSubscription() {
        user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        JsonObjectRequest subreq = new JsonObjectRequest(Request.Method.POST, BaseURL.SUBSCRIPTION_DETAILS+"?user_id="+user_id,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                alertDialog.dismiss();
                try {
                    if (response.getBoolean("responce")) {
                        Log.d("response", response.toString());
                        JSONObject data = response.getJSONObject("subscription_plan");
                        SharePreferenceUtility.saveStringPreferences(getActivity(), Const.SUBSCRIPTION_ID, data.getString("subscription_id"));

                        rl_sublplan.setVisibility(View.VISIBLE);
                        //Check jsonArray null/empty
//                        subscription_id = data.getString("subscription_id");
//                        String is_active = data.getString("is_active");
                        tv_subscript_title.setText(data.getString("subscription_name"));
                        tv_sub_duration.setText("Expires On :" + data.getString("subscription_end_date"));
                        tv_sub_price.setText("₹" + data.getString("unlock_member_price"));
                        subplan_amt.setText("₹" + data.getString("unlock_member_price"));
                    }else {
                        rl_sublplan.setVisibility(View.GONE);
                        SharePreferenceUtility.saveStringPreferences(getActivity(), Const.SUBSCRIPTION_ID, "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });

        AppController.getInstance().addToRequestQueue(subreq);
    }

    private void fetch_cartList(final String userId, String c_id, final String wallet_amt) {
        if (NetworkConnection.connectionChecking(getActivity())){
            alertDialog.show();
            HashMap<String, String> map = new HashMap<>();
            map.put("user_id", userId);
            map.put("coup_id", c_id);
            map.put("wallet_amount", wallet_amt);
            Log.d("body_cartList", map.toString());

            String url = BaseURL.CART_LIST + "?user_id=" + userId + "&coup_id=" + c_id + "&wallet_amount=" + wallet_amt;

            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url,
                    null, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onResponse(JSONObject response) {
                    //TODO: handle success

                    alertDialog.dismiss();
                    try {
                        rlProductDetails.setVisibility(View.VISIBLE);
                        Log.e("cartResponse", response.toString());
                        if (response.getBoolean("responce")) {
                            //cart item
                            cartdeails = new Gson().fromJson(response.getJSONArray("data").toString(),
                                    new TypeToken<ArrayList<CartModel>>() {
                                    }.getType());
                            if(!response.getString("message").equalsIgnoreCase("")) {
                                Toast.makeText(mContext, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                            //deal_product_id details
                            JSONArray productJsonArray = response.getJSONArray("deal_product_id");
                            temp = new ArrayList<>();
                            for (int i = 0; i < productJsonArray.length(); i++) {
                                JSONObject productJsonObject = productJsonArray.getJSONObject(i);
                                Deal_Of_Day_model deal_of_day_model = new Deal_Of_Day_model();
                                deal_of_day_model.setProduct_id(productJsonObject.getString("product_id"));
                                deal_of_day_model.setMax_qty(productJsonObject.getString("max_qty"));
                                temp.add(deal_of_day_model);
                            }

                            adapter = new Cart_adapter(getActivity(), cartdeails, temp);
                            rv_cart.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            if (NetworkConnection.connectionChecking(getActivity())){
                                checkSubscription();
                            }
                            //subscription plan
                            JSONArray jsonArray = response.getJSONArray("subscribe_data");
                            if (jsonArray.length() > 0) {
                                rl_sublplan.setVisibility(View.VISIBLE);
                                //Check jsonArray null/empty
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                subscription_id = jsonObject.getString("subscription_id");
                                String is_active = jsonObject.getString("is_active");
                                tv_subscript_title.setText(jsonObject.getString("subscription_name"));
                                tv_sub_duration.setText(jsonObject.getString("subscription_days") + " Days");
                                tv_sub_price.setText("₹" + jsonObject.getString("subscription_price"));
                                sub_plan = "0";
                                // subplan_amt.setText("Subscription Plan :           " +"₹"+jsonObject.getString("subscription_price"));
                            } else {
                                rl_sublplan.setVisibility(View.GONE);
                                sub_plan = "1";
                                // subplan_amt.setText("");
                            }
                            //tv_sub_remove.setText("");


                      /*  subscription = new Gson().fromJson(response.getJSONArray("subscribe_data").toString(),new TypeToken<ArrayList<Subscription_model>>(){}.getType());
                        sub_Adapter = new Subscription_Adapter(getActivity(), subscription, 1);
                        rv_subscriptions.setAdapter(sub_Adapter);
                        sub_Adapter.notifyDataSetChanged();*/
                            //if plan ues sub_plan is 0


                            //payment details
                            total_amout = response.getString("total_amt");
                            net_total_amt = response.getString("net_total_amt");
                            coupon_amt = response.getString("coupon_amt");
                            wallet_amt_t = response.getString("wallet_amt");
                            delivery_charges = response.getString("delivery_charge");
//                        subscription_plan_chanrges=response.getString("subscription_price");
                            total_product_price = response.getString("total_product_amt");
                            cashback = response.getString("cashback");
                            delivery_charge = response.getString("delivery_charge");
                            tv_total_product.setText("₹" + total_product_price);

                            wallet_amt_d = Double.valueOf(twoDForm.format(Double.valueOf(wallet_amt_t)));
                            Double net_total_amt_d = Double.valueOf(net_total_amt);

                            if(cashback.equalsIgnoreCase("0") || TextUtils.isEmpty(cashback)){
                                ll_cashback.setVisibility(View.GONE);
                            }else {
                                ll_cashback.setVisibility(View.VISIBLE);
                                tv_cashback.setText("₹"+cashback);
                            }

                            if(delivery_charge.equalsIgnoreCase("0") || TextUtils.isEmpty(delivery_charge)){
                                tv_delivery_charges.setText("Free");
                                tv_delivery_charges.setTextColor(getActivity().getResources().getColor(R.color.green_light));
                            }else {
                                tv_delivery_charges.setText("₹" +delivery_charge);
//                                tv_delivery_charges.setTextColor(getActivity().getResources().getColor(R.color.dark_black));
                            }

                            if (response.getString("iscouponapply").equals("0")
                                    || response.getString("iscouponapply").equals("")) {
//                            txtcoupon_amt.setVisibility(View.GONE);
                                coupan_id = "0";
                                txtcoupon_amt.setText("Not Applied");
                                imgv_remove_coupon.setVisibility(View.GONE);
                                tv_promocode.setText("Select Coupon");
                                if(isCouponClicked){
                                    Toast.makeText(mContext, "Coupon Not Applied", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                imgv_remove_coupon.setVisibility(View.VISIBLE);
                                if(response.getString("coupon_amt").equals("0")){
                                    txtcoupon_amt.setText("Cashback Offer Applied");
                                    tv_promocode.setText("Cashback Amount ₹" + cashback);
                                }else {
                                    txtcoupon_amt.setVisibility(View.VISIBLE);
                                    txtcoupon_amt.setText("₹" + coupon_amt);
                                    tv_promocode.setText("You Have Saved ₹" + coupon_amt);
                                }
                            }

                            Log.e("wallet_amt", response.getString("wallet_amt"));
                            if (response.getString("wallet_amt").equals("0.0")) {
//                            txtwallet_amt.setVisibility(View.GONE);
                                wallet_percent_amt = 0.0;
                                txtwallet_amt.setText("Not Used");
                                imgv_remove_wallet.setVisibility(View.GONE);
                                tv_walletamt_textview.setText("Check Wallet Amt");
                                if(isWalletClicked){
                                    Toast.makeText(mContext, "Can't Use Wallet Amount", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                txtwallet_amt.setVisibility(View.VISIBLE);
                                imgv_remove_wallet.setVisibility(View.VISIBLE);
                                Double wallet_amt_d = Double.valueOf(wallet_amt_t);
                                txtwallet_amt.setText("₹" + twoDForm.format(wallet_amt_d));
                                tv_walletamt_textview.setText("Wallet Amount Used ₹" + twoDForm.format(wallet_percent_amt));
                            }

                            isCouponClicked = false ;
                            isWalletClicked = false;
                        /*if(response.getString("subscription_price").equals("0")){
//                            subplan_amt.setVisibility(View.GONE);
                            subplan_amt.setText("Not a Member");
                        }else {
                            subplan_amt.setText("₹" + subscription_plan_chanrges);
                        }
*/
                            txttotalamt.setText("₹" + total_amout);
                            tv_ftotal.setText("₹" + twoDForm.format(net_total_amt_d));
//                            tv_delivery_charges.setText("₹" + delivery_charges);

                            if (NetworkConnection.connectionChecking(getActivity())){
                                getCartcount(userId);
                            }

                       /* if(sessionManagement.getCartCount()>0) {
                            NestedScrollView.setVisibility(View.VISIBLE);
                        }*/
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    //TODO: handle failure
                    alertDialog.dismiss();
                }
            });
            Volley.newRequestQueue(getActivity()).add(jsonRequest);
        }else{
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private void calculate_total(ArrayList<CartModel> cartdeails, String sub_id) {
        for (int k = 0; k < cartdeails.size(); k++) {
            if (sub_id.equals("")) {
                total = total + (Double.parseDouble(cartdeails.get(k).getPrice()) * Integer.parseInt(cartdeails.get(k).getQty()));
            } else {
                total = total + (Double.parseDouble(cartdeails.get(k).getSubscription_price()) * Integer.parseInt(cartdeails.get(k).getQty()));
            }

        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btn_cart_checkout) {
            if (NetworkConnection.connectionChecking(getActivity())){
                makeGetLimiteRequest();
            }else {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.rl_get_walletamt) {
            showSeekBarDialog();
        } else if (id == R.id.btnVeifyCoupan) {
        } else if (id == R.id.tv_sub_remove) {
            showClearDialog();
        }
        if(id == R.id.rv_promocde_layout) {
            isCouponClicked = true ;
            Intent intent = new Intent(getActivity(), CouponActivity.class);
            intent.putExtra("user_id", userId);
            intent.putExtra("wallet_amt", String.valueOf(wallet_percent_amt));
            startActivityForResult(intent, 1001);
        }
        switch (view.getId()) {
            case R.id.imgv_remove_coupon :
                coupan_id = "0" ;
                fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
                break;
            case R.id.imgv_remove_wallet :
                wallet_percent_amt = 0.0 ;
                fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1001 :
                if(!data.getStringExtra("coupon_id").equalsIgnoreCase("0")){
                    coupan_id  =  data.getStringExtra("coupon_id");
                    fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
                }else {
                    isCouponClicked = false;
                    Toast.makeText(mContext, "Coupon Not Applied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //update UI
    private void updateData() {
        // tv_total.setText(getActivity().getResources().getString(R.string.currency) + total);
        tv_item.setText("" + sessionManagement.getCartCount());
        //tv_ftotal.setText(getActivity().getResources().getString(R.string.currency) + net_total_amt);
        //tv_subscription_amount.setText(String.valueOf(subscription_price));
        //tv_ftotal.setText(String.valueOf(Double.parseDouble(tv_total.getText().toString().trim())+(int) subscription_price)+wallet_percent_amt);
        ((MainActivity) getActivity()).setCartCounter("" + sessionManagement.getCartCount());
    }

    private void showClearDialog() {
        AlertDialog.Builder alert_Dialog = new AlertDialog.Builder(getActivity());
        alert_Dialog.setMessage("Are you sure! you want to Remove Subscription Plan. ");
        alert_Dialog.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert_Dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //clear cart data
                //db.clearCart();
                //ArrayList<HashMap<String, String>> map = db.getCartAll();
              /*  Cart_adapter adapter  = new Cart_adapter(getActivity(), cartdeails);
                rv_cart.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                updateData();
            */
                if (NetworkConnection.connectionChecking(getActivity())){
                    remove_cart(sessionManagement.getUserDetails().get(BaseURL.KEY_ID), subscription_id);
                    dialogInterface.dismiss();
                }
            }
        });

        alert_Dialog.show();
    }

    int max = 0, min = 0, step = 1;
    Double wallet_amt_dbl = 0.0, wallet_percent_amt = 0.0;
    String wallet_amt, wallet_limit_min, wallet_limit_max;

    private void showSeekBarDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.seekbar_dialog);
        final Button btn_setwallamt = dialog.findViewById(R.id.select_walletamt_button);
        final SeekBar sb_set_wallet_amt = dialog.findViewById(R.id.wallet_dialog_seekbar);
        final TextView wall_amt_sb = dialog.findViewById(R.id.wall_amt_sb);
        final TextView min_wall_amt = dialog.findViewById(R.id.min_wall_amt);
        final TextView max_wall_amt = dialog.findViewById(R.id.max_wall_amt);
        final Button btn_wall_cont = dialog.findViewById(R.id.btn_wall_cont);
        final ImageView img_close = dialog.findViewById(R.id.imgv_close);

        if (NetworkConnection.connectionChecking(getActivity())) {
            alertDialog.show();
            String wallet_amt_url = BaseURL.WALLET_AMOUNT_URL + "?user_id=" + userId;
            Log.e("wallet_amt_url", wallet_amt_url);
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                    wallet_amt_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    alertDialog.dismiss();
                    Log.e("wallet_amt_res", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String success_status = jsonObject.getString("success");

                        if (success_status.equals("success")) {
                            dialog.show();
                            wallet_amt = jsonObject.getString("wallet");
                            wallet_amt_dbl = Double.valueOf(wallet_amt);
                            JSONArray jsonArray = jsonObject.getJSONArray("details");
                            JSONObject jsonDetails = jsonArray.getJSONObject(0);
                            wallet_limit_min = jsonDetails.getString("wallet_limit_min");
                            min = Integer.parseInt(wallet_limit_min);
                            wallet_limit_max = jsonDetails.getString("wallet_limit_max");
                            max = Integer.parseInt(wallet_limit_max);

                            wall_amt_sb.setText( "₹" + wallet_amt);
                            min_wall_amt.setText("Min \n" + "₹" + wallet_limit_min);
                            max_wall_amt.setText("Max \n" + "₹" + wallet_limit_max);

                            Log.d("sb_set_wallet", "" + max + " " + min);
                            sb_set_wallet_amt.setMax(max - min);
                        } else {
                            Toast.makeText(mContext, "No wallet amount", Toast.LENGTH_SHORT).show();
                            if(dialog.isShowing() && dialog!=null)
                                dialog.dismiss();
                          /*wallet_amt="0";
                            min=0;
                            max=0;*/
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertDialog.dismiss();
                }
            });
            rq.add(stringRequest);

            //sb_set_wallet_amt.setMin(min); required api level 26
            sb_set_wallet_amt.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    //   fetch_cartList(userId,coupan_id, String.valueOf(wallet_percent_amt));
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    int translatedProgress = progress + min;
                    Log.d("sb_set_wallet_amt", "" + translatedProgress);
                    wallet_percent_amt = Double.valueOf(translatedProgress);//((double) translatedProgress / wallet_amt_dbl) * 100;
                    Log.d("wallet_percent_amt", "" + wallet_percent_amt);
                    DecimalFormat twoDForm = new DecimalFormat("##.##");
                    btn_setwallamt.setText("" + " ₹"+ twoDForm.format(wallet_percent_amt) );
                }
            });
            // dialog.show();
        }else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
        btn_wall_cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isWalletClicked = true ;
                fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
                dialog.dismiss();
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wallet_percent_amt = 0.0;
                fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
                dialog.dismiss();
            }
        });
    }

    /**
     * wallet amount updating api
     **/
    private void makeWalletUpdateRequest(String wallet_amt, String user_id) {
        HashMap<String, String> wallet_body = new HashMap<>();

        wallet_body.put("user_id", user_id);
        wallet_body.put("amt", wallet_amt);
        wallet_body.put("flag", "1");

        CustomVolleyJsonRequest wallet_update_req = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.UPDATE_WALLET_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("wallet_update_response", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(wallet_update_req, "update_wallet_amount");
    }

    //Verify coupan and get coupan id
    private void verifyCoupan(String coupon_code) {
        alertDialog.show();
        HashMap<String, String> wallet_body = new HashMap<>();

        wallet_body.put("coupon_code", coupon_code);
        wallet_body.put("user_id",userId);

         String url = BaseURL.VERFY_COUPAN_URL+"?coupon_code="+coupon_code+"&user_id="+userId;

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                alertDialog.dismiss();
                //JSONObject jsonObject = new JSONObject();
                try {
                    boolean status = response.getBoolean("responce");

                    if (status) {
                        JSONArray jsonArray = response.getJSONArray("data");
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        coupan_id = jsonObject.getString("id");

                        String message = response.getString("message");
                        btnVeifyCoupan.setBackgroundColor(getResources().getColor(R.color.color_1));
                        tv_promocode.setEnabled(false);
                        btnVeifyCoupan.setText("Edit");
                        Toast.makeText(mContext, "Promo code applied successfully", Toast.LENGTH_SHORT).show();
                        fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));

                    } else {
                        btnVeifyCoupan.setText("Apply");
                        //String message = response.getString("message");
                        // Toast.makeText(mContext, "Invalid Coupan", Toast.LENGTH_SHORT).show();
                        Toast.makeText(mContext, "Enter valid promo code", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("wallet_update_response", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure
                alertDialog.dismiss();
            }
        });

        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }

    /**
     * Method to make json array request where json response starts with
     */
    private void makeGetLimiteRequest() {
        JsonArrayRequest req = new JsonArrayRequest(BaseURL.GET_LIMITE_SETTING_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        String amt = total_amout;
                        Double total_amount = Double.parseDouble(amt);

                        try {
                            //Parsing json array response
                            //loop through each json object
                            boolean issmall = false;
                            boolean isbig = false;
                            // arraylist list variable for store data;
                            ArrayList<HashMap<String, String>> listarray = new ArrayList<>();

                            for (int i = 0; i < response.length(); i++) {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                int value;

                                if (jsonObject.getString("id").equals("1")) {
                                    value = Integer.parseInt(jsonObject.getString("value"));
                                    if (total_amount < value) {
                                        issmall = true;
                                        Toast.makeText(getActivity(), "" + jsonObject.getString("title") + " : " + value, Toast.LENGTH_SHORT).show();
                                    }
                                } else if (jsonObject.getString("id").equals("2")) {
                                    value = Integer.parseInt(jsonObject.getString("value"));
                                    if (total_amount > value) {
                                        isbig = true;
                                        Toast.makeText(getActivity(), "" + jsonObject.getString("title") + " : " + value, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            if (!issmall && !isbig) {
                                if (sessionManagement.isLoggedIn()) {
                                    Bundle args = new Bundle();
                                    args.putString("total", net_total_amt);
                                    args.putString("total_amout", total_amout);
                                    args.putString("coupon_amt", coupon_amt);
                                    args.putString("wallet_amt_t", String.valueOf(wallet_percent_amt)); // String.valueOf(wallet_amt_d) Replaced wallet_amt_t by wallet_amt_d
                                    args.putString("sub_plan", sub_plan);

                                    args.putString("cashback", cashback);
                                    args.putString("coupons_id", coupan_id);
                                    args.putString("total_product_price", total_product_price);
                                    args.putString("delivery_charge", delivery_charge);
                                    Fragment fm = new Delivery_fragment();
                                    fm.setArguments(args);
                                    FragmentManager fragmentManager = getFragmentManager( );
                                    fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                                } else {
                                    //Toast.makeText(getActivity(), "Please login or regiter.\ncontinue", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getActivity(), LoginOrReg.class);
                                    startActivity(i);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), "Connection Time out", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }

    @Override
    public void onPause() {
        super.onPause();
        // unregister reciver
        getActivity().unregisterReceiver(mCart);
    }

    @Override
    public void onResume() {
        super.onResume();
        // register reciver
        getActivity().registerReceiver(mCart, new IntentFilter("Grocery_cart"));
    }

    // broadcast reciver for receive data
    private BroadcastReceiver mCart = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");

            if (type.contentEquals("update")) {
                updateData();
//                wallet_percent_amt = 0.0 ;
//                coupan_id = getArguments().getString("coupon_id");
                fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
            }
            // else if(type.contentEquals(""))
        }
    };

    public void getCartcount(String user_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        String url = BaseURL.GET_CART_COUNT+"?user_id="+user_id;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo", response.toString());
                try {
                    if (response.getBoolean("responce")) {
                        int item_count = response.getInt("data");
                        sessionManagement.setCartLogin(item_count);
//                        ((MainActivity) getActivity()).setCartCounter(String.valueOf(item_count));
                        if (sessionManagement.getCartCount() == 0) {
                            Fragment fm = new Empty_cart_fragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsonRequest);
    }

    //remove plan from cart list
    private void remove_cart(String user_id, String sub_id) {
        alertDialog.show();
        String tag_json_obj = "json_item_details_req";

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", user_id);
        body.put("sub_id", sub_id);

        Log.d("body_cart", body.toString());

        String url = BaseURL.CART_REMOVE+"?user_id="+user_id+"&sub_id="+sub_id;

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("product_details", response.toString());
                alertDialog.dismiss();
                try {
                    if (!response.getString("message").equals("")) {
                        SharePreferenceUtility.saveStringPreferences(getActivity(), Const.SUBSCRIPTION_ID, "");
                        Toast.makeText(getActivity(), response.getString("message"),
                                Toast.LENGTH_SHORT).show();
                        sessionManagement.update_subscription(null);
                        if (NetworkConnection.connectionChecking(getActivity())){
                            fetch_cartList(userId, coupan_id, String.valueOf(wallet_percent_amt));
                            getCartcount(sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }

}
