package util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import com.adjit.surfcity.LoginOrReg;
import com.adjit.surfcity.MainActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import Model.Cart_Details_Model;
import Model.Product_deatail_Model;
import Model.Subscription_model;
import static Config.BaseURL.IS_LOGIN;
import static Config.BaseURL.KEY_DATE;
import static Config.BaseURL.KEY_EMAIL;
import static Config.BaseURL.KEY_HOUSE;
import static Config.BaseURL.KEY_ID;
import static Config.BaseURL.KEY_IMAGE;
import static Config.BaseURL.KEY_MOBILE;
import static Config.BaseURL.KEY_NAME;
import static Config.BaseURL.KEY_PASSWORD;
import static Config.BaseURL.KEY_PAYMENT_METHOD;
import static Config.BaseURL.KEY_PINCODE;
import static Config.BaseURL.KEY_REWARDS_POINTS;
import static Config.BaseURL.KEY_SOCITY_ID;
import static Config.BaseURL.KEY_SOCITY_NAME;
import static Config.BaseURL.KEY_TIME;
import static Config.BaseURL.KEY_WALLET_Ammount;
import static Config.BaseURL.PREFS_NAME;
import static Config.BaseURL.PREFS_NAME2;
import static Config.BaseURL.TOTAL_AMOUNT;

/**
 * Created by Rajesh Dabhi on 28/6/2017.
 */

public class Session_management {

    SharedPreferences prefs;
    SharedPreferences prefs2;

    SharedPreferences.Editor editor;
    SharedPreferences.Editor editor2;

    Context context;

    int PRIVATE_MODE = 0;

    public Session_management(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(PREFS_NAME, PRIVATE_MODE);
        editor = prefs.edit();

        prefs2 = context.getSharedPreferences(PREFS_NAME2, PRIVATE_MODE);
        editor2 = prefs2.edit();
    }

    public void createLoginSession(String id, String email, String name
            , String mobile, String image, String wallet_ammount, String reward_point, String pincode, String socity_id,
                                   String socity_name, String house, String password) {

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_IMAGE, image);
        editor.putString(KEY_WALLET_Ammount, wallet_ammount);
        editor.putString(KEY_REWARDS_POINTS, reward_point);
        editor.putString(KEY_PINCODE, pincode);
        editor.putString(KEY_SOCITY_ID, socity_id);
        editor.putString(KEY_SOCITY_NAME, socity_name);
        editor.putString(KEY_HOUSE, house);
        editor.putString(KEY_PASSWORD, password);

        editor.commit();
    }

    public void checkLogin() {

        if (!this.isLoggedIn()) {
            Intent loginsucces = new Intent(context, LoginOrReg.class);
            // Closing all the Activities
            loginsucces.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            loginsucces.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(loginsucces);
        }
    }
    public   ArrayList<HashMap<String,String>> set_cart(HashMap<String,String> maps,String qty)
    {
        boolean contains = false;
        ArrayList<HashMap<String,String>> map;
        if(prefs.getString("cart","").equals(""))
        {
             map = new ArrayList();
            map.add(maps);
        // editor.putString("cart",new Gson().toJson(map);
          // editor.commit();
        }
        else {
            map = new Gson().fromJson(prefs.getString("cart",""),new TypeToken<HashMap<String,String>>(){}.getType());
            for(HashMap d : map){
                if(d.get("product_id") != null && d.get("product_id").toString().contains(maps.get("product_id")))
                {
                     d.put("qty",qty);
                     contains = true;
                }
                //something here
            }
            if(!contains)
            {
                map.add(maps);
            }
            editor.putString("cart",new Gson().toJson(map,new TypeToken<HashMap<String,String>>(){}.getType()));
            editor.commit();
        }
      return map;
    }


    public void update_subscription(Subscription_model model) {
      editor.putString("Subscription_Plan",  new Gson().toJson(model,new TypeToken<Subscription_model>(){}.getType()));
      editor.commit();
    }

    public Subscription_model getsubscription() {
        Subscription_model model = new Gson().fromJson(prefs.getString("Subscription_Plan",""),new TypeToken<Subscription_model>(){}.getType());
      //  Log.d("model",model.toString());
          if(model != null) {
              return  model;
          } else {
              return  null;
          }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_ID, prefs.getString(KEY_ID, null));
        // user email id
        user.put(KEY_EMAIL, prefs.getString(KEY_EMAIL, null));
        // user name
        user.put(KEY_NAME, prefs.getString(KEY_NAME, null));
        user.put(KEY_MOBILE, prefs.getString(KEY_MOBILE, null));
        user.put(KEY_IMAGE, prefs.getString(KEY_IMAGE, null));
        user.put(KEY_WALLET_Ammount, prefs.getString(KEY_WALLET_Ammount, null));
        user.put(KEY_REWARDS_POINTS, prefs.getString(KEY_REWARDS_POINTS, null));
        user.put(KEY_PAYMENT_METHOD, prefs.getString(KEY_PAYMENT_METHOD, ""));
        user.put(TOTAL_AMOUNT, prefs.getString(TOTAL_AMOUNT, null));
        user.put(KEY_PINCODE, prefs.getString(KEY_PINCODE, null));
        user.put(KEY_SOCITY_ID, prefs.getString(KEY_SOCITY_ID, null));
        user.put(KEY_SOCITY_NAME, prefs.getString(KEY_SOCITY_NAME, null));
        user.put(KEY_HOUSE, prefs.getString(KEY_HOUSE, null));
        user.put(KEY_PASSWORD, prefs.getString(KEY_PASSWORD, null));

        // return user
        return user;
    }

    public void updateData(String name, String mobile, String pincode
            , String socity_id , String wallet, String rewards,String house) {

        editor.putString(KEY_NAME, name);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_PINCODE, pincode);
        editor.putString(KEY_SOCITY_ID, socity_id);
        //editor.putString(KEY_IMAGE, image);
        editor.putString(KEY_WALLET_Ammount, wallet);
        editor.putString(KEY_REWARDS_POINTS, rewards);
        editor.putString(KEY_HOUSE, house);

        editor.apply();
    }

    public void updateSocity(String socity_name, String socity_id) {
        editor.putString(KEY_PINCODE, socity_name);
        editor.putString(KEY_SOCITY_ID, socity_id);

        editor.apply();
    }

    public void logoutSession() {
        editor.clear();
        editor.commit();

        cleardatetime();

        Intent logout = new Intent(context, MainActivity.class);
        // Closing all the Activities
        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(logout);
    }

    public void logoutSessionwithchangepassword() {
        editor.clear();
        editor.commit();

        cleardatetime();

        Intent logout = new Intent(context, LoginOrReg.class);
        // Closing all the Activities
        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(logout);
    }

    public void creatdatetime(String date, String time) {
        editor2.putString(KEY_DATE, date);
        editor2.putString(KEY_TIME, time);

        editor2.commit();
    }

    public void cleardatetime() {
        editor2.clear();
        editor2.commit();
    }

    public HashMap<String, String> getdatetime() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_DATE, prefs2.getString(KEY_DATE, null));
        user.put(KEY_TIME, prefs2.getString(KEY_TIME, null));

        return user;
    }

    // Get Login State
    public boolean isLoggedIn() {
        return prefs.getBoolean(IS_LOGIN, false);
    }
    public void addCart(Product_deatail_Model model)
    {
        ArrayList<Product_deatail_Model> product_model;
        if(prefs.getString("cart","").equals(""))
        {
            product_model = new ArrayList<>();
            product_model.add(model);
            editor.putString("cart",new Gson().toJson(product_model,new TypeToken<ArrayList<Product_deatail_Model>>(){}.getType()));
            editor.commit();
        }
        else {

            product_model = new Gson().fromJson(prefs.getString("cart",""),new TypeToken<ArrayList<Product_deatail_Model>>(){}.getType());
            product_model.add(model);
            editor.putString("cart",new Gson().toJson(product_model,new TypeToken<ArrayList<Product_deatail_Model>>(){}.getType()));
            editor.apply();

        }

    }
    public void setCartCount(int qty)
    {

       editor.putInt("cart_count",qty);
       editor.apply();

    }
    public  void  removeCart(String product_id){

        boolean contain;
        editor.putInt("cart_count",(getCartCount()-1));
        editor.apply();
        ArrayList<Cart_Details_Model>  models = new Gson().fromJson(prefs.getString("cart_items",""),new TypeToken<ArrayList<Cart_Details_Model>>(){}.getType());
        //ArrayList<Cart_Details_Model>  models1 = new Gson().fromJson(prefs.getString("cart_items",""),new TypeToken<ArrayList<Cart_Details_Model>>(){}.getType());
        for (Cart_Details_Model mode:models)
        {
            if(mode.getProduct_id().equals(product_id))
            {
                models.remove(mode);
                break;
            }
        }
        editor.putString("cart_items",new Gson().toJson(models,new TypeToken<ArrayList<Cart_Details_Model>>(){}.getType()));
        Log.d("cart_items",new Gson().toJson(models,new TypeToken<ArrayList<Cart_Details_Model>>(){}.getType()));
        editor.apply();

    }

    public  void  setCartLogin(int qty)
    {
        editor.putInt("cart_count",qty);
        editor.commit();
    }

    public  int  getCartCount()
    {

        return prefs.getInt("cart_count", 0);


    }

}
