package fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import com.adjit.surfcity.LoginOrReg;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.MyOrderDetail;
import com.adjit.surfcity.My_Order_activity;
import com.adjit.surfcity.R;
import com.google.android.gms.common.util.SharedPreferencesUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import util.Session_management;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    private NotificationManager notificationManager;
    private static final int NOTIFICATION_ID = 6578;
    private static final int NOTI_PRIMARY1 = 1100;
    private static final int NOTI_PRIMARY2 = 1101;
    private static final int NOTI_SECONDARY1 = 1200;
    private static final int NOTI_SECONDARY2 = 1201;
    private NotificationHelper notificationHelper;
    Bitmap bmp ;
    Session_management session_management ;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        session_management = new Session_management(getApplicationContext());
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            showNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getData().get("image"));
        } else if (remoteMessage != null) {
            showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("image"));
        }
    }
    private void showNotification(String title, String message, String image) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            notificationHelper = new NotificationHelper(this, notificationManager);
            sendNotification(NOTI_PRIMARY1, title, message, image);
            return;
        }
        Intent i;
        if (session_management.isLoggedIn()) {
            i = new Intent(this, MainActivity.class);
        } else {
            i = new Intent(this, LoginOrReg.class);
        }
        try {
            InputStream in = new URL(image).openStream();
            bmp = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Notification.Builder notification  ;
        if(image == null || image.equalsIgnoreCase("")){
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, i, PendingIntent.FLAG_UPDATE_CURRENT);
            /*.setStyle(new Notification.BigPictureStyle().bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.food)))*/
            notification = new Notification
                    .Builder(this)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .setShowWhen(true)
                    .setContentText(message)
                    .setContentTitle(title)
                    .setSmallIcon(getSmallIcon())
                    .setContentIntent(resultPendingIntent)
                    .setAutoCancel(true)
                    .setLights(Color.RED, 3000, 3000);
        }else {
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, i, PendingIntent.FLAG_UPDATE_CURRENT);
            /*.setStyle(new Notification.BigPictureStyle().bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.food)))*/
            notification = new Notification
                    .Builder(this)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .setShowWhen(true)
                    .setContentText(message)
                    .setContentTitle(title)
                    .setSmallIcon(getSmallIcon())
                    .setContentIntent(resultPendingIntent)
                    .setAutoCancel(true)
                    .setStyle(new Notification.BigPictureStyle().bigPicture(bmp))
                    .setLights(Color.RED, 3000, 3000);
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notification.setSmallIcon(R.drawable.logo);
            notification.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo));
            notification.setColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            notification.setSmallIcon(R.drawable.logo);
            notification.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo));
        }
        Notification notification1 = notification.build();
        notificationManager.notify(NOTIFICATION_ID, notification1);
    }

    private int getSmallIcon() {
        return R.drawable.logo;
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        SharePreferenceUtility.saveStringPreferences(getApplicationContext(), Const.FCM_TOKEN, s);
        String fcm = (String) SharePreferenceUtility.getPreferences(this, Const.FCM_TOKEN, SharePreferenceUtility.PREFTYPE_STRING);
        Log.d("fcm_token", fcm);
    }
    @RequiresApi(api = 26)
    public void sendNotification(int id, String title, String msg, String image) {
        Notification.Builder nb = null;
        switch (id) {
            case NOTI_PRIMARY1:
                nb = this.notificationHelper.getNotification1(title, msg, image);
                break;
            case NOTI_PRIMARY2:
                nb = this.notificationHelper.getNotification1(title, msg, image);
                break;
            case NOTI_SECONDARY1:
                nb = this.notificationHelper.getNotification2(title, msg, image);
                break;
            case NOTI_SECONDARY2:
                nb = this.notificationHelper.getNotification2(title, msg, image);
                break;
            default:
                break;
        }
        if (nb != null) {
            this.notificationHelper.notify(id, nb);
        }
    }
}
