package fcm;

import android.content.Context;

public class Const {
    public static final String FCM_TOKEN ="FCM_TOKEN";
    public static final String IS_LOGIN ="is_login";
    public static final String SUBSCRIPTION_ID ="SUB_ID";
    public static final String CLIENT_EMAIL = "client_email";
    public static final String DEL_ADDRESS = "del_address";
    public static final String MOB_NUM = "mob_num";
}
