package fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import com.adjit.surfcity.LoginOrReg;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.My_Order_activity;
import com.adjit.surfcity.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import util.Session_management;


public class NotificationHelper extends ContextWrapper {

    private NotificationManager manager;
    public static final String PRIMARY_CHANNEL = "default";
    public static final String SECONDARY_CHANNEL = "second";
    private static final int REQUEST_CODE = 1;
    private Session_management session_management;
    private Bitmap bmp;


    @RequiresApi(api = Build.VERSION_CODES.O)
    public NotificationHelper(Context ctx, NotificationManager notificationManager) {
        super(ctx);
        session_management = new Session_management(getApplicationContext());

        NotificationChannel chan1 = new NotificationChannel(PRIMARY_CHANNEL, getString(R.string.noti_channel_default), NotificationManager.IMPORTANCE_MAX);
        chan1.setLightColor(Color.GREEN);
        chan1.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(chan1);

        NotificationChannel chan2 = new NotificationChannel(SECONDARY_CHANNEL, getString(R.string.noti_channel_second), NotificationManager.IMPORTANCE_MAX);
        chan2.setLightColor(Color.BLUE);
        chan2.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        notificationManager.createNotificationChannel(chan2);
    }

    /**
     * Get a notification of type 1
     * <p>
     * Provide the builder rather than the notification it's self as useful for making notification
     * changes.
     *
     * @param title the title of the notification
     * @param body  the body text for the notification
     * @return the builder as it keeps a reference to the notification (since API 24)
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotification1(String title, String body, String image) {
        Intent i;
        if (session_management.isLoggedIn()) {
            i= new Intent(this,MainActivity.class);
        } else {
            i = new Intent(this, LoginOrReg.class);
        }
        try {
            InputStream in = new URL(image).openStream();
            bmp = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(image == null || image.equalsIgnoreCase("")) {
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);
            return new Notification.Builder(getApplicationContext(), SECONDARY_CHANNEL)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(getSmallIcon())
                    .setShowWhen(true)
                    .setStyle(new Notification.BigTextStyle().bigText(body))
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo));
        }else {
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);
            return new Notification.Builder(getApplicationContext(), SECONDARY_CHANNEL)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(getSmallIcon())
                    .setShowWhen(true)
                    .setStyle(new Notification.BigTextStyle().bigText(body))
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
                    .setStyle(new Notification.BigPictureStyle().bigPicture(bmp))
                    .setAutoCancel(true);
        }
    }

    /**
     * Build notification for secondary channel.
     *
     * @param title Title for notification.
     * @param body  Message for notification.
     * @return A Notification.Builder configured with the selected channel and details
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotification2(String title, String body, String image) {
        Intent i;
        if (session_management.isLoggedIn()) {
            i= new Intent(this,My_Order_activity.class);
        } else {
            i = new Intent(this, LoginOrReg.class);
        }
        try {
            InputStream in = new URL(image).openStream();
            bmp = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        i= new Intent(this,MainActivity.class);
        if(image == null || image.equalsIgnoreCase("")) {
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);
            return new Notification.Builder(getApplicationContext(), SECONDARY_CHANNEL)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(getSmallIcon())
                    .setShowWhen(true)
                    .setStyle(new Notification.BigTextStyle().bigText(body))
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo));
        }else {
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);
            return new Notification.Builder(getApplicationContext(), SECONDARY_CHANNEL)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(getSmallIcon())
                    .setShowWhen(true)
                    .setStyle(new Notification.BigTextStyle().bigText(body))
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
                    .setStyle(new Notification.BigPictureStyle().bigPicture(bmp))
                    .setAutoCancel(true);
        }
//        return null;
    }

    /**
     * Send a notification.
     *
     * @param id           The ID of the notification
     * @param notification The notification object
     */
    public void notify(int id, Notification.Builder notification) {
        getManager().notify(id, notification.build());
    }

    /**
     * Get the small icon for this app
     *
     * @return The small icon resource id
     */
    private int getSmallIcon() {
        return R.drawable.logo;
    }

    /**
     * Get the notification manager.
     * <p>
     * Utility method as this helper works with it a lot.
     *
     * @return The system service NotificationManager
     */
    private NotificationManager getManager() {
        if (manager == null) {
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }
}
