package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adjit.surfcity.LoginOrReg;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Config.BaseURL;
import Interfaces.Interfaces;
import Model.Subscription_model;
import fcm.SharePreferenceUtility;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.Session_management;

/**
 * Created by PC1 on 28-02-2019.
 */

public class Subscription_Adapter extends RecyclerView.Adapter<Subscription_Adapter.Subscription_Viewholder> {
    Activity activity;
    Context context;
    int flag;
    ArrayList<Subscription_model> models = new ArrayList<Subscription_model>();
    TextView duration, orig_price, offer_price,unlock_desc,delivery_desc,cashback_desc;
    Button btn_select;
    DatabaseHandler dbsubscription;
    Session_management sessionManagement;
    ImageView iv_suboffer_remove;
    Subscription_model priceSubscription_model;
    LinearLayout ll_unlock,ll_cashback,ll_free_del;
    String sub_id , sub_amt, txn_id, user_id, order_id ;
    Interfaces.onPurchase onPurchase;

    public Subscription_Adapter(Activity activity, ArrayList<Subscription_model> models, int flag, Interfaces.onPurchase onPurchase) {
        this.activity = activity;
        this.models = models;
        dbsubscription = new DatabaseHandler(activity);
        sessionManagement = new Session_management(activity);
        this.flag = flag;
        this.onPurchase = onPurchase ;
        user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        order_id = System.currentTimeMillis()+"_"+user_id;
    }

    public Subscription_Adapter(Activity activity, ArrayList<Subscription_model> models,
                                int flag, Subscription_model priceSubscription_model, Interfaces.onPurchase onPurchase) {
        this.activity = activity;
        this.models = models;
        dbsubscription = new DatabaseHandler(activity);
        sessionManagement = new Session_management(activity);
        this.flag = flag;
        this.priceSubscription_model = priceSubscription_model;
        this.onPurchase = onPurchase ;
        user_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        order_id = System.currentTimeMillis()+"_"+user_id;
    }

    @Override
    public Subscription_Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subscription, parent, false);
        Subscription_Viewholder holder = new Subscription_Viewholder(view);
        return holder;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final Subscription_Viewholder holder, final int position) {
        duration = holder.duration;
        orig_price = holder.original_price;
        offer_price = holder.offer_price;
        btn_select = holder.btn_select;
        delivery_desc=holder.tv_delivery_details;
        cashback_desc=holder.tv_cashback;
        unlock_desc=holder.tv_unlock_deatils;
        ll_cashback=holder.ll_cashback;
        ll_free_del=holder.ll_free_del;
        ll_unlock=holder.ll_unlock;

        duration.setText(models.get(position).getSubscription_name());
        orig_price.setText(models.get(position).getSubscription_name());
        offer_price.setText(activity.getResources().getString(R.string.currency) + models.get(position).getSubscription_price() + " / " + models.get(position).getSubscription_days() + " Days");

        String url = BaseURL.SUBSCRIPTION_IMAGE_URL + models.get(position).getSubscription_image() ;
/*        Glide.with(activity)
                .load(url)
                .centerCrop()
                .into(holder.imgv_sub);*/

        if(models.get(position).getSubscription_details1().equals("1")){
            ll_unlock.setVisibility(View.VISIBLE);
            unlock_desc.setText("Unlock subscription price");
        }else {
            ll_unlock.setVisibility(View.GONE);
        }

        if(models.get(position).getSubscription_details2().equals("1")){
            ll_free_del.setVisibility(View.VISIBLE);
            delivery_desc.setText("Free Delivery");
        }else {
            ll_free_del.setVisibility(View.GONE);
        }

        if(models.get(position).getSubscription_details3().equals("1")){
            ll_cashback.setVisibility(View.VISIBLE);
            cashback_desc.setText("Cashback");
        }else {
            ll_cashback.setVisibility(View.GONE);
        }

        Subscription_model sel_from_session_model = sessionManagement.getsubscription();

        final Subscription_model current_item = models.get(position);

        if (priceSubscription_model != null && current_item.getSubscription_id().equalsIgnoreCase(priceSubscription_model.getSubscription_id())) {
            holder.btn_select.setBackgroundTintList(ContextCompat.getColorStateList(activity, R.color.color_1));
        } else {
            try {
                if (priceSubscription_model != null) {
                    if (Integer.parseInt(models.get(position).getSubscription_price()) >= Integer.parseInt(priceSubscription_model.getSubscription_price())) {
                        holder.btn_select.setEnabled(true);
                        holder.btn_select.setBackgroundTintList(ContextCompat.getColorStateList(activity, R.color.light_orange));
                    } else {
                        holder.btn_select.setEnabled(false);
                        holder.btn_select.setBackgroundTintList(ContextCompat.getColorStateList(activity, R.color.dark_gray));
                    }
                } else {
                    holder.btn_select.setEnabled(true);
                    holder.btn_select.setBackgroundTintList(ContextCompat.getColorStateList(activity, R.color.light_orange));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        if (flag == 0) {
            holder.iv_suboffer_remove.setVisibility(View.GONE);
        }

        if (flag == 1) {
            holder.btn_select.setVisibility(View.GONE);
        }

        // select subscription btn click listener
        holder.btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        sessionManagement.update_subscription(models.get(position));
                        sub_id = BaseURL.sub_id = models.get(position).getSubscription_id();
                        sub_amt = models.get(position).getSubscription_price() ;

                        if (sessionManagement.isLoggedIn()) {
                            if (priceSubscription_model != null && current_item.getSubscription_id().equalsIgnoreCase(priceSubscription_model.getSubscription_id())){
                                Toast.makeText(activity, "Already Activated Plan.", Toast.LENGTH_SHORT).show();
                            }else {
                                if (NetworkConnection.connectionChecking(activity)){
                                    onPurchase.onClickPurchase(sub_id, sub_amt, "", order_id, user_id);
                                }else {
                                    Toast.makeText(activity, activity.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Intent intent = new Intent(activity, LoginOrReg.class);
                            activity.startActivity(intent);
                        }
                    }
                    notifyDataSetChanged();
                models.get(position).setSelected(true);
            }
        });

        holder.iv_suboffer_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.connectionChecking(context)){
                    if (sessionManagement.isLoggedIn()) {
                        remove_cart(sessionManagement.getUserDetails().get(BaseURL.KEY_ID), models.get(position).getSubscription_id(), position);
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public static class Subscription_Viewholder extends RecyclerView.ViewHolder {
        TextView duration, original_price, offer_price,tv_unlock_deatils,tv_delivery_details,tv_cashback;
        Button btn_select;
        ImageView iv_suboffer_remove;
        LinearLayout ll_unlock,ll_cashback,ll_free_del;

        public Subscription_Viewholder(View itemView) {
            super(itemView);
            duration = itemView.findViewById(R.id.duration);
            original_price = itemView.findViewById(R.id.offer_name);
            offer_price = itemView.findViewById(R.id.offer_price);
            btn_select = itemView.findViewById(R.id.btn_select);
            iv_suboffer_remove = itemView.findViewById(R.id.iv_suboffer_remove);
            tv_unlock_deatils=itemView.findViewById(R.id.tv_unlock_deatils);
            tv_delivery_details=itemView.findViewById(R.id.tv_free_delivery);
            tv_cashback=itemView.findViewById(R.id.tv_cashback);
            ll_cashback=itemView.findViewById(R.id.ll_cashback);
            ll_free_del=itemView.findViewById(R.id.ll_free_del);
            ll_unlock=itemView.findViewById(R.id.ll_subscription);
        }
    }

    private void remove_cart(String user_id, String sub_id, final int position) {
        String tag_json_obj = "json_item_details_req";

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", user_id);
        body.put("sub_id", sub_id);

        Log.d("body_cart", body.toString());
        String url = BaseURL.CART_REMOVE+"?user_id="+user_id+"&sub_id="+sub_id;
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("product_details", response.toString());
                try {
                    if (!response.getString("message").equals("")) {
                        Toast.makeText(activity, response.getString("message"), Toast.LENGTH_SHORT).show();
                        models.remove(position);
                        notifyDataSetChanged();
                        sessionManagement.update_subscription(null);
                        if (NetworkConnection.connectionChecking(activity)){
                            getCartcount(sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
                        }
                        updateintent();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        Volley.newRequestQueue(activity).add(jsonRequest);
    }

    private void add_to_cart_api(String user_id, String sub_id, String pro_id, String qty) {
        String url ;
        String tag_json_obj = "json_item_details_req";
        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", user_id);
        if (!sub_id.equals("0")) {
            body.put("sub_id", sub_id);
        }
        body.put("pro_id", pro_id);
        body.put("qty", qty);
        Log.d("body_cart", body.toString());
        if (!sub_id.equals("0")) {
            url = BaseURL.ADD_TO_CART+"?user_id="+user_id+"&sub_id="+sub_id+"&pro_id="+pro_id+"&qty="+qty+"&price=0";
        } else {
            url = BaseURL.ADD_TO_CART+"?user_id="+user_id+"&pro_id="+pro_id+"&qty="+qty+"&price=0";
        }
        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("product_details", response.toString());
                try {
                    if (response.getBoolean("responce")) {
                        if (NetworkConnection.connectionChecking(activity)){
                            getCartcount(sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
                        }
                        Toast.makeText(activity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsonRequest);
    }

    private void updateintent() {
        Intent updates = new Intent("Grocery_cart");
        updates.putExtra("type", "update");
        updates.putExtra("reload", "reload");
        activity.sendBroadcast(updates);
    }

    private void getCartcount(String user_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        String url = BaseURL.GET_CART_COUNT+"?user_id="+user_id;

        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo", response.toString());
                try {
                    if (response.getBoolean("response")) {

                        int item_count = response.getInt("data");
                        sessionManagement.setCartLogin(item_count);
                        ((MainActivity) activity).setCartCounter(String.valueOf(item_count));
                        updateintent();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsonRequest);
    }
}
