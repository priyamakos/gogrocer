package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import com.adjit.surfcity.R;

import util.Session_management;
import util.Utils;

/**
 * Created by Rajesh Dabhi on 4/7/2017.
 */

public class View_time_adapter extends RecyclerView.Adapter<View_time_adapter.MyViewHolder> {

    private List<String> modelList;
    private Context context;
    public int mSelectedItem = -1;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String getdate = "";
    private String gettime = "";
    private Session_management sessionManagement;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
       public RadioButton rb_selecttime;

        public MyViewHolder(View view) {
            super(view);
            title =  view.findViewById(R.id.tv_socity_name);
            rb_selecttime = view.findViewById(R.id.rb_selecttime);

            rb_selecttime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    //get default time
                    getdate = "" + mYear + "-" + (mMonth + 1) + "-" + mDay;
                    String start = modelList.get(getAdapterPosition()).trim().split("-")[0] ;
                    String end = modelList.get(getAdapterPosition()).trim().split("-")[1] ;
                    gettime =  Utils.convert24To12(start.trim()) + " - " + Utils.convert24To12(end.trim()) ;
                            //modelList.get(getAdapterPosition());

                    sessionManagement = new Session_management(context);
                    sessionManagement.cleardatetime();

                    sessionManagement.creatdatetime(getdate, gettime);
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });

            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    //get default time
                    getdate = "" + mYear + "-" + (mMonth + 1) + "-" + mDay;
                    String start = modelList.get(getAdapterPosition()).trim().split("-")[0] ;
                    String end = modelList.get(getAdapterPosition()).trim().split("-")[1] ;
                    gettime =  Utils.convert24To12(start.trim()) + " - " + Utils.convert24To12(end.trim()) ;
                    //modelList.get(getAdapterPosition());

                    sessionManagement = new Session_management(context);
                    sessionManagement.cleardatetime();

                    sessionManagement.creatdatetime(getdate, gettime);
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }

    public View_time_adapter(List<String> modelList) {
        this.modelList = modelList;
    }

    @Override
    public View_time_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_socity_rv, parent, false);

        context = parent.getContext();

        return new View_time_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final View_time_adapter.MyViewHolder holder, final int position) {
        //Socity_model mList = modelList.get(position);
        String start = modelList.get(position).trim().split("-")[0] ;
        String end = modelList.get(position).trim().split("-")[1] ;
        try {
            holder.title.setText(Utils.convert24To12(start.trim()) + " - " + Utils.convert24To12(end.trim()));
        }catch (Exception e){
            holder.title.setText(start.trim() + " - " + end.trim());
        }
        holder.rb_selecttime.setChecked(position == mSelectedItem);
    }


    @Override
    public int getItemCount() {
        return modelList.size();
    }

}
