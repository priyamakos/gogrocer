package Adapter;



import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.surfcity.CouponActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Config.BaseURL;
import Model.Coupon_list_model;
import util.Session_management;
import Fragment.Cart_fragment;

public class Coupon_adapter extends RecyclerView.Adapter<Coupon_adapter.CouponViewholder>{
    Context context;
    ArrayList<Coupon_list_model> model=new ArrayList<Coupon_list_model>();
    private Session_management sessionManagement;
    String userId,coupon_id,cpn_code, wallet_percent_amt ;
    public Coupon_adapter(Context context, ArrayList<Coupon_list_model> model, String wallet_percent_amt) {
        this.context=context;
        this.model=model;
        this.wallet_percent_amt = wallet_percent_amt ;
        sessionManagement = new Session_management(context);
        userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
    }

    @Override
    public CouponViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_coupon_list, parent, false);
        CouponViewholder holder = new CouponViewholder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CouponViewholder holder, int position) {
     String free_del = "", cashback = "" ;
        holder.tv_coupon_name.setText(model.get(position).getCoupon_name());
        holder.tv_coupon_price.setText(model.get(position).getDetails());
        if(model.get(position).getFree_delivery().equals("0"))
            free_del = "" ;
        else
            free_del = "+ Free Delivery";

        if(!model.get(position).getCashback().equals("") && !model.get(position).getCashback().equals("0"))
            cashback = " + Cashback ₹ " +  model.get(position).getCashback();
        else
            cashback = "" ;

/*        if(!model.get(position).getDiscount_value().equals("0")){
            if(model.get(position).getDiscount_type().equals("1"))
                holder.tv_coupon_price.setText("Discount "+model.get(position).getDiscount_value() +"  % "+ free_del + " " + cashback);
            else
                holder.tv_coupon_price.setText("Discount ₹ "+model.get(position).getDiscount_value() + free_del + " " + cashback);
        } else{
            if(!free_del.equals("") && !cashback.equals("")) {
                holder.tv_coupon_price.setText(free_del.replace("+","") + " " + cashback);
            }else {
                if(!free_del.equals("")){
                    holder.tv_coupon_price.setText(free_del.replace("+",""));
                }else {
                    holder.tv_coupon_price.setText(cashback.replace("+",""));
                }
            }
        }*/

        /*if(!model.get(position).getFree_delivery().equals("0"))
            holder.tv_coupon_price.setText("Free Delivery");*/

        holder.tv_coupon_name.setText(model.get(position).getCoupon_name());
        final String coupon_code=model.get(position).getCoupon_id();
        holder.btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("coupon_id", coupon_code);
                ((CouponActivity)context).setResult(Activity.RESULT_OK, resultIntent);
                ((CouponActivity)context).finish();
                if(NetworkConnection.connectionChecking(context)) {
                    //verifyCoupan(coupon_code);
                }
            }
        });
   }

    private void verifyCoupan(String coupon_code) {
        HashMap<String, String> wallet_body = new HashMap<>();
        wallet_body.put("coupon_code", coupon_code);
        wallet_body.put("user_id",userId);
        String url = BaseURL.VERFY_COUPAN_URL+"?coupon_code="+coupon_code+"&user_id="+userId;
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                try {
                    boolean status = response.getBoolean("responce");
                    if (status) {
                        JSONArray jsonArray = response.getJSONArray("data");
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        coupon_id = jsonObject.getString("id");
                        cpn_code=jsonObject.getString("coupon_code");

                        String message = response.getString("message");
                        Toast.makeText(context,  "Promo code applied successfully", Toast.LENGTH_SHORT).show();

                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("coupon_id", coupon_id);
                        ((CouponActivity)context).setResult(Activity.RESULT_OK, resultIntent);
                        ((CouponActivity)context).finish();
                    } else {
                        Toast.makeText(context, "Coupon not applied", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("wallet_update_response", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure

            }
        });
        Volley.newRequestQueue(context).add(jsonRequest);
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public static class CouponViewholder extends RecyclerView.ViewHolder {
        TextView tv_coupon_name,tv_coupon_price, btn_apply;
        public CouponViewholder(View itemView) {
            super(itemView);
            tv_coupon_name=itemView.findViewById(R.id.tv_coupon_name);
            tv_coupon_price=itemView.findViewById(R.id.tv_coupon_price);
            btn_apply=itemView.findViewById(R.id.btn_apply);

        }
    }



}
