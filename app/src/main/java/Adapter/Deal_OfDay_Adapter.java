package Adapter;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import Fragment.ProductDetails_Fragment;
import com.adjit.surfcity.Club_membership;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.security.PublicKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Config.BaseURL;
import Model.Deal_Of_Day_model;
import de.hdodenhof.circleimageview.CircleImageView;

import com.adjit.surfcity.R;

public class Deal_OfDay_Adapter extends RecyclerView.Adapter<Deal_OfDay_Adapter.MyViewHolder> {

    private List<Deal_Of_Day_model> modelList;
    private Context context;
    public int counter;
    CountDownTimer countDownTimer;
    public TextView hour1,hour2,minute1,minute2,sec1,sec2;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        public TextView product_nmae, product_prize, offer_product_prize, start_time, end_time,offer_textview,tv_unit,prices;
        public CardView cv_deal_day ;
        //public TextView hour1,hour2,minute1,minute2,sec1,sec2;

        public CircleImageView image ;
        public ImageView iv_icon;
        RelativeLayout subscription_link;

        public MyViewHolder(View view) {
            super(view);
            cv_deal_day = view.findViewById(R.id.cv_deal_day);
            product_nmae = (TextView) view.findViewById(R.id.product_name);
            product_prize = (TextView) view.findViewById(R.id.product_prize);
            offer_product_prize = (TextView) view.findViewById(R.id.offer_product_prize);

            //start_time = (TextView) view.findViewById(R.id.start_time);
            end_time = (TextView) view.findViewById(R.id.end_time);

            offer_textview = (TextView) view.findViewById(R.id.ofer_textview);
            tv_unit  = (TextView) view.findViewById(R.id.tv_unit);
            //prices = view.findViewById(R.id.price);
            //iv_icon= view.findViewById(R.id.iv_icon);
            //subscription_link=view.findViewById(R.id.subscription_link);
            //subscription_link.setOnClickListener(this);
            hour1=view.findViewById(R.id.tv_h1);
            hour2=view.findViewById(R.id.tv_h2);
            minute1=view.findViewById(R.id.tv_m1);
            minute2=view.findViewById(R.id.tv_m2);
            sec1=view.findViewById(R.id.tv_sec1);
            sec2=view.findViewById(R.id.tv_sec2);

            product_prize.setPaintFlags(product_prize.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            image = view.findViewById(R.id.iv_icon);
            image.setOnClickListener(this);
            cv_deal_day.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            int position = getAdapterPosition()-1;
            if (id == R.id.subscription_link) {
                Intent intent = new Intent(context, Club_membership.class);
                context.startActivity(intent);
            }else if (id == R.id.cv_deal_day) {
                showProductDetail(position);
            }
         }
        }

    public Deal_OfDay_Adapter(List<Deal_Of_Day_model> modelList, Activity activity) {
        this.modelList = modelList;
    }

    @Override
    public Deal_OfDay_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_deal_of_the_day, parent, false);
        context = parent.getContext();
        return new Deal_OfDay_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Deal_OfDay_Adapter.MyViewHolder holder, int position) {
        position = position - 1 ;
        if(position >= 0){
            holder.cv_deal_day.setVisibility(View.VISIBLE);
            Deal_Of_Day_model mList = modelList.get(position);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String start_time = mList.getStart_time();
            String start_date = mList.getStart_date();
            String end_time = mList.getEnd_time();
            String end_date = mList.getEnd_date();

            String start_dt = start_date+" "+start_time;
            String end_dt = end_date+" "+end_time;
            //yyyy-MM-dd   hh:mm
            Log.e("datetime",start_dt+"  "+end_dt);

            Date startDate = null;
            Date endDate = null;
            try {
                startDate = dateFormat.parse(start_dt);
                endDate = dateFormat.parse(end_dt);
                Log.e("enddate", String.valueOf(endDate));
            }catch (ParseException e) {
                e.printStackTrace();
            }
            //long duration = endDate.getTime()-startDate.getTime();
            long duration = endDate.getTime()- Calendar.getInstance().getTimeInMillis();
            long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
            long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
            long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
            String counterUp = String.valueOf(diffInMinutes);
            Log.d("counterUp",counterUp);

            if (counterUp.equals("")) {
                Log.d("dod_timer","Not start and end datetime");
            } else {
                // countdown = counterUp;
//                setcountdown(counterUp);
            }

            if (mList.getStatus().equals("1")) {
                holder.offer_product_prize.setText(context.getResources().getString(R.string.currency) + mList.getDeal_price());
                holder.offer_product_prize.setTextColor(context.getResources().getColor(R.color.green_light));
                //holder.prices.setText(mList.getPrice());
                holder.offer_textview.setTextColor(context.getResources().getColor(R.color.white));
                holder.tv_unit.setText( mList.getUnit_value() + " " + mList.getUnit() );
                //holder.end_time.setText(mList.getEnd_time());
                //holder.end_time.setTextColor(context.getResources().getColor(R.color.green));
            } else if (mList.getStatus().equals("2")) {
                holder.offer_product_prize.setText(context.getResources().getString(R.string.currency) + mList.getDeal_price());
                //holder.prices.setText(mList.getPrice());
                holder.offer_product_prize.setTextColor(context.getResources().getColor(R.color.green_light));
                holder.offer_textview.setTextColor(context.getResources().getColor(R.color.white));
                holder.tv_unit.setText( mList.getUnit_value() + " " + mList.getUnit() );
                //holder.end_time.setText("Expired");
                //holder.end_time.setTextColor(context.getResources().getColor(R.color.color_3));
            }else if(mList.getStatus().equals("0")){
                holder.offer_product_prize.setText(context.getResources().getString(R.string.currency) + mList.getDeal_price());
                holder.offer_product_prize.setTextColor(context.getResources().getColor(R.color.green_light));
                holder.offer_textview.setTextColor(context.getResources().getColor(R.color.white));
                holder.tv_unit.setText( mList.getUnit_value() + " " + mList.getUnit() );
            }
            if (NetworkConnection.connectionChecking(context)){
                Glide.with(context)
                        .load(BaseURL.IMG_PRODUCT_URL + mList.getProduct_image())
                        .placeholder(R.drawable.icon)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .into(holder.image);
            }

            holder.product_prize.setText(context.getResources().getString(R.string.currency) + mList.getPrice());
            holder.product_nmae.setText(mList.getProduct_name());
        }else if (position < 0){
            holder.cv_deal_day.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size()+1;
    }

    private void showProductDetail(final int position) {
        Log.d("deals_product_id",modelList.get(position).getProduct_id());
        Bundle args = new Bundle();
        Fragment fm = new ProductDetails_Fragment();
        args.putString("product_id", modelList.get(position).getProduct_id());
        args.putString("max_qty", modelList.get(position).getMax_qty());
        fm.setArguments(args);
        FragmentManager fragmentManager =  ((AppCompatActivity)context).getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
    }

    private void setcountdown(String countdown) {
       // deliverytime.setText(countdown);

        if (countDownTimer == null) {
            //String getMinutes = minutes.getText().toString();//Get minutes from edittexf
            //Check validation over edittext
            if (!countdown.equals("") && countdown.length() > 0) {
                //int noOfMinutes = Integer.parseInt(countdown) * 60 * 1000;//Convert minutes into milliseconds
                long noOfMinutes =Long.parseLong(countdown) * 60 * 1000;//Convert minutes into milliseconds
                startTimer(noOfMinutes);//start countdown
                //startTimer.setText(getString(R.string.stop_timer));//Change Text

            } else {
            }
        } else {
            //Else stop timer and change text
            stopCountdown();
            //startTimer.setText(getString(R.string.start_timer));
           // hour1.setText("00:00:00");
            hour1.setText("0");
            hour2.setText("0");
            minute1.setText("0");
            minute2.setText("0");
            sec1.setText("0");
            sec2.setText("0");
        }
    }

    //Start Countodwn method
    private void startTimer(long noOfMinutes) {
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds

                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                //set text
                String h1=hms.substring(0,1);
                String h2=hms.substring(1,2);
                String m1=hms.substring(3,4);
                String m2=hms.substring(4,5);
                String s1=hms.substring(6,7);
                String s2=hms.substring(7,8);
                hour1.setText(h1);
                hour2.setText(h2);
                minute1.setText(m1);
                minute2.setText(m2);
                sec1.setText(s1);
                sec2.setText(s2);

                Log.d("countDown",hms);
            }

            public void onFinish()
            {
                //deliverytime.setText("TIME'S UP!!"); //On finish change timer text
               // hour1.setText("00:00:00");
              
                hour1.setText("0");
                hour2.setText("0");
                minute1.setText("0");
                minute2.setText("0");
                sec1.setText("0");
                sec2.setText("0");
                countDownTimer = null;//set CountDownTimer to null
                //startTimer.setText(getString(R.string.start_timer));//Change button text
            }
        }.start();
    }

    //Stop Countdown method
    private void stopCountdown()
    {
        if (countDownTimer != null)
        {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }
   /* public class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = (TimeUnit.MILLISECONDS.toDays(millis)) + "Day "
                    + (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)) + ":")
                    + (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)) + ":"
                    + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
            Toast.makeText(context, hms, Toast.LENGTH_SHORT).show();

           *//* long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
            millisUntilFinished -= TimeUnit.DAYS.toMillis(days);

            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
            millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
            millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

            Toast.makeText(context, days + ":" + hours + ":" + minutes + ":" + seconds, Toast.LENGTH_SHORT).show();*//*
        }

        @Override
        public void onFinish() {

        }
    }*/
}

