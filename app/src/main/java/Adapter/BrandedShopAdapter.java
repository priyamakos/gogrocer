package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import Config.BaseURL;
import Model.ShopNow_model;
import Model.Shop_List_Model;

/**
 * Created by Rajesh Dabhi on 22/6/2017.
 */

public class BrandedShopAdapter extends RecyclerView.Adapter<BrandedShopAdapter.MyViewHolder> {

    private List<Shop_List_Model> modelList;
    private Context context;
    int count;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_home_title);
            image = (ImageView) view.findViewById(R.id.iv_home_img);
        }
    }

    public BrandedShopAdapter(List<Shop_List_Model> modelList) {
        this.modelList = modelList;
    }

    @Override
    public BrandedShopAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_shop_now, parent, false);
        context = parent.getContext();
        return new BrandedShopAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BrandedShopAdapter.MyViewHolder holder, int position) {
        Shop_List_Model mList = modelList.get(position);
        if(NetworkConnection.connectionChecking(context)){
            Glide.with(context)
                    .load(BaseURL.IMG_SHOP_URL + mList.getLogo())
                    .placeholder(R.drawable.icon)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(holder.image);
        }
        holder.title.setText(mList.getShop_name());
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }
}

