package Adapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.adjit.surfcity.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Model.Top_Slider_Model;

/**
 * Created by PC1 on 27-02-2019.
 */

public class Top_SliderAdapter extends RecyclerView.Adapter<Top_SliderAdapter.Top_Viewholder>
{
    Context activity;
    ArrayList<Top_Slider_Model> models = new ArrayList<>();

    public Top_SliderAdapter(Context activity, ArrayList<Top_Slider_Model> models)
    {
        this.activity = activity;
        this.models = models;
    }

    @Override
    public Top_Viewholder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_img,parent,false);
        Top_Viewholder holder = new Top_Viewholder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Top_Viewholder holder, int position)
    {
        ImageView img = holder.img;

        if (models.get(position).getSlider_url().equals(""))
        {
            Picasso.with(activity).load(R.drawable.icon).fit().into(img);
        }else
        {
            Picasso.with(activity).load(models.get(position).getSlider_url()).fit().into(img);
        }

        Log.e("slider_name",models.get(position).getSlider_title());
        //img.setImageURI(Uri.parse(models.get(position).getSlider_url()));

    }

    @Override
    public int getItemCount()
    {
        return models.size();
    }

    public static class Top_Viewholder extends RecyclerView.ViewHolder
    {
        ImageView img;
        public Top_Viewholder(View itemView)
        {
            super(itemView);
            img = itemView.findViewById(R.id.slider_img);

        }
    }
}
