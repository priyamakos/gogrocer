package Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adjit.surfcity.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.WalletHistroy;

public class WalletHistroyAdapter extends RecyclerView.Adapter<WalletHistroyAdapter.WalletViewHolder>{

    ArrayList<WalletHistroy> walletHistroyArrayList;
    Context mContext;

    public WalletHistroyAdapter(ArrayList<WalletHistroy> walletHistroyArrayList,Context mContext) {
        this.walletHistroyArrayList = walletHistroyArrayList;
        mContext = mContext;
    }

    @Override
    public WalletViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_wallet_histroy,parent,false);
        WalletViewHolder  walletViewHolder = new WalletViewHolder(view);
        return walletViewHolder;
    }

    @Override
    public void onBindViewHolder(WalletViewHolder holder, int position) {
        TextView txt_amount = holder.txt_amount;
        TextView txt_status = holder.txt_status;
        TextView txt_date = holder.txt_date;
        DecimalFormat twoDForm = new DecimalFormat("##.##");
        String status_str = walletHistroyArrayList.get(position).getStatus();
        Double amt = Double.valueOf(walletHistroyArrayList.get(position).getAmount());
        if(status_str.equals("1")) {
            holder.cashback_amount_desc.setText("You've won");
            txt_amount.setText("+ ₹ "+twoDForm.format(amt));
        }else if(status_str.equals("0")){
            holder.cashback_amount_desc.setText("You've used");
            txt_amount.setText("- ₹ "+twoDForm.format(amt));

        }

//        txt_amount.setText(walletHistroyArrayList.get(position).getAmount());
        txt_date.setText(walletHistroyArrayList.get(position).getTrancdate());

    }

    @Override
    public int getItemCount() {
        return walletHistroyArrayList.size();
    }

    public static class WalletViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_amount,txt_status,txt_date, cashback_amount_desc;
        public WalletViewHolder(View itemView) {
            super(itemView);
            txt_amount = itemView.findViewById(R.id.trans_amount);
//            txt_status = itemView.findViewById(R.id.trans_type);
            txt_date = itemView.findViewById(R.id.tracsdate);
            cashback_amount_desc = itemView.findViewById(R.id.cashback_amount_desc);
        }
    }
}
