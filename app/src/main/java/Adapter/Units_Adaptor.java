package Adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adjit.surfcity.R;

import java.util.ArrayList;

import Model.Unit_Model;

public class Units_Adaptor extends RecyclerView.Adapter<Units_Adaptor.MyunitViewHolder> {
    ArrayList<Unit_Model> units;
   Context context;
    public Units_Adaptor(ArrayList<Unit_Model> units, Context context)
    {
        this.units = units;
        this.context = context;
    }

    @Override
    public MyunitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyunitViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_product_unit,parent,false));
    }

    @Override
    public void onBindViewHolder(MyunitViewHolder holder, int position) {
        holder.tv_unit.setText(units.get(position).getName());

        if(units.get(position).isSelect()) {
            holder.layout.setBackgroundResource(R.drawable.custom_unit_select_background);
        } else {
            holder.layout.setBackgroundResource(R.drawable.custom_unit_background);
            holder.tv_unit.setTextColor(context.getResources().getColor(R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return units.size();
    }

    public  class  MyunitViewHolder extends RecyclerView.ViewHolder{
        TextView tv_unit;
        ConstraintLayout layout;
        public MyunitViewHolder(View itemView) {
            super(itemView);
            tv_unit = itemView.findViewById(R.id.tv_unit);
            layout = itemView.findViewById(R.id.main_view);

        }
    }
}
