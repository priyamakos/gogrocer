package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Model.Socity_model;
import com.adjit.surfcity.R;

/**
 * Created by Rajesh Dabhi on 29/6/2017.
 */

public class Socity_adapter extends RecyclerView.Adapter<Socity_adapter.MyViewHolder>
        implements Filterable {

    public List<Socity_model> modelList, mFilteredList, searchable;
//    private MyFilter filter;

    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_socity_name);
        }
    }

   /* @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new MyFilter(this, mFilteredList);
        }
        return filter;
    }*/

    public Socity_adapter(List<Socity_model> modelList) {
        this.modelList = modelList;
        this.mFilteredList = modelList;
        this.searchable = modelList ;
    }

    @Override
    public Socity_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_socity_rv, parent, false);
        context = parent.getContext();
        return new Socity_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Socity_adapter.MyViewHolder holder, int position) {
        Socity_model mList = modelList.get(position);
        holder.title.setText(mList.getPincode());
    }

    public List<Socity_model> getCurrentList(){
        return modelList ;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    @Override
    public Filter getFilter(){
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().trim().toLowerCase();
                if (charString.length() == 0) {
                    mFilteredList = searchable;
                } else {
                    List<Socity_model> filteredList = new ArrayList<>();
                    for (Socity_model androidVersion : searchable) {
                        if (androidVersion.getPincode().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                modelList = (List<Socity_model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
