package Adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import Config.BaseURL;
import Model.Home_Icon_model;
import Model.Shop_List_Model;

public class Shop_List_Adapter extends RecyclerView.Adapter<Shop_List_Adapter.MyViewHolder> {

    private List<Shop_List_Model> modelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title ;
        public ImageView image ;
//        public LinearLayout ll_parent ;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.service_text);
            image = (ImageView) view.findViewById(R.id.service_image);
//            ll_parent = (LinearLayout) view.findViewById(R.id.ll_parent);
        }
    }

    public Shop_List_Adapter(List<Shop_List_Model> modelList) {
        this.modelList = modelList;
    }

    @Override
    public Shop_List_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_shop_list, parent, false);
        context = parent.getContext();
        return new Shop_List_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Shop_List_Adapter.MyViewHolder holder, int position) {
        Shop_List_Model mList = modelList.get(position);
        if(NetworkConnection.connectionChecking(context)){
            Glide.with(context)
                    .load(BaseURL.IMG_SHOP_URL + mList.getLogo())
                    .placeholder(R.drawable.logo)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(holder.image);
        }

        holder.title.setText(mList.getShop_name());
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

}

