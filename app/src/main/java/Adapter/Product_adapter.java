package Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adjit.surfcity.LoginOrReg;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import Config.BaseURL;
import Fragment.ProductDetails_Fragment;
import Model.Product_model;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.DatabaseHandler;
import util.Session_management;

/**
 * Created by Rajesh Dabhi on 26/6/2017.
 */
public class Product_adapter extends RecyclerView.Adapter<Product_adapter.MyViewHolder> {

    private List<Product_model> modelList;
    private Context context;
    private DatabaseHandler dbcart;
    HashMap<String, String> map;
    private Session_management sessionManagement;
    boolean IS_DEAL_OF_THE_DAY = false;
    Boolean isSingleItem = false ;
    String userId ;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog; public int qty;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tv_title, tv_price, tv_reward, tv_total, tv_contetiy, tv_add, tv_sufcity_price, tv__old_price, tv_subscription_price, tv_customized;
        public ImageView iv_logo, iv_plus, iv_minus, iv_remove, lockimg_prod_subplan;
        RelativeLayout ll_product_layout, rlSubscription;
        public Double reward;
        RelativeLayout relativelayout_add;
        LinearLayout linearlayout;

        public MyViewHolder(View view) {
            super(view);
            relativelayout_add = view.findViewById(R.id.relativelayout_add);
            linearlayout = view.findViewById(R.id.llAddQuantity);
            tv_title = (TextView) view.findViewById(R.id.tv_subcat_title);
            tv_price = (TextView) view.findViewById(R.id.tv_subcat_price);
            tv__old_price = view.findViewById(R.id.tv_price_old);
            tv_subscription_price = view.findViewById(R.id.tv_subscription_price);
            // tv_reward = (TextView) view.findViewById(R.id.tv_reward_point);
            tv_total = (TextView) view.findViewById(R.id.tv_subcat_total);
            tv_contetiy = (TextView) view.findViewById(R.id.tv_subcat_contetiy);
            rlSubscription = view.findViewById(R.id.rlSubscription);
            // tv_add = (TextView) view.findViewById(R.id.tv_subcat_add);
            iv_logo = (ImageView) view.findViewById(R.id.iv_subcat_img);
            iv_plus = (ImageView) view.findViewById(R.id.iv_subcat_plus);
            iv_minus = (ImageView) view.findViewById(R.id.iv_subcat_minus);
            iv_remove = (ImageView) view.findViewById(R.id.iv_subcat_remove);
            ll_product_layout = view.findViewById(R.id.ll_product_layout);
            tv_sufcity_price = view.findViewById(R.id.tv_sufcity_price);
            lockimg_prod_subplan = view.findViewById(R.id.lockimg_prod_subplan);
            iv_remove.setVisibility(View.GONE);
            iv_minus.setOnClickListener(this);
            iv_plus.setOnClickListener(this);
            // tv_add.setOnClickListener(this);
            iv_logo.setOnClickListener(this);
            ll_product_layout.setOnClickListener(this);
            tv_customized = view.findViewById(R.id.tv_customized);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            int position = getAdapterPosition();
            if (id == R.id.iv_subcat_plus) {
                int qty = Integer.valueOf(tv_contetiy.getText().toString());
                qty = qty + 1;
                tv_contetiy.setText(String.valueOf(qty));
            } else if (id == R.id.iv_subcat_minus) {

                int qty = 0;
                if (!tv_contetiy.getText().toString().equalsIgnoreCase(""))
                    qty = Integer.valueOf(tv_contetiy.getText().toString());

                if (qty > 0) {
                    qty = qty - 1;
//                    tv_contetiy.setText(String.valueOf(qty));
                }
                if (qty < 1) {
                    linearlayout.setVisibility(View.GONE);
                    relativelayout_add.setVisibility(View.VISIBLE);
                }

            } else if (id == R.id.tv_subcat_add) {

            } else if (id == R.id.iv_subcat_img) {

            } else if (id == R.id.ll_product_layout) {
                map = new HashMap<>();
                map.put("product_id", modelList.get(position).getProduct_id());
                map.put("product_name", modelList.get(position).getProduct_name());
                map.put("category_id", modelList.get(position).getCategory_id());
                map.put("product_description", modelList.get(position).getProduct_description());
                map.put("deal_price", modelList.get(position).getDeal_price());
                map.put("start_date", modelList.get(position).getStart_date());
                map.put("start_time", modelList.get(position).getStart_time());
                map.put("end_date", modelList.get(position).getEnd_date());
                map.put("end_time", modelList.get(position).getEnd_time());
                map.put("price", modelList.get(position).getPrice());
                map.put("product_image", modelList.get(position).getProduct_image());
                map.put("status", modelList.get(position).getStatus());
                map.put("in_stock", modelList.get(position).getIn_stock());
                map.put("unit_value", modelList.get(position).getUnit_value());
                map.put("unit", modelList.get(position).getUnit());
                map.put("increament", modelList.get(position).getIncreament());
                map.put("rewards", modelList.get(position).getRewards());
                map.put("stock", modelList.get(position).getStock());
                map.put("title", modelList.get(position).getTitle());

                showProductDetail(position);
            }
        }
    }

    public Product_adapter(List<Product_model> modelList, Context context) {
        this.modelList = modelList;
        dbcart = new DatabaseHandler(context);
    }

    public Product_adapter(List<Product_model> modelList, Context context, boolean IS_DEAL_OF_THE_DAY) {
        this.modelList = modelList;
        dbcart = new DatabaseHandler(context);
        this.IS_DEAL_OF_THE_DAY = IS_DEAL_OF_THE_DAY;
    }

    @Override
    public Product_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_rv, parent, false);
        context = parent.getContext();
        sessionManagement = new Session_management(context);
        mDialogBuilder = new AlertDialog.Builder(context);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
        return new Product_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Product_adapter.MyViewHolder holder, final int position) {
        final Product_model mList = modelList.get(position);
        try{
            isSingleItem = mList.getSingleItem() ;
        }catch (Exception e){
            isSingleItem = false ;
        }
        if(isSingleItem){
            holder.tv_customized.setVisibility(View.GONE);
        }else {
            holder.tv_customized.setVisibility(View.VISIBLE);
        }
        if (NetworkConnection.connectionChecking(context)){
            Glide.with(context)
                    .load(BaseURL.IMG_PRODUCT_URL + mList.getProduct_image())
                    .centerCrop()
                    .placeholder(R.drawable.icon)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .fitCenter()
                    .into(holder.iv_logo);
        }

        holder.tv_title.setText(mList.getProduct_name());
        //holder.tv_reward.setText(mList.getRewards());
        holder.tv_price.setText(mList.getUnit_value() + " " + mList.getUnit());

        // holder.tv_sufcity_price.setText(mList.getDeal_price());
        holder.tv__old_price.setPaintFlags(holder.tv__old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tv__old_price.setText(context.getResources().getString(R.string.currency) + " " + mList.getPrice());
        holder.tv_subscription_price.setText(mList.getSubscription_price());
        /*if (dbcart.isInCart(mList.getProduct_id())) {
            holder.tv_add.setText(context.getResources().getString(R.string.tv_pro_update));
            holder.tv_contetiy.setText(dbcart.getCartItemQty(mList.getProduct_id()));
        } else {
            holder.tv_add.setText(context.getResources().getString(R.string.tv_pro_add));
        }
        Double items = Double.parseDouble(dbcart.getInCartItemQty(mList.getProduct_id()));
        Double price = Double.parseDouble(mList.getPrice());
        Double reward;
        if(mList.getRewards().equals("")){
            reward=0.0;
        }else {
            reward = Double.parseDouble(mList.getRewards());
        }
        holder.tv_total.setText("" + price * items);
        holder.tv_reward.setText("" + reward * items);*/

        if (IS_DEAL_OF_THE_DAY) {
            holder.rlSubscription.setVisibility(View.GONE);
            holder.tv_sufcity_price.setText(mList.getDeal_price());
        }else {
            holder.rlSubscription.setVisibility(View.VISIBLE);
            holder.tv_sufcity_price.setText(mList.getSurfcity_price());
        }
        BaseURL.sub_id = (String) SharePreferenceUtility.getPreferences(context, Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
        if (BaseURL.sub_id != null && !BaseURL.sub_id.equalsIgnoreCase("")) {
//            BaseURL.sub_id = sessionManagement.getsubscription().getSubscription_id();
            holder.lockimg_prod_subplan.setImageResource(R.drawable.ic_lock_open_black_24dp);
        } else {
            BaseURL.sub_id = "0";
            holder.lockimg_prod_subplan.setImageResource(R.drawable.ic_lock_outline_black_24dp);
        }

        holder.iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.connectionChecking(context)){
                    int Login_AT_TO_CART = 3;
                    qty = Integer.valueOf(holder.tv_contetiy.getText().toString());
                    qty = qty + 1;
                    if (sessionManagement.isLoggedIn()) {
                    if (isSingleItem){
                        /*if(Integer.parseInt(mList.getIn_stock()) >= qty){*/
//                        holder.tv_contetiy.setText(String.valueOf(qty));
                           /* if (!holder.tv_contetiy.getText().toString().equalsIgnoreCase("0")) */{
                                if (!BaseURL.sub_id.equals("0")) {
                                    Log.e("qty", String.valueOf(qty));
                                    add_to_cart_api(userId, BaseURL.sub_id, mList.getProduct_id(), "1",
                                            mList.getUnit(), mList.getUnit_value(), holder.tv_contetiy, position);
                                } else {
                                    add_to_cart_api(userId, "0", mList.getProduct_id(), "1",
                                            mList.getUnit(), mList.getUnit_value(), holder.tv_contetiy, position);
                                }
                            }
                    }else {
                        showDialodg(context, mList, holder, position);
                    }
                    } else {
                        Intent intent = new Intent(context, LoginOrReg.class);
                        intent.putExtra("flag", Login_AT_TO_CART);
                        context.startActivity(intent);
                    }
                  /*  }else {
                        Toast.makeText(context, context.getResources().getString(R.string.product_out_of_stock), Toast.LENGTH_SHORT).show();
                    }*/

                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.relativelayout_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.connectionChecking(context)){
                    int Login_AT_TO_CART = 3;
                    qty = Integer.valueOf(holder.tv_contetiy.getText().toString());
                    qty = qty + 1;
                    if (sessionManagement.isLoggedIn()) {
                        if (isSingleItem) {
                            holder.linearlayout.setVisibility(View.VISIBLE);
                            holder.relativelayout_add.setVisibility(View.GONE);
//                            holder.tv_contetiy.setText(String.valueOf(qty));
                            String userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
                            /*if (!holder.tv_contetiy.getText().toString().equalsIgnoreCase("0")) */{
                                if (!BaseURL.sub_id.equals("0")) {
                                    Log.e("qty", String.valueOf(qty));
                                    add_to_cart_api(userId, BaseURL.sub_id, mList.getProduct_id(), "1",
                                            mList.getUnit(), mList.getUnit_value(), holder.tv_contetiy, position);
                                } else {
                                    Log.e("qty", String.valueOf(qty));
                                    add_to_cart_api(userId, "0", mList.getProduct_id(), "1",
                                            mList.getUnit(), mList.getUnit_value(), holder.tv_contetiy, position);
                                }
                            }
                        } else {
                            showDialodg(context, mList, holder, position);
                        }
                    }else {
                        Intent intent = new Intent(context, LoginOrReg.class);
                        intent.putExtra("flag", Login_AT_TO_CART);
                        context.startActivity(intent);
                    }
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.iv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.connectionChecking(context)){
                    int Login_AT_TO_CART = 3;
                    qty = 0;
                    if (!holder.tv_contetiy.getText().toString().equalsIgnoreCase(""))
                        qty = Integer.valueOf(holder.tv_contetiy.getText().toString());
                    if (qty > 0) {
                        qty = qty - 1;
//                        holder.tv_contetiy.setText(String.valueOf(qty));
                    }
                    if (qty < 1) {
                        holder.linearlayout.setVisibility(View.GONE);
                        holder.relativelayout_add.setVisibility(View.VISIBLE);
                    }
                    if (sessionManagement.isLoggedIn()) {
                        if(isSingleItem){
                            String userId = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);
                            if (!holder.tv_contetiy.getText().toString().equalsIgnoreCase("0")) {
                                if (!BaseURL.sub_id.equals("0")) {
                                    Log.e("qty", String.valueOf(qty));
                                    add_to_cart_api(userId, BaseURL.sub_id, mList.getProduct_id(), "-1",
                                            mList.getUnit(), mList.getUnit_value(), holder.tv_contetiy, position);
                                } else {
                                    Log.e("qty", String.valueOf(qty));
                                    add_to_cart_api(userId, "0", mList.getProduct_id(), "-1",
                                            mList.getUnit(), mList.getUnit_value(), holder.tv_contetiy, position);
                                }
                            }
                        }else {
                            showDialodg(context, mList, holder, position);
                        }
                    } else {
                        Intent intent = new Intent(context, LoginOrReg.class);
                        intent.putExtra("flag", Login_AT_TO_CART);
                        context.startActivity(intent);
                    }
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void add_to_cart_api(String user_id, String sub_id, String pro_id, final String quantity, String unit,
                                 String unit_value, final TextView holder, int pos) {
        String tag_json_obj = "json_item_details_req";
        alertDialog.show();
        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", user_id);
        body.put("sub_id", sub_id);
        body.put("pro_id", pro_id);
        body.put("qty", quantity);
        body.put("price", "0");
        Log.d("body_cart", body.toString());

        String url = BaseURL.ADD_TO_CART+"?user_id="+user_id+"&sub_id="+sub_id+"&pro_id="+pro_id+"&qty="+quantity+"&price=0&unit="+unit+"&unit_value="+unit_value;

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.e("product_list", response.toString());
                try {
                    if (response.getBoolean("responce")) {
                        if (NetworkConnection.connectionChecking(context)){
                            if (sessionManagement.isLoggedIn()) {
                                TextView tv_contetiy = (TextView) holder ;//view.findViewById(R.id.tv_subcat_contetiy);
                                tv_contetiy.setText(String.valueOf(qty));
                                getCartcount(sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
                            }
                        }
                    }
                    alertDialog.dismiss();
                    Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        queue.add(jsonRequest);
    }

    private void getCartcount(String user_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        String url = BaseURL.GET_CART_COUNT+"?user_id="+user_id;

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo", response.toString());
                try {
                    if (response.getBoolean("responce")) {
//                        alertDialog.show();
                        int item_count = response.getInt("data");
                        sessionManagement.setCartLogin(item_count);
                        ((MainActivity) context).setCartCounter(String.valueOf(item_count));
                    }
                } catch (JSONException e) {
//                    alertDialog.show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                alertDialog.show();
            }
        });
        queue.add(jsonRequest);
    }


    private void showImage(String image) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.product_image_dialog);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        ImageView iv_image_cancle = (ImageView) dialog.findViewById(R.id.iv_dialog_cancle);
        ImageView iv_image = (ImageView) dialog.findViewById(R.id.iv_dialog_img);
        if (NetworkConnection.connectionChecking(context)){
            Glide.with(context)
                    .load(BaseURL.IMG_PRODUCT_URL + image)
                    .centerCrop()
                    .placeholder(R.drawable.icon)
                    .crossFade()
                    .into(iv_image);
        }
        iv_image_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void showDialodg(Context context, final Product_model model, final RecyclerView.ViewHolder holder, final int position){
        BottomSheetDialog getMobileFromUser = new BottomSheetDialog(context);
        getMobileFromUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View showBottomPopup = ((Activity)context).getLayoutInflater().inflate(R.layout.citylist, null);
        getMobileFromUser.setContentView(showBottomPopup);
        getMobileFromUser.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        getMobileFromUser.setCancelable(true);
        RadioGroup rg_sizes ;
        LinearLayout ll_size1 ; TextView rb_size1 ; TextView tv_size1_price, tv_size1_sub_price, tv_size1_subcribe_price;
        LinearLayout ll_size2 ; TextView rb_size2 ; TextView tv_size2_price, tv_size2_sub_price, tv_size2_subcribe_price;
        LinearLayout ll_size3 ; TextView rb_size3 ; TextView tv_size3_price, tv_size3_sub_price, tv_size3_subcribe_price;
        ImageView iv_subcat_minus1, iv_subcat_plus1, iv_subcat_minus2, iv_subcat_plus2, iv_subcat_minus3, iv_subcat_plus3 ;
        final TextView tv_subcat_contetiy1, tv_subcat_contetiy2, tv_subcat_contetiy3 , tv_p_name;
        Button btn_cancel, btn_add_to_cart ;
        rg_sizes = showBottomPopup.findViewById(R.id.rg_sizes);
        ll_size1 = showBottomPopup.findViewById(R.id.ll_size1); rb_size1 = showBottomPopup.findViewById(R.id.rb_size1);
        tv_size1_price = showBottomPopup.findViewById(R.id.tv_size1_price); tv_size1_sub_price = showBottomPopup.findViewById(R.id.tv_size1_sub_price);
        ll_size2 = showBottomPopup.findViewById(R.id.ll_size2); rb_size2 = showBottomPopup.findViewById(R.id.rb_size2);
        tv_size2_price = showBottomPopup.findViewById(R.id.tv_size2_price); tv_size2_sub_price = showBottomPopup.findViewById(R.id.tv_size2_sub_price);
        ll_size3 = showBottomPopup.findViewById(R.id.ll_size3); rb_size3 = showBottomPopup.findViewById(R.id.rb_size3);
        tv_size3_price = showBottomPopup.findViewById(R.id.tv_size3_price); tv_size3_sub_price = showBottomPopup.findViewById(R.id.tv_size3_sub_price);
        btn_cancel = showBottomPopup.findViewById(R.id.btn_cancel); btn_add_to_cart = showBottomPopup.findViewById(R.id.btn_add_to_cart);
        iv_subcat_minus1 = showBottomPopup.findViewById(R.id.iv_subcat_minus1); iv_subcat_plus1 = showBottomPopup.findViewById(R.id.iv_subcat_plus1);
        iv_subcat_minus2 = showBottomPopup.findViewById(R.id.iv_subcat_minus2); iv_subcat_plus2 = showBottomPopup.findViewById(R.id.iv_subcat_plus2);
        iv_subcat_minus3 = showBottomPopup.findViewById(R.id.iv_subcat_minus3); iv_subcat_plus3 = showBottomPopup.findViewById(R.id.iv_subcat_plus3);
        tv_subcat_contetiy1 = showBottomPopup.findViewById(R.id.tv_subcat_contetiy1); tv_subcat_contetiy2 = showBottomPopup.findViewById(R.id.tv_subcat_contetiy2);
        tv_subcat_contetiy3 = showBottomPopup.findViewById(R.id.tv_subcat_contetiy3); tv_p_name = showBottomPopup.findViewById(R.id.tv_p_name);
        tv_size1_subcribe_price = showBottomPopup.findViewById(R.id.tv_size1_subcribe_price); tv_size2_subcribe_price = showBottomPopup.findViewById(R.id.tv_size2_subcribe_price);
        tv_size3_subcribe_price = showBottomPopup.findViewById(R.id.tv_size3_subcribe_price);

        rb_size1.setText(model.getUnit_value() + " " + model.getUnit());
        tv_size1_sub_price.setText("\u20B9 "+model.getSurfcity_price());
        tv_size1_price.setText("\u20B9 "+model.getPrice());
        tv_p_name.setText(model.getProduct_name());
        tv_size1_subcribe_price.setText("\u20B9 "+model.getSubscription_price());

        if(model.getPrice1().trim().equalsIgnoreCase("0")){
            ll_size2.setVisibility(View.GONE);
        }else {
            ll_size2.setVisibility(View.VISIBLE);
            rb_size2.setText(model.getUnit_value1() + " " + model.getUnit1());
            tv_size2_sub_price.setText("\u20B9 "+model.getSurfcity_price1());
            tv_size2_price.setText("\u20B9 "+model.getPrice1());
            tv_size2_subcribe_price.setText("\u20B9 "+model.getSubscription_price1());
        }
        if(model.getPrice2().trim().equalsIgnoreCase("0")){
            ll_size3.setVisibility(View.GONE);
        }else {
            ll_size3.setVisibility(View.VISIBLE);
            rb_size3.setText(model.getUnit_value2() + " " + model.getUnit2());
            tv_size3_sub_price.setText("\u20B9 "+model.getSurfcity_price2());
            tv_size3_price.setText("\u20B9 "+model.getPrice2());
            tv_size3_subcribe_price.setText("\u20B9 "+model.getSubscription_price2());
        }

        iv_subcat_plus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = Integer.valueOf(tv_subcat_contetiy1.getText().toString());
                qty = qty + 1;
//                tv_subcat_contetiy1.setText(""+qty);
                if (!BaseURL.sub_id.equals("0")) {
                    add_to_cart_api(userId, BaseURL.sub_id, model.getProduct_id(), "1",
                            model.getUnit(), model.getUnit_value(), tv_subcat_contetiy1, position);
                } else {
                    add_to_cart_api(userId, "0", model.getProduct_id(), "1",
                            model.getUnit(), model.getUnit_value(), tv_subcat_contetiy1, position);
                }
            }
        });
        iv_subcat_minus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = 0;
                if (!tv_subcat_contetiy1.getText().toString().equalsIgnoreCase(""))
                    qty = Integer.valueOf(tv_subcat_contetiy1.getText().toString());
                if (qty > 0) {
                    qty = qty - 1;
//                    tv_subcat_contetiy1.setText(String.valueOf(qty));
                }
                if (!tv_subcat_contetiy1.getText().toString().equalsIgnoreCase("0")) {
                    if (!BaseURL.sub_id.equals("0")) {
                        Log.e("qty", String.valueOf(qty));
                        add_to_cart_api(userId, BaseURL.sub_id, model.getProduct_id(), "-1",
                                model.getUnit(), model.getUnit_value(), tv_subcat_contetiy1, position);
                    } else {
                        Log.e("qty", String.valueOf(qty));
                        add_to_cart_api(userId, "0", model.getProduct_id(), "-1",
                                model.getUnit(), model.getUnit_value(), tv_subcat_contetiy1, position);
                    }
                }
            }
        });

        iv_subcat_plus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = Integer.valueOf(tv_subcat_contetiy2.getText().toString());
                qty = qty + 1;
//                tv_subcat_contetiy2.setText(""+qty);
                if (!BaseURL.sub_id.equals("0")) {
                    add_to_cart_api(userId, BaseURL.sub_id, model.getProduct_id(), "1",
                            model.getUnit1(), model.getUnit_value1(), tv_subcat_contetiy2, position);
                } else {
                    add_to_cart_api(userId, "0", model.getProduct_id(), "1",
                            model.getUnit1(), model.getUnit_value1(), tv_subcat_contetiy2, position);
                }
            }
        });
        iv_subcat_minus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = 0;
                if (!tv_subcat_contetiy2.getText().toString().equalsIgnoreCase(""))
                    qty = Integer.valueOf(tv_subcat_contetiy2.getText().toString());
                if (qty > 0) {
                    qty = qty - 1;
//                    tv_subcat_contetiy2.setText(String.valueOf(qty));
                }
                if (!tv_subcat_contetiy2.getText().toString().equalsIgnoreCase("0")) {
                    if (!BaseURL.sub_id.equals("0")) {
                        Log.e("qty", String.valueOf(qty));
                        add_to_cart_api(userId, BaseURL.sub_id, model.getProduct_id(), "-1",
                                model.getUnit1(), model.getUnit_value1(), tv_subcat_contetiy2, position);
                    } else {
                        Log.e("qty", String.valueOf(qty));
                        add_to_cart_api(userId, "0", model.getProduct_id(), "-1",
                                model.getUnit1(), model.getUnit_value1(), tv_subcat_contetiy2, position);
                    }
                }
            }
        });

        iv_subcat_plus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = Integer.valueOf(tv_subcat_contetiy3.getText().toString());
                qty = qty + 1;
//                tv_subcat_contetiy3.setText(""+qty);
                if (!BaseURL.sub_id.equals("0")) {
                    add_to_cart_api(userId, BaseURL.sub_id, model.getProduct_id(), "1",
                            model.getUnit2(), model.getUnit_value2(), tv_subcat_contetiy3, position);
                } else {
                    add_to_cart_api(userId, "0", model.getProduct_id(), "1",
                            model.getUnit2(), model.getUnit_value2(), tv_subcat_contetiy3, position);
                }
            }
        });
        iv_subcat_minus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = 0;
                if (!tv_subcat_contetiy3.getText().toString().equalsIgnoreCase(""))
                    qty = Integer.valueOf(tv_subcat_contetiy1.getText().toString());
                if (qty > 0) {
                    qty = qty - 1;
//                    tv_subcat_contetiy3.setText(String.valueOf(qty));
                }
                if (!tv_subcat_contetiy3.getText().toString().equalsIgnoreCase("0")) {
                    if (!BaseURL.sub_id.equals("0")) {
                        Log.e("qty", String.valueOf(qty));
                        add_to_cart_api(userId, BaseURL.sub_id, model.getProduct_id(), "-1",
                                model.getUnit2(), model.getUnit_value2(), tv_subcat_contetiy3, position);
                    } else {
                        Log.e("qty", String.valueOf(qty));
                        add_to_cart_api(userId, "0", model.getProduct_id(), "-1",
                                model.getUnit2(), model.getUnit_value2(), tv_subcat_contetiy3, position);
                    }
                }
            }
        });
        getMobileFromUser.show();
    }

    //  private void showProductDetail(String image, String title, String description, String detail, final int position, String qty) {
    private void showProductDetail(final int position) {
        Bundle args = new Bundle();
        Fragment fm = new ProductDetails_Fragment();
        args.putString("product_id", modelList.get(position).getProduct_id());
        if(IS_DEAL_OF_THE_DAY) args.putString("max_qty", "0");
        fm.setArguments(args);
        FragmentManager fragmentManager = ((AppCompatActivity) context).getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
    }

}