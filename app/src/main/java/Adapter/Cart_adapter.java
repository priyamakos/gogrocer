package Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import Fragment.Empty_cart_fragment;
import com.adjit.surfcity.AppController;
import com.adjit.surfcity.MainActivity;
import com.adjit.surfcity.NetworkConnectivity.NetworkConnection;
import com.adjit.surfcity.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Config.BaseURL;
import Model.CartModel;
import Model.Deal_Of_Day_model;
import Model.Product_deatail_Model;
import fcm.Const;
import fcm.SharePreferenceUtility;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.Session_management;

/**
 * Created by Rajesh Dabhi on 26/6/2017.
 **/

public class Cart_adapter extends RecyclerView.Adapter<Cart_adapter.ProductHolder> {
    ArrayList<CartModel> list;
    Activity activity;
    String Reward;
    int lastpostion, qty;
    DatabaseHandler dbHandler;
    Session_management session_management;
    private ArrayList<Deal_Of_Day_model> temp ;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog alertDialog;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Cart_adapter(Activity activity, ArrayList<CartModel> list, ArrayList<Deal_Of_Day_model> temp) {
        this.list = list;
        this.activity = activity;
        this.temp = temp ;
        dbHandler = new DatabaseHandler(activity);
        mDialogBuilder = new AlertDialog.Builder(activity);
        mDialogBuilder.setView(R.layout.custom_progress_bar);
        mDialogBuilder.setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        session_management = new Session_management(activity);

        if (session_management.getsubscription() != null) {
            BaseURL.sub_id = (String) SharePreferenceUtility.getPreferences(activity, Const.SUBSCRIPTION_ID, SharePreferenceUtility.PREFTYPE_STRING);
            if(BaseURL.sub_id == null || BaseURL.sub_id.equalsIgnoreCase("")) {
                BaseURL.sub_id = "0";
            }
        }
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_rv, parent, false);
        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final CartModel map = list.get(position);

        if (NetworkConnection.connectionChecking(activity)){
            Glide.with(activity)
                    .load(BaseURL.IMG_PRODUCT_URL + map.getProduct_image())
                    .centerCrop()
                    .placeholder(R.drawable.icon)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .centerCrop()
                    .into(holder.iv_logo);
        }
        holder.tv_title.setText(map.getProduct_name());

        if(map.getIsdealofproduct()) {
            holder.tv_price.setText(activity.getResources().getString(R.string.currency) + " " + map.getDeal_price() + " / " + map.getDeal_unit_value() + " " + map.getDeal_unit());
        }else if(BaseURL.sub_id.equals("0")) {
            holder.tv_price.setText(activity.getResources().getString(R.string.currency) + " " + map.getSurfcity_price() + " / " + map.getUnit_value() + " " + map.getUnit() );
        }else {
            holder.tv_price.setText(activity.getResources().getString(R.string.currency) + " " + map.getSubscription_price() + " / " + map.getUnit_value() + " " + map.getUnit());
        }

        Double items = Double.parseDouble(map.getQty()) ;
        Double price ;
        if(map.getIsdealofproduct()) {
            price = Double.parseDouble(map.getDeal_price()) ;
        }else if(BaseURL.sub_id.equals("0")) {
            price = Double.parseDouble(map.getSurfcity_price()) ;
        }else {
            price = Double.parseDouble(map.getSubscription_price()) ;
        }

        if(!map.getQty().equals("")) {
            holder.tv_contetiy.setText(map.getQty());
        }

        Double reward;
        if (map.getRewards().equals("")) {
            reward = 0.0;
        } else {
            reward = Double.parseDouble(map.getRewards());
        }

        holder.tv_total.setText("" + price * items);

        holder.iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.connectionChecking(activity)) {
                    qty = Integer.valueOf(holder.tv_contetiy.getText().toString());
                   /* if(checkMax(qty, list.get(position).getProduct_id())) {*/
                        qty = qty + 1;
                        /*if(Integer.parseInt(list.get(position).getIn_stock()) >= qty){*/
//                            holder.tv_contetiy.setText(String.valueOf(qty));
                    if(map.getIsdealofproduct()){
                        add_to_cart_api(session_management.getUserDetails().get(BaseURL.KEY_ID), BaseURL.sub_id, list.get(position).getProduct_id(),
                                "1", position, list.get(position).getDeal_unit(), list.get(position).getDeal_unit_value(), holder.tv_contetiy);
                    }else {
                        add_to_cart_api(session_management.getUserDetails().get(BaseURL.KEY_ID), BaseURL.sub_id, list.get(position).getProduct_id(),
                                "1", position, list.get(position).getUnit(), list.get(position).getUnit_value(), holder.tv_contetiy);
                    }
                       /* }else {
                            Toast.makeText(activity, activity.getResources().getString(R.string.product_out_of_stock), Toast.LENGTH_SHORT).show();
                        }*/
                    /*}else {
                        Toast.makeText(activity, "You can add maximum "+qty+" products.", Toast.LENGTH_SHORT).show();
                    }*/
                    //updateintent();
                }else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.iv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(NetworkConnection.connectionChecking(activity)) {
                    qty = 0;
                    if (!holder.tv_contetiy.getText().toString().equalsIgnoreCase(""))
                        qty = Integer.valueOf(holder.tv_contetiy.getText().toString());
                    if (qty > 1) {
                        qty = qty - 1;
//                        holder.tv_contetiy.setText(String.valueOf(qty));
                       if(map.getIsdealofproduct()){
                           add_to_cart_api(session_management.getUserDetails().get(BaseURL.KEY_ID), BaseURL.sub_id, list.get(position).getProduct_id(),
                                   "-1", position, list.get(position).getDeal_unit(), list.get(position).getDeal_unit_value(), holder.tv_contetiy);
                       }else {
                           add_to_cart_api(session_management.getUserDetails().get(BaseURL.KEY_ID), BaseURL.sub_id, list.get(position).getProduct_id(),
                                   "-1", position, list.get(position).getUnit(), list.get(position).getUnit_value(), holder.tv_contetiy);
                       }
                    }else if (qty <= 1) {
                        cartDelete(session_management.getUserDetails().get(BaseURL.KEY_ID), list.get(position).getProduct_id(), position, list.get(position).getUnit(), list.get(position).getUnit_value());
                    }
                    //updateintent();
                }else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();

                }
            }
        });

        holder.iv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.connectionChecking(activity)){
                    if (session_management.isLoggedIn()) {
                        show_Dialog(activity, session_management.getUserDetails().get(BaseURL.KEY_ID), list.get(position).getProduct_id(), position);
                    }
                }else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.cart_set_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (session_management.isLoggedIn()) {
                    show_qtyDialog(activity, holder.cart_set_quantity, list.get(position).getProduct_id(), session_management.getUserDetails().get(BaseURL.KEY_ID), position);
                }
            }
        });

    }

    private boolean checkMax(int qty, String product_id) {
        int t = qty + 1;
        boolean result = true ;
        for(int i = 0 ; i < temp.size(); i++){
            if(product_id.equalsIgnoreCase(temp.get(i).getProduct_id())){
               result = true ;
                if(!(t <= Integer.parseInt(temp.get(i).getMax_qty()))){
                    result = false ;
                }
            }
        }
        return result;
    }


    private void show_qtyDialog(final Activity activity, final TextView qty, final String prod_id, final String user_id, final int position) {

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_qty_dialog);
        ListView lv_qty = dialog.findViewById(R.id.lv_qty);

        lv_qty.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView view1 = (TextView) view;
               // Toast.makeText(activity,view1.getText().toString(),Toast.LENGTH_SHORT).show();
                if (NetworkConnection.connectionChecking(activity)){
                    qty.setText(view1.getText().toString());
                    add_to_cart_api(user_id, BaseURL.sub_id, prod_id, qty.getText().toString(), position,
                            list.get(position).getUnit(), list.get(position).getUnit_value(), null);
                }else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();


    }

    private void show_Dialog(Activity activity, final String user_id, final String prod_id, final int position) {
        Dialog dialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Delete");
        builder.setMessage("Are you sure want to delete.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                cartDelete(user_id, prod_id, position, "", "");
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ProductHolder extends RecyclerView.ViewHolder {

        public TextView tv_title, tv_price, tv_reward, tv_total, tv_add, tv_unit, tv_unit_value,tv_contetiy;
        public ImageView iv_logo,iv_plus, iv_minus;
        public TextView iv_remove;
        public TextView cart_set_quantity;

        public ProductHolder(View view) {
            super(view);

            tv_title = (TextView) view.findViewById(R.id.tv_subcat_title);
            tv_price = (TextView) view.findViewById(R.id.tv_subcat_price);
            tv_total = (TextView) view.findViewById(R.id.tv_subcat_total);
            //tv_reward = (TextView) view.findViewById(R.id.tv_reward_point);
            tv_contetiy = (TextView) view.findViewById(R.id.tv_contetiy);
            tv_add = (TextView) view.findViewById(R.id.tv_subcat_add);
            iv_logo = (ImageView) view.findViewById(R.id.iv_subcat_img);
            iv_plus = (ImageView) view.findViewById(R.id.iv_plus);
            iv_minus = (ImageView) view.findViewById(R.id.iv_minus);
            iv_remove = view.findViewById(R.id.iv_subcat_remove);
            cart_set_quantity = view.findViewById(R.id.cart_set_quantity);
            //   tv_add.setText(R.string.tv_pro_update);
        }
    }

    private void cartDelete(String user_id, final String pro_id, final int position, String unit, String unit_value) {
        alertDialog.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        map.put("pro_id", pro_id);
        Log.d("del_cat", map.toString());

        String url = BaseURL.CART_DELETE
                +"?user_id="+user_id
                +"&pro_id="+pro_id
                +"&unit="+unit
                +"&unit_value="+unit_value+"&price=0";

        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                try {
                    Log.d("res", response.toString());
                    alertDialog.dismiss();
                    if (response.getBoolean("responce")) {
                        Toast.makeText(activity.getBaseContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                        if (NetworkConnection.connectionChecking(activity)){
                            if (session_management.isLoggedIn()) {
                                getCartcount(session_management.getUserDetails().get(BaseURL.KEY_ID));
                            }
                        }
                        //updateintent();
                        list.remove(position);
                        notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        queue.add(jsonRequest);
    }

    private void updateintent() {
        Intent updates = new Intent("Grocery_cart");
        updates.putExtra("type", "update");
        updates.putExtra("reload", "reload");
        activity.sendBroadcast(updates);
    }


    private void add_to_cart_api(String user_id, String sub_id, String pro_id, final String quantity, final int position,
                                 String unit, String unit_value, final TextView tv_contetiy) {
        String url ;
        String tag_json_obj = "json_item_details_req";
        alertDialog.show();
        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", user_id);

        if (!sub_id.equals("")) {
            body.put("sub_id", sub_id);
        }

        body.put("pro_id", pro_id);
        body.put("qty",quantity);
        Log.d("body_cart", body.toString());
        if (!sub_id.equals("")) {
            url = BaseURL.ADD_TO_CART+"?user_id="+user_id+"&sub_id="+sub_id+"&pro_id="+pro_id+"&qty="+quantity+"&price=0&unit="+unit+"&unit_value="+unit_value;
        } else {
            url = BaseURL.ADD_TO_CART+"?user_id="+user_id+"&pro_id="+pro_id+"&qty="+quantity+"&price=0&unit="+unit+"&unit_value="+unit_value;
        }

        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("product_details", response.toString());
                try {
                    alertDialog.dismiss();
                    if (response.getBoolean("responce")) {
                        TextView contetiy = (TextView) tv_contetiy ;
                        contetiy.setText(String.valueOf(qty));
                        Toast.makeText(activity, response.getString("message"), Toast.LENGTH_SHORT).show();
                        list.get(position).setQty(quantity);
                        ((MainActivity) activity).setCartCounter("" + session_management.getCartCount());
                        updateintent();
                        notifyItemChanged(position, list.get(position));
                    }else {
                        Toast.makeText(activity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    alertDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });
        queue.add(jsonRequest);
    }

    private void getCartcount(String user_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        String url = BaseURL.GET_CART_COUNT+"?user_id="+user_id;

        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                Log.d("respo", response.toString());
                try {
                    if (response.getBoolean("responce")) {

                        int item_count = response.getInt("data");
                        session_management.setCartLogin(item_count);
                        ((MainActivity) activity).setCartCounter(String.valueOf(item_count));
                        updateintent();
                        if(item_count == 0) {
                            Fragment fm = new Empty_cart_fragment();
                            FragmentManager fragmentManager = activity.getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsonRequest);
    }

}

